DROP VIEW IF EXISTS view_videos_orden;
CREATE VIEW view_videos_orden AS SELECT 
videos.orden as clasesorden,
clases.nombre as clase,
videos.*,
videos.id as videos_id,
clases.idiomas_clases_id as idiomaid
FROM `videos` 
INNER JOIN clases on clases.id = videos.clases_id
ORDER BY videos.orden ASC