<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
        }

        public function videos($id = ''){            
            $id = base64_decode($id);
            if(is_numeric($id)){
            	$video = $this->elements->clases_prueba(array('clases_prueba.id'=>$id));
            	if($video->num_rows()>0){
            		$video = $video->row();
            		require_once(APPPATH.'libraries/Stream.php');
                    $webm = str_replace('.mp4','.webm',$video->_fichero);
                    $mp4 = $video->_fichero;
                    if(file_exists($webm)){
                        $video->_fichero = $webm;
                    }else{
                        $video->_fichero = $mp4;
                    }
                    $stream = new VideoStream('audios/'.$video->_fichero);
                    $stream->start();
            	}else{
            		throw new exception('Video no encontrado',404);
            	}
            }else{
            	throw new exception('Video no encontrado',404);
            }
        }
    }
?>
