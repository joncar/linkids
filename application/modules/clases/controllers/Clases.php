<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Clases extends Panel{
        protected $max_videos = 3;
        function __construct() {
            parent::__construct();
            $this->max_videos = $this->elements->getMaxVideos();            
        }

        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posees permisos para realizar esta operación','403');
                }
                else{
                    //Ha pagado su membresia?
                    if(!$this->elements->canAccess()){
                        if(empty($this->user->user_vinculacion)){
                            redirect('finanzas/cargarPago');                        
                        }else{
                            $this->load->view('errors/600');
                        }
                        
                    }
                    if(!empty($param->output)){
                        $panel = $this->user->admin==1?'panel':'panelUsuario';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = 'template';
                    $this->load->view($template,$param);
                }                
            }
        }

        public function read($id = ""){

        	$id = base64_decode($id);
            $progreso = $this->elements->getProgreso($id);
            if($progreso){                
                if($progreso->fecha_apertura!=date("Y-m-d") && $this->db->get_where('progresos',array('user_id'=>$this->user->id,'DATE(fecha_apertura)'=>date("Y-m-d")))->num_rows()==0){
                    //Traer siguiente video en la lista
                    $this->db->order_by('orden','ASC');
                    $this->db->where('IF(progresos.videos_id IS NULL,0,1) = 0',NULL,FALSE);
                    $lista = $this->elements->getVideosEnOrden();
                    if($lista->num_rows()>0){
                        $progreso = $this->elements->getProgreso($lista->row()->videos_id,TRUE);
                    }
                }
            	$this->db->select('clases.*');
                $this->db->join('videos','videos.clases_id = clases.id');
                $clase = $this->elements->clases(array('videos.id'=>$progreso->videos_id));
                if($clase->num_rows()==0){
                    //redirect('clases');
                    exit();
                }
                $clase = $clase->row();
                $clase->videoInfo = $this->elements->videos(array('id'=>$progreso->videos_id))->row();
                $clase->video = $clase->videoInfo->fichero;
                $clase->next = $this->elements->getNext();
                $clase->next = $clase->next->id==$id?-1:$clase->next;                 
                $desde = $this->elements->progresos();
                $desde = $desde->num_rows()>0?$desde->num_rows()-1:0;                        
                $this->db->limit($this->max_videos,$desde);
                $videos = $this->elements->videos();    
            }else{
                $clase = false;
                $videos = false;
            }

        	$this->loadView(array(
        		'view'=>'read',
        		'title'=>'Clases',
        		'page'=>$this->load->view('es/clases_view',array('clase'=>$clase,'videos'=>$videos),TRUE,'paginas'),        		
        	));
        }

        public function videos($id = ''){            
            $id = base64_decode($id);
            if(is_numeric($id)){
            	$video = $this->elements->videos(array('id'=>$id));
            	if($video->num_rows()>0){
            		$video = $video->row();
            		if(empty($_SESSION['user'])){
            			throw new exception('Usuario no autorizado',403);
            		}else{    
                        require_once(APPPATH.'libraries/Stream.php');
                        $webm = str_replace('.mp4','.webm',$video->_fichero);
                        $mp4 = $video->_fichero;
                        if(file_exists($webm)){
                            $video->_fichero = $webm;
                        }else{
                            $video->_fichero = $mp4;
                        }
                        $stream = new VideoStream('audios/'.$video->_fichero);
                        $stream->start();           			
            		}	
            	}else{
            		throw new exception('Video no encontrado',404);
            	}
            }else{
            	throw new exception('Video no encontrado',404);
            }
        }

        function video($url){
            $file =  'audios/'.$url;
            $fp = @fopen($file, 'rb');
            $size   = filesize($file); // File size
            $length = $size;           // Content length
            $start  = 0;               // Start byte
            $end    = $size - 1;       // End byte
            header('Content-type: video/mp4');
            header("Accept-Ranges: 0-$length");
            

            if (isset($_SERVER['HTTP_RANGE'])) {
                $c_start = $start;
                $c_end   = $end;
                list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
                if (strpos($range, ',') !== false) {
                    header('HTTP/1.1 416 Requested Range Not Satisfiable');
                    header("Content-Range: bytes $start-$end/$size");
                    exit;
                }
                if ($range == '-') {
                    $c_start = $size - substr($range, 1);
                }else{
                    $range  = explode('-', $range);
                    $c_start = $range[0];
                    $c_end   = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $size;
                }
                $c_end = ($c_end > $end) ? $end : $c_end;
                if ($c_start > $c_end || $c_start > $size - 1 || $c_end >= $size) {
                    header('HTTP/1.1 416 Requested Range Not Satisfiable');
                    header("Content-Range: bytes $start-$end/$size");
                    exit;
                }
                $start  = $c_start;
                $end    = $c_end;
                $length = $end - $start + 1;
                fseek($fp, $start);
                header('HTTP/1.1 206 Partial Content');
            }
            header("Content-Range: bytes $start-$end/$size");
            header("Content-Length: ".$length);


            $buffer = 1024 * 8;
            while(!feof($fp) && ($p = ftell($fp)) <= $end) {

                if ($p + $buffer > $end) {
                    $buffer = $end - $p + 1;
                }
                set_time_limit(0);
                echo fread($fp, $buffer);
                flush();
            }
            fclose($fp);        
        }

        function videos_ocacionales($id = ''){
            $id = base64_decode($id);
            $where = empty($id)?array():array('videos_ocacionales.id'=>$id);
            $video = $this->elements->videos_ocacionales($where)->row();
            $this->loadView(array(
                'view'=>'read',
                'title'=>'Musica/Videos',
                'page'=>$this->load->view('es/videos',array('id'=>$id,'video'=>$video),TRUE,'paginas')
            ));
        }

        public function aperturar(){
            $this->form_validation->set_rules('videos_id');
            if($this->form_validation->run()){
                $aperturas = $this->db->get_where('progresos',array('user_id'=>$this->user->id,'fecha_apertura'=>date("Y-m-d")));
                if($aperturas->num_rows()<$this->max_videos){
                    $progreso = $this->elements->getProgreso(base64_decode($_POST['videos_id']),TRUE);
                    if($progreso){
                        echo json_encode(array('success'=>true));
                    }else{
                        echo json_encode(array('success'=>false,'msj'=>'Error al aperturar'));
                    }
                }else{
                    echo json_encode(array('success'=>false,'msj'=>'Ya haz visto muchos videos hoy, ven mañana a por más'));
                }
            }
        }

        function search(){
            if(!empty($_POST['q'])){
                $q = $_POST['q'];
                $this->db->where("view_videos_orden.clase like '%".$q."%' OR view_videos_orden.nombre like '%".$q."%'",NULL,FALSE);
                $videos = $this->elements->getVideosEnOrden();
                if($videos->num_rows()==0){
                    echo '<li><a href="javascript:void()">Sin resultados</a></li>';
                }else{
                    foreach($videos->result() as $v){
                        $video = $this->elements->videos(array('view_videos_orden.id'=>$v->id))->row();
                        if($video->activo){
                            echo '<li><a href="'.$video->link.'">'.$v->nombre.'</a></li>';
                        }else{
                            echo '<li><a href="javascript:void(0)" data-toggle="modal" data-target="#clase-bloqueada">'.$v->nombre.'</a></li>';
                        }
                    }
                }
            }
        }

        function searchVideos(){
            if(!empty($_POST['q'])){
                $q = $_POST['q'];
                $this->db->where("nombre like '%".$q."%'",NULL,FALSE);
                $videos = $this->elements->videos_ocacionales();
                if($videos->num_rows()==0){
                    echo '<li><a href="javascript:void()">Sin resultados</a></li>';
                }else{
                    foreach($videos->result() as $v){
                        echo '<li><a href="'.$v->link.'">'.$v->nombre.'</a></li>';
                    }
                }
            }
        }
    }
?>
