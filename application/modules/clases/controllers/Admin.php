<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        public function clases(){            
            $crud = $this->crud_function('','');
            $crud->set_subject('Categorias');
            $crud->set_lang_string('insert_success_message','Se ha almacenado con éxito <script>document.location.href="'.base_url('clases/admin/videos/{id}/add').'";</script>');
            $crud->add_action('Añadir videos','',base_url('clases/admin/videos').'/');
            $crud = $crud->render();         
            $crud->title = 'Categorias';   
            $this->loadView($crud);
        }

        public function clases_prueba(){
            $crud = $this->crud_function('','');
            $crud->set_relation('videos_id','videos','nombre',array(),'orden');
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        public function idiomas_clases(){            
            $crud = $this->crud_function('','');
            $crud->field_type('bandera','image',array('path'=>'img/idiomas','width'=>'150px','height'=>'150px'));
            $crud = $crud->render();            
            $this->loadView($crud);
        }

         public function insignias(){            
            $crud = $this->crud_function('','');
            $crud->field_type('imagen','image',array('path'=>'img/insignias','width'=>'150px','height'=>'150px'));
            $crud->set_order('orden');
            $crud->set_clone();
            $crud = $crud->render();            
            $this->loadView($crud);
        }

        public function videos($x = ''){
        	$crud = $this->crud_function('','');           
            $videos = array();
            foreach(scandir('audios') as $file){
                if($file!='.' && $file!='..' && strpos($file,'.mp4')){
                    $videos[$file] = $file;
                }
            } 
        	$crud->field_type('fichero','dropdown',$videos)/*set_field_upload('fichero','audios')*/                 
                 ->field_type('thumb','image',array('path'=>'img/thumbs','width'=>'200px','height'=>'100px'));
            $crud->callback_after_insert(array($this,'generateThumb'))
                 ->callback_after_update(array($this,'generateThumb'));
            $crud->field_type('clases_id','hidden','1');
            $crud->add_action('Añadir categorias','',base_url('clases/admin/clases_categorias').'/');

        	$crud->set_order('orden');
            $crud = $crud->render();            
            $this->loadView($crud);
        }   

        function generateThumb($post,$primary){
            $file = $post['fichero'];                        
            exec('ffmpeg -i audios/'.$file.' -f mjpeg -vframes 71 -s 200x100 -an img/thumbs/'.$primary.'.jpg');            
            $this->db->update('videos',array('thumb'=>$primary.'.jpg'),array('id'=>$primary));
            return true;
        }

        function progresos($x = ''){

            if(!is_numeric($x)){
                $this->as['progresos'] = 'user';
            }
            $crud = $this->crud_function('',''); 
            if(is_numeric($x)){
                $crud->where('user_id',$x);
                $crud->set_relation('videos_id','videos','nombre');
            }
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read();
            if(!is_numeric($x)){
                $crud->add_action('Ver','',base_url('clases/admin/progresos/').'/');
                $crud->columns('nombre','apellido_paterno','email');
            }
            $crud = $crud->render();            
            $this->loadView($crud);
        }  

        function import_videos_ocacionales(){
            $files = scandir('musica-completa');
            $this->db->query('truncate videos_ocacionales');
            foreach($files as $f){
                if($f!='.' && $f!='..'){
                    $nombre = explode('.',$f,2)[0];
                    $this->db->insert('videos_ocacionales',array(
                        'nombre'=>$nombre,
                        'video'=>$f,
                        'autor'=>'Linkids',
                        'idiomas_clases_id'=>1
                    ));
                }
            }
        }

        function videos_ocacionales(){
            $crud = $this->crud_function('','');   
            $crud->field_type('miniatura','image',array('path'=>'img/thumbs','width'=>'50px','height'=>'50px'));
            $crud->set_field_upload('video','videos_ocacionales_files');            
            $crud->set_order('orden');
            $crud = $crud->render();            
            $this->loadView($crud);
        }    

        function clases_categorias($x = ''){                        
            $crud = $this->crud_function('','');   
            $crud->field_type('videos_id','hidden',$x)
                 ->where('videos_id',$x);
            $crud = $crud->render();            
            $this->loadView($crud);
        } 

        function refreshBD(){
            //Renombrar ficheros para darles formato utf
            $files = scandir('audios');
            foreach($files as $f){
                if(strpos($f,'.mp4')){
                    $path = 'audios/';
                    list($f,$ext) = explode('.',$f,2);
                    $newFile = $path.toUrl($f).'.'.$ext;                    
                    $f = $path.$f.'.'.$ext;
                    rename($f,$newFile);
                }
            }

            $idiomas = array(
                'espa'=>3,
                'frances'=>6,
                'fances'=>6,
                'france'=>6,
                'chino'=>4,
                'mandarin'=>4,
                'aleman'=>5,
                'ingles'=>1
            );

            //Generamos thumbs e insertamos en bd
            $files = scandir('audios');
            exec('rm -rf img/thumbs/*');
            $this->db->query('truncate videos');
            foreach($files as $f){
                if(strpos($f,'.mp4')){
                    list($ff,$ext) = explode('.',$f,2);
                    exec('ffmpeg -i audios/'.$f.' -f mjpeg -vframes 71 -s 200x100 -an img/thumbs/'.$ff.'.jpg');
                    $thumb = $ff.'.jpg';
                    //Insertamos en bd
                    $names = explode('-',$ff);
                    preg_match_all('!\d+!', $names[0],$orden);  
                    $this->db->insert('videos',array(
                        'clases_id'=>1,
                        'idiomas_clases_id'=>empty($names[2])?1:$idiomas[$names[2]],
                        'thumb'=>$thumb,
                        'nombre'=>empty($names[1])?ucfirst($names[0]):ucfirst($names[0].' '.$names[1]),
                        'descripcion'=>'',
                        'fichero'=>$f,
                        'orden'=>$orden[0][0]
                    ));
                }
            }
            $this->db->query('truncate progresos');
            $this->db->query('update videos, (SELECT * FROM idiomas_temp) as cd SET videos.idiomas_clases_id = cd.idiomas_clases_id WHERE videos.orden = cd.videos_id');
        }
    }
?>
