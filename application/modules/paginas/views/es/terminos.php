<!-- Menu Top -->
    <header>
      <?php $this->load->view('es/includes/menu-valoracion',array(),FALSE,'paginas');?>
      <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy&display=swap" rel="stylesheet">
    </header>
    <!-- Menu Top -->

    <!-- Scroll to top button -->
    <div class="scrolltotop">
      <a class="button-circle button-circle-sm button-circle-dark" href="#">
        <i class="ti-arrow-up"></i>
      </a>
    </div>
    <!-- end Scroll to top button -->

    <!-- Social -->
    <div class="social">
      <ul>
        <li class="icon-whatsapp text-center"><a href="https://wa.me/5215517005534" title="Enviar mensaje"><i class="fab fa-whatsapp"></i></a></li>
        <li class="icon-planes text-center"><a href="<?= base_url() ?>main.html#planes" title="Ver Planes" id="margin-planes"><b>Planes</b></a></li>
      </ul>
    </div>

    <!-- Home section -->
    <div class="section-xl bg-image parallax bg-home-header hero-bkg-animated" style="background-image: url(<?= base_url() ?>theme/svg/home/header.svg)" id="inicio">
      <div class="container">
        <div class="row margin-top-50 margin-bottom-50 align-items-center">
          <div class="col-12 col-sm-12 text-center titulo-header-home">
          	<div class="margin-bottom-20">
	            <h1 class="font-weight-bold no-margin text-uppercase text-yellow margin-bottom-10 font-cursiva">
	              <span class="text-white"></span><br>Términos y condiciones
	            </h1>
        	  </div>
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Home section -->






    <!-- Services section -->
    <div class="section bg-orange">
      <div class="container">

        <div class="row container-white-2">
          <div class="col-12 col-md-12 bg-white-home">


              <div class="row margin-bottom-30">
                <div class="col-12 col-md-12">
                  <p class="text-home">
                    Este contrato describe los términos y condiciones generales (en adelante únicamente &quot;TÉRMINOS Y CONDICIONES&quot;) aplicables al uso de los contenidos,
                    productos y servicios ofrecidos a través del sitio linkids.com.mx (en adelante, &quot;SITIO WEB&quot;), del cual es titular Ruben Esteban Ataides Fagundez (en
                    adelante, &quot;TITULAR&quot;). Cualquier persona que desee acceder o hacer uso del sitio o los servicios que en él se ofrecen, podrá hacerlo sujetándose a los
                    presentes TÉRMINOS Y CONDICIONES, así como a políticas y principios incorporados al presente documento. En todo caso, cualquier persona que no
                    acepte los presentes términos y condiciones, deberá abstenerse de utilizar el SITIO WEB y/o adquirir los productos y servicios que en su caso sean ofrecidos.<br><br>

                    <span class="font-weight-bold">I. DEL OBJETO.</span><br>
                    El objeto de los presentes TÉRMINOS Y CONDICIONES es regular el acceso y la utilización del SITIO WEB, entendiendo por este cualquier tipo de contenido,
                    producto o servicio que se encuentre a disposición del público en general dentro del dominio: linkids.com.mx.<br><br>

                    El TITULAR se reserva la facultad de modificar en cualquier momento y sin previo aviso, la presentación, los contenidos, la funcionalidad, los productos,
                    los servicios, y la configuración que pudiera estar contenida en el SITIO WEB; en este sentido, el USUARIO reconoce y acepta que linkids.com.mx en cualquier momento podrá interrumpir, desactivar o cancelar cualquiera de los
                    elementos que conforman el SITIO WEB o el acceso a los mismos.<br><br>

                    El acceso al SITIO WEB por parte del USUARIO tiene carácter de contrato privado y, por regla general es de cobro obligando al USUARIO a proporcionar una contraprestación para poder disfrutar de ello, independientemente al
                    costo referente de la conexión a internet suministrada por el proveedor de este tipo de servicios que hubiere contratado el mismo USUARIO.<br>
                    El acceso a parte de los contenidos y servicios del SITIO WEB podrá realizarse previa suscripción o registro previo del USUARIO.<br><br>

                    El SITIO WEB se encuentra dirigido exclusivamente a los maestros, padres y sus hijos, para los cuales está dirigido este programa, la información
                    proporcionada es una herramienta de apoyo y estimulación en el aprendizaje de los diferentes idiomas ofrecidos, a través de la estimulación neuronal de los
                    usuarios. <br><br>

                    El SITIO WEB está dirigido principalmente a USUARIOS residentes en la República Mexicana, por lo cual, linkids.com.mx no asegura que el SITIO WEB
                    cumpla total o parcialmente con la legislación de otros países, de forma que, si el USUARIO reside o tiene su domicilio establecido en otro país y decide
                    acceder o utilizar el SITIO WEB lo hará bajo su propia responsabilidad y deberá asegurarse de que tal acceso y navegación cumple con la legislación local que
                    le es aplicable, no asumiendo linkids.com.mx ninguna responsabilidad que se pueda derivar de dicho acto.<br><br>

                    Se hace del conocimiento del USUARIO que el TITULAR podrá administrar o gestionar el SITIO WEB de manera directa o a través de un tercero, lo cual no modifica en ningún sentido lo establecido en los presentes
                    TÉRMINOS Y CONDICIONES.<br><br>

                    <span class="font-weight-bold">II. DEL USUARIO.</span><br>
                    El acceso o utilización del SITIO WEB, así como de los recursos habilitados para interactuar entre los USUARIOS, o entre el USUARIO y el TITULAR tales como
                    medios para realizar publicaciones o comentarios, confiere la condición de USUARIO del SITIO WEB, por lo que quedará sujeto a los presentes TÉRMINOS Y CONDICIONES, así como a sus ulteriores modificaciones, sin perjuicio de la
                    aplicación de la legislación aplicable, por tanto, se tendrán por aceptados desde el momento en el que se accede al SITIO WEB. Dada la relevancia de lo
                    anterior, se recomienda al USUARIO revisar las actualizaciones que se realicen a los presentes TÉRMINOS Y CONDICIONES.<br><br>

                    Es responsabilidad del USUARIO utilizar el SITIO WEB de acuerdo a la forma en la que fue diseñado; en este sentido, queda prohibida la utilización de cualquier tipo de software que automatice la interacción o descarga de los
                    contenidos o servicios proporcionados a través del SITIO WEB. Además, el USUARIO se compromete a utilizar la información, contenidos o servicios ofrecidos a través del SITIO WEB de manera lícita, sin contravenir lo dispuesto
                    en los presentes TÉRMINOS Y CONDICIONES, la moral o el orden público, y se abstendrá de realizar cualquier acto que pueda suponer una afectación a los derechos de terceros, o perjudique de algún modo el funcionamiento
                    del SITIO WEB.<br><br>

                    Así mismo, el usuario se compromete a proporcionar información lícita y veraz en los formularios habilitados en el SITIO WEB, en los cuales el usuario tenga
                    que proporcionar ciertos datos o información para el acceso a algún contenido, producto o servicio ofrecido por el propio SITIO WEB. En todo caso, el USUARIO notificará de forma inmediata al TITULAR acerca de cualquier hecho
                    que permita suponer el uso indebido de la información registrada en dichos formularios, tales como, robo, extravío, o acceso no autorizado a cuentas y/o contraseñas, con el fin de proceder a su inmediata cancelación.<br>
                    linkids.com.mx se reserva el derecho de retirar todos aquellos comentarios y aportaciones que vulneren la ley, el respeto a la dignidad de la persona, que
                    sean discriminatorios, atenten contra los derechos de tercero o el orden público, o bien, que a su juicio no resulten adecuados para su publicación.<br><br>

                    En cualquier caso, linkids.com.mx no será responsable de las opiniones vertidas por los USUARIOS a través de comentarios o publicaciones que estos
                    realicen.<br><br>

                    El sólo acceso al SITIO WEB no supone el establecimiento de ningún tipo de relación entre el TITULAR y el USUARIO.<br>
                    Al tratarse de un SITIO WEB dirigido exclusivamente a personas que cuenten con la mayoría de edad, el USUARIO manifiesta ser mayor de edad y disponer de la capacidad jurídica necesaria para sujetarse a los presentes TÉRMINOS Y
                    CONDICIONES.<br><br>

                    <span class="font-weight-bold">III. DEL ACCESO Y NAVEGACIÓN EN EL SITIO WEB.</span><br>
                    El TITULAR no garantiza de ningún modo la continuidad y disponibilidad de los contenidos, productos o servicios ofrecidos a través del SITIO WEB, no obstante, el TITULAR llevará a cabo las acciones que de acuerdo a sus
                    posibilidades le permitan mantener el buen funcionamiento del SITO WEB, sin que esto suponga alguna responsabilidad de parte de linkids.com.mx.<br><br>

                    De igual forma linkids.com.mx no será responsable ni garantiza que el contenido o software al que pueda accederse a través del SITIO WEB, se
                    encuentre libre de errores, software malicioso, o que pueda causar algún daño a nivel de software o hardware en el equipo a través del cual el USUARIO
                    accede al SITIO WEB.<br>
                    El TITULAR tampoco se hace responsable de los daños que pudiesen ocasionarse por un uso inadecuado del SITIO WEB. En ningún caso
                    linkids.com.mx será responsable por las pérdidas, daños o perjuicios de cualquier tipo que surjan por el sólo acceso o utilización del SITIO WEB.<br><br>

                    <span class="font-weight-bold">IV. POLÍTICAS DE CANCELACION DEL SERVICIO</span><br>
                    El servicio de “LINKIDS” es proporcionado mediante la plataforma web en la página de internet www.linkids.com.mx, por lo que al acceder con su usuario y contraseña proporcionados puede tener acceso al contenido.
                    Puede cancelar su membresía a “LINKIDS” en el momento que usted desee haciendo click en la leyenda “Cancelar Suscripción” y dando click nuevamente en “Aceptar”.<br>
                    Podrá tener acceso hasta el periodo final de su mes, semestre o año, dependiendo el paquete del servicio adquirido. Su cuenta será suspendida y ya no podrá hacer uso de la misma una vez terminado el periodo adquirido.
                    Los pagos no son reembolsables por los periodos utilizados de manera parcial o por el contenido no visto de la plataforma “LINKIDS”.<br><br>

                    <span class="font-weight-bold">IV. POLÍTICA DE PRIVACIDAD Y PROTECCIÓN DE DATOS.</span><br>
                    De conformidad con lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares, el TITULAR se compromete a adoptar las medidas necesarias que estén a su alcance para asegurar la privacidad de
                    los datos personales recabados de forma que se garantice su seguridad, se evite su alteración, pérdida o tratamiento no autorizado.<br><br>

                    Además, a efecto de dar cumplimiento a lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de Particulares, todo dato personal que sea recabado a través del SITIO WEB, será tratado de
                    conformidad con los principios de licitud, calidad, finalidad, lealtad, y responsabilidad. Todo tratamiento de datos personales quedará sujeto al consentimiento de su titular. En todo caso, la utilización de datos financieros o
                    patrimoniales, requerirán de autorización expresa de sus titulares, no obstante, esta podrá darse a través del propio SITIO WEB utilizando los mecanismos habilitados para tal efecto, y en todo caso se dará la mayor
                    diligencia y cuidado a este tipo de datos. Lo mismo ocurrirá en el caso de datos personales sensibles, considerando por estos aquellos que debido a una utilización indebida puedan dar origen a discriminación o su divulgación
                    conlleve un riesgo para el titular.<br><br>

                    En todo momento se procurará que los datos personales contenidos en las bases de datos o archivos que en su caso se utilicen, sean pertinentes, correctos y actualizados para los fines para los cuales fueron recabados.
                    El tratamiento de datos personales se limitará al cumplimiento de las finalidades previstas en el Aviso de Privacidad el cual se encontrará disponible en la siguiente dirección electrónica: contacto@linkids.com.mx
                    El SITIO WEB podrá incluir hipervínculos o enlaces que permitan acceder a páginas web de terceros distintos de linkids.com.mx. Los titulares de dichos sitios web dispondrán de sus propias políticas de privacidad y protección de
                    datos, por lo cual linkids.com.mx no asume ningún tipo de responsabilidad por los datos que sean facilitados por el USUARIO a través de cualquier sitio web distinto a linkids.com.mx.
                    linkids.com.mx se reserva el derecho a modificar su Política de Privacidad, de acuerdo a sus necesidades o derivado de algún cambio en la legislación. El acceso o utilización del SITIO WEB después de dichos cambios, implicará la
                    aceptación de estos cambios.<br><br>

                    Por otra parte, el acceso al SITIO WEB puede implicar la utilización de cookies, las cuales, son pequeñas cantidades de información que se almacenan en el
                    navegador utilizado por el USUARIO. Las cookies facilitan la navegación, la hacen más amigable, y no dañan el dispositivo de navegación, para ello,
                    pueden recabar información para ingresar al SITIO WEB, almacenar las preferencias del USUARIO, así como la interacción que este tenga con el SITIO
                    WEB, como por ejemplo: la fecha y hora en la que se accede al SITIO WEB, el tiempo que se ha hecho uso de este, los sitios visitados antes y después del mismo, el número de páginas visitadas, la dirección IP de la cual accede el
                    usuario, la frecuencia de visitas, etc.<br><br>

                    Este tipo de información será utilizada para mejorar el SITIO WEB, detectar errores, y posibles necesidades que el USUARIO pueda tener, lo anterior a efecto de ofrecer a los USUARIOS servicios y contenidos de mejor calidad. En
                    todo caso, la información que se recopile será anónima y no se identificará a usuarios individuales.<br><br>

                    En caso de que el USUARIO no desee que se recopile este tipo de información deberá deshabilitar, rechazar, restringir y/o eliminar el uso de cookies en su
                    navegador de internet. Los procedimientos para realizar estas acciones pueden diferir de un navegador a otro; en consecuencia, se sugiere revisar las
                    instrucciones facilitadas por el desarrollador del navegador. En el supuesto de que rechace el uso de cookies (total o parcialmente) el USUARIO podrá
                    continuar haciendo uso del SITIO WEB, aunque podrían quedar deshabilitadas algunas de las funciones del mismo.<br>
                    Es posible que en el futuro estas políticas respecto a las cookies cambien o se actualicen, por ello es recomendable revisar las actualizaciones que se realicen
                    a los presentes TÉRMINOS Y CONDICIONES, con objetivo de estar adecuadamente informado sobre cómo y para qué utilizamos las cookies que se generan al ingresar o hacer uso del SITIO WEB.<br><br>

                    <span class="font-weight-bold">V. POLÍTICA DE ENLACES.</span><br>
                    El SITIO WEB puede contener enlaces, contenidos, servicios o funciones, de otros sitios de internet pertenecientes y/o gestionados por terceros, como por ejemplo imágenes, videos, comentarios, motores de búsqueda, etc.<br>v

                    La utilización de estos enlaces, contenidos, servicios o funciones, tiene por objeto mejorar la experiencia del USUARIO al hacer uso del SITIO WEB, sin que
                    pueda considerarse una sugerencia, recomendación o invitación para hacer uso de sitios externos. linkids.com.mx en ningún caso revisará o controlará el contenido de los sitios externos, de igual forma, no hace propios los productos,
                    servicios, contenidos, y cualquier otro material existente en los referidos sitios enlazados; por lo cual, tampoco se garantizará la disponibilidad, exactitud,
                    veracidad, validez o legalidad de los sitios externos a los que se pueda tener acceso a través del SITIO WEB. Así mismo, el TITULAR no asume ninguna
                    responsabilidad por los daños y perjuicios que pudieran producirse por el acceso o uso, de los contenidos, productos o servicios disponibles en los sitios
                    web no gestionados por linkids.com.mx a los que se pueda acceder mediante el SITIO WEB.<br><br>

                    <span class="font-weight-bold">VI. POLÍTICA EN MATERIA DE PROPIEDAD INTELECTUAL E INDUSTRIAL.</span><br>
                    linkids.com.mx por sí o como parte cesionaria, es titular de todos los derechos de propiedad intelectual e industrial del SITIO WEB, entendiendo por este el código fuente que hace posible su funcionamiento así como las imágenes,
                    archivos de audio o video, logotipos, marcas, combinaciones de colores, estructuras, diseños y demás elementos que lo distinguen. Serán, por consiguiente, protegidas por la legislación mexicana en materia de propiedad
                    intelectual e industrial, así como por los tratados internacionales aplicables.<br><br>

                    Por consiguiente, queda expresamente prohibida la reproducción, distribución, o difusión de los contenidos del SITIO WEB, con fines comerciales, en cualquier soporte y por cualquier medio, sin la autorización de linkids.com.mx.
                    El USUARIO se compromete a respetar los derechos de propiedad intelectual e industrial del TITULAR. No obstante, además de poder visualizar los elementos del SITIO WEB podrá imprimirlos, copiarlos o almacenarlos, siempre y cuando
                    sea exclusivamente para su uso estrictamente personal.<br><br>

                    Por otro lado, el USUARIO, se abstendrá de suprimir, alterar, o manipular cualquier elemento, archivo, o contenido, del SITIO WEB, y por ningún motivo
                    realizará actos tendientes a vulnerar la seguridad, los archivos o bases de datos que se encuentren protegidos, ya sea a través de un acceso restringido mediante un usuario y contraseña, o porque no cuente con los permisos para
                    visualizarlos, editarlos o manipularlos.<br>
                    En caso de que el USUARIO o algún tercero consideren que cualquiera de los contenidos del SITIO WEB suponga una violación de los derechos de protección linkids.com.mx a través de los datos de contacto disponibles en el propio
                    SITIO WEB.<br><br>

                    <span class="font-weight-bold">VII. LEGISLACIÓN Y JURISDICCIÓN APLICABLE.</span><br>
                    linkids.com.mx puede presentar las acciones civiles o penales que considere necesarias por la utilización indebida del SITIO WEB, sus contenidos, productos o servicios, o por el incumplimiento de los presentes TÉRMINOS Y
                    CONDICIONES.<br><br>

                    La relación entre el USUARIO y linkids.com.mx se regirá por la legislación vigente en México, principalmente en la Ciudad de México. De surgir cualquier
                    controversia en relación a la interpretación y/o a la aplicación de los presentes TÉRMINOS Y CONDICIONES, las partes se someterán a la jurisdicción ordinaria de los tribunales que correspondan conforme a derecho en el estado al que se
                    hace referencia.
                  </p>
                </div>
              </div>

              <div class="row align-items-center bg-image bg-home-header text-center idiomas" style="background-image: url(<?= base_url() ?>theme/svg/home/clases.svg)">
                <div class="col-12 col-sm-6 idioma-borde link-idiomas" style="margin-bottom: 0px;">
                  <a href="#">
                    <!--<img src="<?= base_url() ?>theme/svg/home/idiomas.svg" alt="Linkids" class="mx-auto d-block">-->
                    <h3 class="text-white font-weight-bold margin-bottom-20">Música y<br>matemáticas</h3>
                    </a><a class="button button-md button-blue-home" href="<?= base_url() ?>login.html" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>

                </div>
                <div class="col-12 col-sm-6 idioma-borde link-idiomas2" style="margin-bottom: 0px;">
                  <a href="#">
                    <!--<img src="<?= base_url() ?>theme/svg/home/idiomas6.svg" alt="Linkids" class="mx-auto d-block">-->
                    <h3 class="text-white font-weight-bold margin-bottom-20">Niños más<br>exitosos y hábiles</h3>
                    </a><a class="button button-md button-blue-home" href="<?= base_url() ?>login.html" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>
                </div>
              </div>

          </div>
        </div><!-- end row -->


      </div><!-- end container -->
    </div>
    <!-- end Services section -->

    <footer id="form-home">
      <div class="footer bg-blue">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-6 text-center text-md-left">
              <p class="margin-top-10 text-white">
                &copy; 2019 Linkids - ¡Evolución educativa en idiomas!<br>
                <a href="<?= base_url() ?>aviso.html" style="text-color:white; text-decoration:underline;">Aviso de Privacidad</a> |
                <a href="<?= base_url() ?>terminos.html"  style="text-color:white; text-decoration:underline;">Términos y Condiciones</a>
              </p>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right">
              <ul class="list-horizontal-unstyled">
                <li style="font-size: 24px;"><a href="https://www.facebook.com/Estimulaci%C3%B3n-Temprana-en-Idiomas-y-M%C3%BAsica-341652089375058/" target="blank" class="text-white"><i class="fab fa-facebook-f"></i></a></li>
                <li style="font-size: 24px;"><a href="https://www.instagram.com/linkidsevolucioneducativa/?igshid=19d64owl0uuub" target="blank" class="text-white"><i class="fab fa-instagram"></i></a></li>
                <li style="font-size: 24px;"><a href="https://www.youtube.com/channel/UCdFU2NHJZyDv-ShOopzhX4A" target="blank" class="text-white"><i class="fab fa-youtube"></i></a></li>
              </ul>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div>
    </footer>
