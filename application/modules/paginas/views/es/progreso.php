<?php
  if(!$this->elements->canAccess()){
        if(empty($this->user->user_vinculacion)){
            redirect('finanzas/cargarPago');
        }else{
            redirect('clases');
        }

    }
?>
<?php if(empty($_SESSION['user'])): redirect('panel');die();endif;?>
    <!-- Menu Top Desktop -->
    <header class="d-none d-lg-block">
      <?php $this->load->view('es/includes/menu-top',array(),FALSE,'paginas');?>
      <?php $this->load->view('es/includes/progreso',array(),FALSE,'paginas');?>
    </header>
    <!-- Menu Top -->
    <!-- Sidebar Navigation Desktop -->
    <!-- Sidebar Navigation Desktop -->
    <div class="sidebar-nav-left d-none d-lg-block">
      <?php $this->load->view('es/includes/sidebar-perfil.php',array(),FALSE,'paginas');?>
    </div>
    <!-- end Sidebar Navigation -->

    <!-- Menu Top Movil -->
    <header class="d-lg-none">
      <?php $this->load->view('es/includes/menu-top-perfil-movil.php',array(),FALSE,'paginas');?>
    </header>
    <!-- Menu Top Movil -->

    <div class="sidebar-wrapper-left">
      <!-- Inicia Contenido -->
      <div class="section container-white-inside">
        <div class="container">
          <div class="col-12">
            <h3 class="margin-bottom-20">Continúa con tus lecciones:</h3>
          </div>

          <div class="row text-center contact-form">

            <?php
              $toca = $this->elements->getVideosHoy();
              foreach($toca as $c):
            ?>
                <div class="col-12 col-sm-4">
                  <a href="<?= $c->activo==1?$c->link:'javascript:void(0)' ?>" <?= !$c->activo?'data-toggle="modal" data-target="#clase-bloqueada"':''; ?>>
                    <div id="gallery" class="text-white active">
                      <div id="images">
                        <div class="figure">
                          <img class="image" src="<?= $c->thumb ?>"/>
                          <div class="caption">
                            <div class="body font-weight-bold">
                              <?php if($c->activo): ?>
                                <i class="fas fa-check-circle icon-check"></i><br>¡Completado!
                              <?php else: ?>
                                <i style="color:#eee" class="fas fa-lock icon-check"></i><br>¡Pendiente!
                              <?php endif ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <div class="title-lessons">
                    <a href="<?= $c->activo==1?$c->link:'javascript:void(0)' ?>" <?= !$c->activo?'data-toggle="modal" data-target="#clase-bloqueada"':''; ?>>
                      <?= $c->nombre ?> <img src="<?= $c->idiomas->bandera ?>" alt="Idiomas">
                    </a>
                  </div>
                </div>
              <?php endforeach ?>

          </div>

        </div><!-- end container -->
      </div>

      <div class="section container-white-inside">
        <div class="container">

          <div class="col-12 text-center">
            <h3 class="margin-bottom-20">Tu progreso semanal</h3>
          </div>

          <!-- Dias de la semana Desktop -->
          <div class="col-12 text-center d-none d-lg-block">
              <div class="wizard-progress">

                <?php
                  $time = strtotime('monday this week');
                  $max_videos = $this->elements->getMaxVideos();
                  $min_videos = 3;
                ?>
                <?php
                  $this->db->order_by('fecha_apertura','ASC');
                  $primerdia = $this->db->get_where('progresos',array('user_id'=>$this->user->id));
                  if($primerdia->num_rows()>0){
                    $primerdia = $primerdia->row()->fecha_apertura;
                  }else{
                    $primerdia = date("Y-m-d");
                  }
                  for($i = 0;$i<7;$i++):
                  $hoy = strtotime(date("Y-m-d",$time).' + '.$i.' days');
                  $fecha_actual = strtotime(date("Y-m-d",time()).' 00:00');
                  $videos = $this->db->get_where('progresos',array('fecha_apertura'=>date("Y-m-d",$hoy),'user_id'=>$this->user->id))->num_rows();
                  $nivel = $videos>0?$this->elements->insignias()->row($videos-1)->imagen:$this->elements->insignias()->row(0)->imagen;
                  //Si fue ayer, se pone completado
                  $claseBorde = '';
                  $step = '';
                  if($videos==0 && $hoy<strtotime($primerdia)){
                    $claseBorde = 'image-progress-blue';
                    $tooltip = 'No habias llegado';
                  }
                  if($hoy<$fecha_actual){ // Si ya paso el dia
                    if($hoy<$fecha_actual && $videos==0 && $hoy>=strtotime($primerdia)){
                      $claseBorde = 'image-progress-blue';
                      $tooltip = 'No completado';
                    }
                    if($hoy<$fecha_actual && $videos > 0 && $videos<$max_videos){
                      $claseBorde = 'image-progress-blue';
                      $tooltip = $videos>=$min_videos?'Completado':'Vistes solo, '.$videos.' videos y no haz llegado a la meta';
                      $claseBorde = $videos>=$min_videos?'image-progress-blue':'image-progress-pink';
                      $step = $videos>=$min_videos?'complete':'';
                    }

                    if($hoy<$fecha_actual && $videos==$max_videos){
                      $claseBorde = 'image-progress-blue';
                      $tooltip = 'Completado';
                      $step = 'complete';
                    }
                  }
                  elseif($hoy==$fecha_actual){ //Si es hoy
                    //Si es hoy y no esta completo el maximo de visualizaciones se pone en progreso
                    if(date("Y-m-d",$hoy)==date("Y-m-d") && $videos<$max_videos){
                      $tooltip = 'En progreso, llevas '.$videos.' videos';
                      $claseBorde = $videos>=$min_videos?'image-progress-blue':'image-progress-pink';
                      $step = $videos>=$min_videos?'complete':'';
                    }
                    //Si es hoy y esta completa las visualizaciones
                    if(date("Y-m-d",$hoy)==date("Y-m-d") && $videos==$max_videos){
                      $claseBorde = 'image-progress-blue';
                      $tooltip = 'Completado';
                      $step = 'complete';
                    }
                  }else{ // Si es mañana
                    //Si aun no ha llegado el dia
                    if($hoy>$fecha_actual){
                      $claseBorde = 'image-progress-yellow';
                      $tooltip = 'Pendiente';
                    }
                  }


                ?>
                  <div class="step <?= $step ?> text-uppercase font-weight-bold text-blue">
                    <?= utf8_encode(strftime("%a",$hoy)) ?>
                    <div class="node" id="check-green"></div>

                    <button data-toggle="tooltip" data-placement="top" title="<?= $tooltip ?>">
                      <div class="image-progress">
                        <img src="<?= $nivel ?>" class="rounded-circle box-shadow <?= $claseBorde ?>">
                      </div>
                    </button>

                  </div>
                <?php endfor ?>
              </div>
          </div>
          <!-- Dias de la semana Desktop -->

          <!-- Dias de la semana Movil -->
          <div class="col-12 text-center d-lg-none">

            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Día se la semana</th>
                  <th scope="col">Observaciones</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><div class="image-progress"><img src="<?= base_url() ?>theme/assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-blue">Lunes</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-gray"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="<?= base_url() ?>theme/assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-blue">Martes</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-green"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="<?= base_url() ?>theme/assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-blue">Miércoles</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-green"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="<?= base_url() ?>theme/assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-blue">Jueves</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-green"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="<?= base_url() ?>theme/assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-pink">Viernes</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-gray"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="<?= base_url() ?>theme/assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-pink">Sábado</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-gray"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="<?= base_url() ?>theme/assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-yellow">Domingo</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-gray"></i><br>Observaciones</div></td>
                </tr>
              </tbody>
            </table>

          </div>
          <!-- Dias de la semana Movil -->
          <?php $racha = $this->elements->getRacha(); ?>
          <div class="row col-12 col-md-8 offset-md-2 text-center margin-top-10">
              <div class="col-6 col-sm-6 number-profile">
                <h2 class="font-weight-bold"><?= $racha[0] ?></h2><small class="text-blue text-uppercase">Mejor Racha</small>
              </div>

              <div class="col-6 col-sm-6 number-profile">
                <h2 class="font-weight-bold"><?= $racha[1] ?></h2><small class="text-blue text-uppercase">Racha Actual</small>
              </div>

              <!--
              <div class="col-12 col-sm-4">
                <button class="button button-sm button-blue margin-top-10" type="submit">¡Continuar!</button>
              </div>-->
          </div>

        </div><!-- end container -->
      </div>

      <!-- Termina Contenido -->

    </div><!-- end sidebar-wrapper-left -->

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
      <?php $this->load->view('es/includes/modales',array(),FALSE,'paginas'); ?>

  </body>
</html>
