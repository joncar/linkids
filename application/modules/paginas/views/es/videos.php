<link rel="stylesheet" href="<?= base_url() ?>js/reproductor/mediaelementplayer.css">
<!-- Menu Top Desktop -->
    <header class="d-none d-lg-block">
      <?php $this->load->view('es/includes/menu-top',array(),FALSE,'paginas');?>
      <?php $this->load->view('es/includes/progreso',array(),FALSE,'paginas');?>
    </header>
    <!-- Menu Top -->

    <!-- Sidebar Navigation Desktop -->
    <div class="sidebar-nav-left d-none d-lg-block sticky">
      <?php $this->load->view('es/includes/sidebar-videos',array(),FALSE,'paginas');?>
    </div>
    <!-- end Sidebar Navigation -->

    <!-- Menu Top Movil -->
    <header class="d-lg-none">
      <?php $this->load->view('es/includes/menu-top-videos-movil',array(),FALSE,'paginas');?>
      <div class="alert text-white">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <div class="col-md-6 offset-md-3 text-center">
            <div class="text-message-top">Tu progreso de hoy:</div>
            <div class="wizard-progress-top" id="progress-top">
              <div class="step complete"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-black"></div></div>
            </div>
        </div>
      </div>
    </header>
    <!-- Menu Top Movil -->

    <!-- Termina Contenido -->
    <div class="sidebar-wrapper-left">
      <!-- Inicia Contenido -->

      <!-- Full Video -->
      <div class="container-white-video">
        <div class="container">

          <div class="row">
            <div class="col-12 col-sm-12">
              <div class="videoWrapper mx-auto d-block">
                  <audio id="player2" controls="true" style="width:100%; height:auto;">
                    <source src="<?= $video->fichero ?>" type="audio/mp3" media="">
                  </audio>
                  <input type="hidden" name="lang" value="es">
                  <input type="hidden" name="stretching" value="responsive">
              </div>
            </div>
          </div><!-- end row -->

        </div>
      </div>
      <!-- Full Video -->


      <div class="container">
        <div class="row margin-top-30">
          <div class="col-12 col-sm-12 titulo-video description-video">
            <img src="<?= base_url() ?>theme/assets/images/banderas/United-States-of-America(USA).png" alt="Idiomas" class="image-title-video-flag-videos"><br>
            <small>Reproduciendo:</small><br>
            <span class="font-weight-bold text-blue"><?= $video->nombre ?></span>
          </div>
          <div class="col-12 col-sm-12 description-video">
            <p>
              <span class="font-weight-bold">Descripción:</span>
              <?= $video->descripcion ?>
            </p>
          </div>
        </div><!-- end row -->
      </div>


    </div>


<script src="<?= base_url() ?>js/reproductor/mediaelement-and-player.js"></script>
<script src="<?= base_url() ?>js/reproductor/renderers/dailymotion.js"></script>
<script src="<?= base_url() ?>js/reproductor/renderers/facebook.js"></script>
<script src="<?= base_url() ?>js/reproductor/renderers/soundcloud.js"></script>
<script src="<?= base_url() ?>js/reproductor/renderers/twitch.js"></script>
<script src="<?= base_url() ?>js/reproductor/renderers/vimeo.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/cs.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/de.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/es.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/fa.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/fr.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/hr.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/hu.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/it.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/ja.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/ko.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/ms.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/nl.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/pl.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/pt.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/ro.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/ru.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/sk.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/sv.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/uk.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/zh-cn.js"></script>
<script src="<?= base_url() ?>js/reproductor/lang/zh.js"></script>
<script src="<?= base_url() ?>js/reproductor/demo.js"></script>
<script>
  
</script>