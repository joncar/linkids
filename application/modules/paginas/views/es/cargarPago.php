
<!-- Menu Top Desktop -->
<header class="d-none d-lg-block">
	<?php $this->load->view('es/includes/menu-top',array(),FALSE,'paginas');?>
</header>

<header class="d-lg-none">
	<?php $this->load->view('es/includes/menu-top-perfil-movil',array(),FALSE,'paginas');?>
	<div class="alert text-white">
		<span class="closebtn" onclick="this.parentElement.style.display='none';"><i class="far fa-times-circle"></i></span>
	</div>
</header>

<!-- Inicia Contenido -->
<div class="section container-white-top" style="background-image: url(<?= base_url() ?>theme/svg/home/header.svg); background-size: cover; background-position: bottom;">
	<div class="container">			
		<div class="col-12 text-center">
			<h3 class="margin-bottom-20 text-white">Selecciona el plan que más se adapte a tus necesidades:</h3>
		</div>
		<div class="col-12 text-center margin-bottom-30">
			<div class="row">
				<?php foreach($this->elements->membresias()->result() as $m): ?>
					<div class="col-12 col-md-4">
						<div class="container-membresia">
						  <img src="<?= $m->foto ?>" class="card-img-top">
						  <div class="card-body">
						    <h5 class="card-title text-white"><?= $m->nombre ?></h5>
						    <p class="card-text margin-bottom-20"><h3 class="text-yellow"><?= moneda($m->precio,'$') ?></h3></p>
						    <?php if(empty($m->precio) || $m->precio<1): ?>
						    	<a href="#contacto-escuelas" data-toggle="modal" class="button button-md button-blue-home" title="Seleccionar Membresía">Seleccionar</a>
					    	<?php else: ?>
						    	<a href="javascript:seleccionar(<?= $m->id ?>,<?= $m->precio ?>,'<?= $m->nombre ?>')" class="button button-md button-blue-home" title="Seleccionar Membresía">Seleccionar</a>
							<?php endif ?>
							<?php								
								if(!empty($_SESSION['pago']) && $_SESSION['pago']==$m->id){
									echo '<script>window.onload = function(){seleccionar('.$m->id.','.$m->precio.', \''.$m->nombre.'\');};</script>';
									unset($_SESSION['pago']);
								}
							?>	
						  </div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>

		<div class="col-12 text-center">
			<h4 class="margin-bottom-20 text-white">Si eres una escuela o tienes un grupo grande de usuarios que quisieras registrar, contáctanos para ofrecerte un plan grupal.</h4>
			<a class="button button-md button-green-home text-white" data-toggle="modal" data-target="#contacto-escuelas" title="Contacto para escuelas"><i class="fas fa-hand-point-up icon-btn-home"></i>¡Contáctanos!</a>
		</div>

	</div><!-- end container -->
</div>
		<!-- Termina Contenido -->

<!-- Cambiar foto de perfil -->
<div class="modal fade" id="cargarPago" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-lg">

      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            Selecciona un medio de pago
        </div>
      </div>

      <div class="modal-body">
      	<div class="row">
        	<div class="col-12 col-md-6" style="cursor:pointer;" onclick="seleccionarPago(1)">
        		<img src="<?= base_url() ?>img/paypal.png" alt="" style="width:100%">
        	</div>
    	</div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn button-sm button-blue" data-dismiss="modal">Cancelar</button>        
      </div>

    </div>
  </div>
</div>
<!-- Cambiar foto de perfil -->

<script src='<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/slim.croppic.min.js'></script>
<script src='<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/slim.cropper.js'></script>
<script src="https://checkout.stripe.com/checkout.js"></script>
<script>
	var elegido = 0;
	var precio = 0;
	var nombre = '';
	function seleccionar(id,pre,nom){
		elegido = id;
		precio = pre;
		nombre = nom;
		//$("#cargarPago").modal('toggle'); Descomentar cnado se tengan mas pasarelas de pago
		seleccionarPago(1);
	}

	function seleccionarPago(id){		
		switch(id){
			case 1://Paypal
				document.location.href="<?= base_url('finanzas/pagarConPaypal/') ?>/"+elegido;
			break;
			case 2:
				goPayStripe();
			break;
		}
	}

	<?php 
    	$key = 'pk_test_zDUfEk0luTZ6FCBohuemZNe000hQgMSHan'; 
    	//$key = 'pk_live_ypyQSU4xEOkNc1sqDQdEn89p'; 	
    ?>
    function goPayStripe(){
    	var handler = StripeCheckout.configure({
		  key: '<?= $key ?>',
		  image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
		  locale: 'auto',
		  token: function(token) {
		    document.location.href="<?= base_url('finanzas/pagarConStripe/') ?>/"+elegido+'?token='+JSON.stringify(token);
		  }
		});
		var total = precio;
		total*= 100;
		handler.open({
		    name: nombre,
		    description: '2 widgets',
		    currency: 'MXN',
		    amount: total
		 });
    }
</script>