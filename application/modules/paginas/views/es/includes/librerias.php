<!-- ***** JAVASCRIPTS ***** -->
<!-- Libraries -->
<script src="<?= base_url() ?>theme/assets/plugins/bootstrap/popper.min.js"></script>
<!-- Plugins -->
<script src="<?= base_url() ?>theme/assets/plugins/bootstrap/bootstrap.min.js"></script>
<!-- <script src="<?= base_url() ?>theme/assets/plugins/appear.min.js"></script> -->
<!--<script src="<?= base_url() ?>theme/assets/plugins/easing.min.js"></script>-->
<!--<script src="<?= base_url() ?>theme/assets/plugins/retina.min.js"></script>-->
<!--<script src="<?= base_url() ?>theme/assets/plugins/countdown.min.js"></script>-->
<!--<script src="<?= base_url() ?>theme/assets/plugins/imagesloaded.pkgd.min.js"></script>-->
<!--<script src="<?= base_url() ?>theme/assets/plugins/isotope.pkgd.min.js"></script>-->
<script src="<?= base_url() ?>theme/assets/plugins/jarallax/jarallax.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/jarallax/jarallax-video.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/jarallax/jarallax-element.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/magnific-popup/magnific-popup.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
<!-- Place your API Key in 'YOUR_API_KEY'
You can get Google API key here: 'https://developers.google.com/maps/documentation/javascript/get-api-key' -->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap"></script>-->
<!--<script src="<?= base_url() ?>theme/assets/plugins/gmaps.min.js"></script>-->
<!-- Scripts -->
<script src="<?= base_url() ?>theme/assets/js/functions.js"></script>
<script src="<?= base_url() ?>js/frame.js"></script>




<!-- Progress Bar Sidebar -->
<script>
  var list = document.getElementById('progress'),
    next = document.getElementById('next'),
    clear = document.getElementById('clear');
    if(list){
        var children = list.children;
    }
   var completed = 0;
if(next){
    // simulate activating a node
    next.addEventListener('click', function() {

        // count the number of completed nodes.
        completed = (completed === 0) ? 1 : completed + 2;
        if (completed > children.length) return;

        // for each node that is completed, reflect the status
        // and show a green color!
        for (var i = 0; i < completed; i++) {
            children[i].children[0].classList.remove('grey');
            children[i].children[0].classList.add('green');

            // if this child is a node and not divider,
            // make it shine a little more
            if (i % 2 === 0) {
                children[i].children[0].classList.add('activated');
            }
        }

    }, false);

    // clear the activated state of the markers
    clear.addEventListener('click', function() {
        for (var i = 0; i < children.length; i++) {
            children[i].children[0].classList.remove('green');
            children[i].children[0].classList.remove('activated');
            children[i].children[0].classList.add('grey');
        }
        completed = 0;
    }, false);
}
</script>
<!-- Progress Bar Sidebar -->
