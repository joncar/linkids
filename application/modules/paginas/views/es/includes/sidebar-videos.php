
<button class="sidebar-nav-toggle">
    <span class="lines"></span>
</button>

<div class="sidebar-nav-content bg-white" id="sidebar-videos">

    <form action="" class="search">
      <div class="field">
        <input type="text" class="input-search" id="input-search-sidebar" name="input-search-sidebar" required>
        <label for="input-search-sidebar">Buscar</label>
        <ul class="autocompleteResponse">
          <li><a href="#">Clases de prueba alan</a></li>
          <li><a href="#">Clases de prueba alan</a></li>
        </ul>
      </div> <!-- /field -->
    </form>

    <div class="margin-top-20">
      <!-- List Video -->
      <?php foreach($this->elements->videos_ocacionales()->result() as $v): ?>
        <a href="<?= $v->link ?>" title="Video Linkids">
          <div class="row video-list align-items-center">
          
            <div class="col-12 col-sm-2 no-padding" style="margin-bottom: 0px;">
              <img src="<?= $v->thumb ?>" alt="Imagen Video">
            </div>

            <div class="col-12 col-sm-8" style="margin-bottom: 0px;">
              <span class="font-weight-bold"><?= $v->nombre ?></span><br><?= $v->autor ?>
            </div>
            <div class="col-12 col-sm-2 no-padding text-center" style="margin-bottom: 0px;"><?= $v->duracion ?></div>
          </div>
        </a>
        <!-- List Video -->
      <?php endforeach ?>

     
    </div>

</div><!-- end sidebar-nav-content -->

<script>
  var onsearch = undefined
  $(document).on('keyup','#input-search-sidebar',function(){
      var valor = $(this).val();
      if(valor.length>=3){
        if(typeof(onsearch)!=='undefined' && onsearch!=null){
          onsearch.abort();
        }
        onsearch = $.post('<?= base_url('clases/searchVideos') ?>',{q:valor},function(data){
          $(".autocompleteResponse").addClass('in');
          $(".autocompleteResponse").html(data);
          onsearch = undefined;
        });        
      }
  });
  $(document).on('click',function(){
      $(".autocompleteResponse").removeClass('in');
  });
</script>