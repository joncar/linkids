<ul id="progress-sidebar">
	<?php foreach($this->elements->clases_prueba(array(),FALSE)->result() as $v): ?>
		<li class="margin-bottom-50">
			<p class="text-sidebar-bar">
				<i class="fas fa-play icono-star-sidebar-azul"></i>
				<span>¡Comenzar!</span><br>
				<a href="<?= $v->fichero ?>" title="Clase Linkids" class="overflow-ellipsis lista-sidebar">
					<?= $v->nombre ?>
					<img src="<?= $v->bandera ?>" alt="Idiomas">
				</a>
			</p>
		</li>
	<?php endforeach ?>
</ul>