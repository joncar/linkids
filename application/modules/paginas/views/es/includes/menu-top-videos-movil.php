<!-- navbar -->
<nav class="navbar navbar-sticky poppins">
  <div class="container text-center">

    <ul class="nav">
      <div class="name-movil"><span>Bienvenido a Linkids</span><br>Alan Torres</div>
      <li>
        <form action="" class="search">
          <div class="field">
            <input type="text" class="input-search" id="input-search" name="input-search" required>
            <label for="input-search">Buscar</label>
          </div> <!-- /field -->
        </form>
      </li>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>"><i class="fas fa-user-circle text-pink"></i> Iniciar sesión/Perfil</a></li>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>clases"><i class="fas fa-play-circle text-pink"></i> Clases</a></li>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>videos_ocacionales"><i class="fas fa-headphones-alt text-pink"></i> Música</a></li>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>"><i class="fab fa-fort-awesome text-pink"></i> Sobre Nosotros</a></li>
      <li class="progreso-movil" style="margin-bottom: 20px;">
        <div class="font-weight-bold text-center margin-bottom-20">Nombre de la clase</div>

        <!-- List Video -->
        <div class="text-center margin-bottom-20">
          <a href="#" title="Video Linkids">
            <div class="row video-list text-left">
              <div class="col-4"><img src="<?= base_url() ?>theme/assets/images/videos/video-01.jpg" alt="Imagen Video" class="mx-auto d-block"></div>
              <div class="col-8"><span class="font-weight-bold text-left">Animal songs compil…</span><br>Jennifer T. Coy 03:16</div>
            </div>
          </a>
        </div>
        <!-- List Video -->

        <!-- List Video -->
        <div class="text-center margin-bottom-20">
          <a href="#" title="Video Linkids">
            <div class="row video-list text-left">
              <div class="col-4"><img src="<?= base_url() ?>theme/assets/images/videos/video-01.jpg" alt="Imagen Video" class="mx-auto d-block"></div>
              <div class="col-8"><span class="font-weight-bold text-left">Animal songs compil…</span><br>Jennifer T. Coy 03:16</div>
            </div>
          </a>
        </div>
        <!-- List Video -->

        <!-- List Video -->
        <div class="text-center margin-bottom-20">
          <a href="#" title="Video Linkids">
            <div class="row video-list text-left">
              <div class="col-4"><img src="<?= base_url() ?>theme/assets/images/videos/video-01.jpg" alt="Imagen Video" class="mx-auto d-block"></div>
              <div class="col-8"><span class="font-weight-bold text-left">Animal songs compil…</span><br>Jennifer T. Coy 03:16</div>
            </div>
          </a>
        </div>
        <!-- List Video -->

        <!-- List Video -->
        <div class="text-center margin-bottom-20">
          <a href="#" title="Video Linkids">
            <div class="row video-list text-left">
              <div class="col-4"><img src="<?= base_url() ?>theme/assets/images/videos/video-01.jpg" alt="Imagen Video" class="mx-auto d-block"></div>
              <div class="col-8"><span class="font-weight-bold text-left">Animal songs compil…</span><br>Jennifer T. Coy 03:16</div>
            </div>
          </a>
        </div>
        <!-- List Video -->

        <!-- List Video -->
        <div class="text-center margin-bottom-20">
          <a href="#" title="Video Linkids">
            <div class="row video-list text-left">
              <div class="col-4"><img src="<?= base_url() ?>theme/assets/images/videos/video-01.jpg" alt="Imagen Video" class="mx-auto d-block"></div>
              <div class="col-8"><span class="font-weight-bold text-left">Animal songs compil…</span><br>Jennifer T. Coy 03:16</div>
            </div>
          </a>
        </div>
        <!-- List Video -->

      </li>
      <li class="nav-item salir-movil"><a class="nav-link" href="<?= base_url() ?>" style="color: white;">Salir de Linkids <i class="fas fa-sign-out-alt"></i></a></li>
    </ul><!-- end nav -->

    <a class="navbar-brand img-menu-top" href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/assets/images/logo-linkids.png" alt="Logo Linkids" srcset="<?= base_url() ?>theme/svg/logo-login.svg"></a>

    <!-- Nav Toggle button -->
    <button class="nav-toggle-btn">
        <span class="lines"></span>
    </button>

  </div><!-- end container -->
</nav><!-- end navbar -->
