<!-- Nuevo Menu -->
<nav class="navbar"><!-- add 'navbar-dark/navbar-grey/navbar-transparent/navbar-transparent-dark' -->
  <div class="container">
    <a class="navbar-brand img-menu-top" href="<?= base_url() ?>">
      <img src="<?= base_url() ?>theme/svg/home/logo.png" alt="Logo Linkids" style="width: 40%;">
    </a>

    <ul class="nav">
      <li class="nav-item"><a class="nav-link" style="color: #0261dc;" href="<?= base_url() ?>#planes"><i class="fas fa-play-circle text-pink"></i> Me interesa</a></li>      
    </ul><!-- end nav -->
    <!-- Nav Toggle button -->
    <button class="nav-toggle-btn">
            <span class="lines"></span>
        </button><!-- toggle button will show when screen resolution is less than 992px -->
  </div><!-- end container -->
</nav>
<!-- end navbar -->
