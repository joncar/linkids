<!-- navbar -->
<nav class="navbar navbar-sticky poppins">
  <div class="container text-center">

    <ul class="nav">
      <div class="name-movil"><span>Bienvenido a Linkids</span><br><?= $this->user->nombre.' '.$this->user->apellido_paterno ?></div>
      <li>
        <form action="" class="search">
          <div class="field">
            <input type="text" class="input-search" id="input-search-sidebar" name="input-search-sidebar" required>
            <label for="input-search-sidebar">Buscar</label>
            <ul class="autocompleteResponse">
              <li><a href="#">Clases de prueba alan</a></li>
              <li><a href="#">Clases de prueba alan</a></li>
            </ul>
          </div> <!-- /field -->
        </form>
      </li>
      <?php if($this->user->log): ?>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>perfil.html"><i class="fas fa-user-circle" style="color:#fcb548"></i> Perfil</a></li>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>clases"><i class="fas fa-play-circle" style="color:#fcb548"></i> Clases</a></li>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>videos_ocacionales"><i class="fas fa-headphones-alt" style="color:#fcb548"></i> Música</a></li>      
      <?php else: ?>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>"><i class="fas fa-user-circle" style="color:#fcb548"></i> Iniciar sesión</a></li>
      <?php endif ?>            
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>"><i class="fab fa-fort-awesome" style="color:#fcb548"></i> Sobre Nosotros</a></li>
      <?php 
          if($this->user->log):
            $videos = $this->elements->getCurrentVideoData(); 
          if($videos->num_rows()>0):    $videos = $this->elements->videos(array('id'=>$videos->row()->id))->row();     
      ?>
      <li class="progreso-movil" style="margin-bottom: 20px;">
        <div class="font-weight-bold text-center"><?= $videos->nombre ?></div>
        <img src="<?= $videos->thumb ?>"/ alt="Videos Linkids" style="width: 40%;">
        <p class="text-sidebar-bar">
          <span style="color:green;">
            <?php 
              if($videos->activo && !$videos->current){
                echo '¡Completada!';
              }elseif($videos->activo && $videos->current){
                echo '¡Comenzar!';
              }else{
                 echo '¡Bloqueada!';
              }
            ?></span><br>
          <a href="<?= $videos->link ?>" title="Clase Linkids"><?= $videos->idiomas->nombre ?> <img src="<?= $videos->bandera ?>" alt="Idiomas"></a>
          <div class="margin-top-20 list-menu-sidebar list-movil">



            <ul>
              
              <?php foreach($this->elements->videos()->result() as $n=>$c): ?>
                <?php if($c->activo && !$c->current): ?>
                  <li>
                  <p class="text-sidebar-bar">
                      <i class="fas fa-star icono-star-sidebar-azul"></i>
                      <span>¡Completada!</span><br>
                      <a href="<?= $c->link ?>" title="Clase Linkids" class="overflow-ellipsis lista-sidebar">
                        <?= $c->nombre ?> 
                        <img src="<?= $c->idiomas->bandera ?>" alt="Idiomas">
                      </a>
                  </p>
                  <div class="margin-top-10 list-menu-sidebar"></div>
                </li>
                <li><div class="divider blue"></div></li>
                <?php elseif($c->activo && $c->current): ?>
                <li>
                  <p class="text-sidebar-bar">
                      <i class="fas fa-play icono-star-sidebar-azul"></i> 
                      <span>¡Comenzar!</span><br>
                      <a href="<?= $c->link ?>" title="Clase Linkids" class="overflow-ellipsis lista-sidebar">
                        <?= $c->nombre ?> 
                        <img src="<?= $c->idiomas->bandera ?>" alt="Idiomas">
                      </a>
                  </p>
                  <div class="margin-top-10 list-menu-sidebar"></div>
                </li>
                <li><div class="divider blue"></div></li>
                <?php else: ?>
                <li>
                  <p class="text-sidebar-bar">
                      <i class="fas fa-play icono-star-sidebar-gris"></i> 
                      <span style="color: #BEBEBE;">¡Bloqueada!</span><br>
                      <a href="#clase-bloqueada" data-toggle="modal" title="Clase Linkids" class="overflow-ellipsis lista-sidebar icono-star-sidebar-gris-texto" style="color: #BEBEBE;">
                        <?= $c->nombre ?> 
                        <img src="<?= $c->idiomas->bandera ?>" alt="Idiomas">
                      </a>
                  </p>
                  <div class="margin-top-10 list-menu-sidebar"></div>
                </li>
                <li><div class="divider blue"></div></li>
                <?php endif ?>
                <?php endforeach ?>
              
            </ul>






          </div>
        </p>
      </li>
      <?php endif ?>    
      <li class="nav-item salir-movil"><a class="nav-link" href="<?= base_url() ?>main/unlog" style="color: white;">Salir de Linkids <i class="fas fa-sign-out-alt"></i></a></li>
    <?php endif ?>      
    </ul><!-- end nav -->

    <a class="navbar-brand img-menu-top" href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/assets/images/logo-linkids.png" alt="Logo Linkids" srcset="<?= base_url() ?>theme/svg/logo-login.svg"></a>

    <!-- Nav Toggle button -->
    <button class="nav-toggle-btn">
        <span class="lines"></span>
    </button>

  </div><!-- end container -->
</nav><!-- end navbar -->
