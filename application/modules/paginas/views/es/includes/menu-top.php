<!-- Nuevo Menu -->
<nav class="navbar"><!-- add 'navbar-dark/navbar-grey/navbar-transparent/navbar-transparent-dark' -->
  <div class="container">
    <a class="navbar-brand img-menu-top" href="<?= base_url() ?>">
      <img src="<?= base_url() ?>theme/svg/home/logo.png" alt="Logo Linkids" style="width: 40%;">
    </a>

    <ul class="nav">
      <li class="nav-item"><a class="nav-link" style="color: #0261dc;" href="<?= base_url() ?>clases"><i class="fas fa-play-circle text-pink"></i> Clases</a></li>
      <li class="nav-item"><a class="nav-link" style="color: #0261dc;" href="<?= base_url() ?>videos_ocacionales"><i class="fas fa-headphones-alt text-pink"></i>  Música</a></li>
      <li class="nav-item"><a class="nav-link" style="color: #0261dc;" href="<?= base_url() ?>"><i class="fab fa-fort-awesome text-pink"></i> Sobre Nosotros</a></li>

      <!-- Sub Dropdown -->
      <li class="nav-item nav-dropdown">
        <a class="nav-link img-profile-top" href="#"><img src="<?= $this->user->getFoto() ?>" class="rounded-circle box-shadow borde-img-top" alt="Perfil Linkids"></a>
          <ul class="dropdown-menu">
            <li><a href="<?= base_url('perfil.html') ?>"><i class="fas fa-user-circle"></i> Ver mi Perfil</a></li>
            <li><a href="<?= base_url('progreso.html') ?>"><i class="fas fa-user-circle"></i> Ver mi Progreso</a></li>
            <li><a href="<?= base_url('main/unlog') ?>"><i class="fas fa-sign-out-alt"></i> Salir</a></li>
          </ul>
      </li>

    </ul><!-- end nav -->
    <!-- Nav Toggle button -->
    <button class="nav-toggle-btn">
            <span class="lines"></span>
        </button><!-- toggle button will show when screen resolution is less than 992px -->
  </div><!-- end container -->
</nav>
<!-- end navbar -->
