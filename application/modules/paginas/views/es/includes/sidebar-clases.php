<button class="sidebar-nav-toggle">
    <span class="lines"></span>
</button>

<div class="sidebar-nav-content bg-white text-blue">

    <form action="" class="search">
      <div class="field">
        <input type="text" class="input-search" id="input-search-sidebar" name="input-search-sidebar" required>
        <label for="input-search-sidebar">Buscar</label>
        <ul class="autocompleteResponse">
          <li><a href="#">Clases de prueba alan</a></li>
          <li><a href="#">Clases de prueba alan</a></li>
        </ul>
      </div> <!-- /field -->
    </form>

    <div class="margin-top-20">
      <ul id="progress-sidebar">

          <?php foreach($this->elements->videos()->result() as $n=>$c): ?>
          <?php if($c->activo && !$c->current): ?>
            <li class="margin-bottom-50">
            <p class="text-sidebar-bar">
                <i class="fas fa-star icono-star-sidebar-azul"></i>
                <span>¡Completada!</span><br>
                <a href="<?= $c->link ?>" title="Clase Linkids" class="overflow-ellipsis lista-sidebar">
                  <?= $c->nombre ?> 
                  <img src="<?= $c->idiomas->bandera ?>" alt="Idiomas">
                </a>
            </p>
          </li>
          <?php elseif($c->activo && $c->current): ?>
          <li class="margin-bottom-50">
            <p class="text-sidebar-bar">
                <i class="fas fa-play icono-star-sidebar-azul"></i> 
                <span>¡Comenzar!</span><br>
                <a href="<?= $c->link ?>" title="Clase Linkids" class="overflow-ellipsis lista-sidebar">
                  <?= $c->nombre ?> 
                  <img src="<?= $c->idiomas->bandera ?>" alt="Idiomas">
                </a>
            </p>
          </li>
          <?php else: ?>
          <li class="margin-bottom-50 icono-star-sidebar-gris-texto">
            <p class="text-sidebar-bar">
                <i class="fas fa-play icono-star-sidebar-gris"></i> 
                <span style="color: #BEBEBE;">¡Bloqueada!</span><br>
                <a href="#clase-bloqueada" data-toggle="modal" title="Clase Linkids" class="overflow-ellipsis lista-sidebar icono-star-sidebar-gris-texto" style="color: #BEBEBE;">
                  <?= $c->nombre ?> 
                  <img src="<?= $c->idiomas->bandera ?>" alt="Idiomas">
                </a>
            </p>
          </li>
          <?php endif ?>
          <?php endforeach ?>

      </ul>
    </div>

</div><!-- end sidebar-nav-content -->

<script>
  var onsearch = undefined
  $(document).on('keyup','#input-search-sidebar',function(){
      var valor = $(this).val();
      if(valor.length>=3){
        if(typeof(onsearch)!=='undefined' && onsearch!=null){
          onsearch.abort();
        }
        onsearch = $.post('<?= base_url('clases/search') ?>',{q:valor},function(data){
          $(".autocompleteResponse").addClass('in');
          $(".autocompleteResponse").html(data);
          onsearch = undefined;
        });
      }
  });
  $(document).on('click',function(){
      $(".autocompleteResponse").removeClass('in');
  });
</script>
