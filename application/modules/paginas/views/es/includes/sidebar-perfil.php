<button class="sidebar-nav-toggle">
    <span class="lines"></span>
</button>

<div class="sidebar-nav-content bg-gradient-nav-perfil" style="height: auto;">

  <div class="margin-profile-sidebar">
      <div class="img-profile-sidebar">
        <img src="<?= $this->user->getFoto() ?>" class="rounded-circle box-shadow mx-auto d-block" alt="Perfil Linkids">
      </div>

      <div class="name-sidebar-profile text-white">
        <?= $this->user->nombre ?><br>
        <small>Plunkett Kinder</small>
      </div>

      <div class="animacion-sidebar-menu">
        <div class="section container-white-none">
          <span class="cloud cloud--small"></span>
          <span class="cloud cloud--medium"></span>
          <span class="cloud cloud--large"></span>
        </div>
      </div>
  </div>

</div><!-- end sidebar-nav-content -->
