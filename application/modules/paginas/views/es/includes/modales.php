<!-- Cambiar foto de perfil -->
<div class="modal fade" id="edit-profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php /*include('theme/svg/logo.svg'); */ ?>
        </div>
      </div>

      <div class="modal-body">
        <div class="profile profile-pic">
          <div class="image mx-auto d-block">
            <div class="circle-1"></div>
            <div class="circle-2"></div>
            <img src="<?= base_url() ?>theme/assets/jessica-potter.jpg" width="70" height="70" alt="Perfil de usuario">
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn button-sm button-blue" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn button-sm button-blue">Guardar mi foto</button>
      </div>

    </div>
  </div>
</div>
<!-- Cambiar foto de perfil -->

<!-- Clases Bloqueadas -->
<div class="modal fade" id="programa-completo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-felicitacion">

      <div class="modal-body text-center">
          <div class="margin-bottom-20"><?php /*include('theme/svg/logo.svg'); */ ?></div>
          <img src="<?= base_url() ?>theme/svg/home/logo.png" alt="Logo Linkids" style="width: 30%;"><br><br>
          <div class="margin-bottom-30 text-white text-center"><p style="text-align: center;">Conoce el Programa Completo Linkids</p></div>
          <img src="<?= base_url() ?>theme/assets/images/BG/bg-video-home.jpg" alt="Logo Linkids"><br><br>
          <a href="<?= base_url() ?>theme/assets/images/calendario.pdf" target="blank"><button type="button" class="btn button-sm button-orange" style="width: 100%;">Descargar</button></a>
      </div>

      <!--
      <div class="modal-footer text-center">
        <button type="button" class="btn button-sm button-orange" data-dismiss="modal" style="width: 100%;">Cerrar</button>
      </div>-->

    </div>
  </div>
</div>
<!-- Recuperar mi contraseña -->

<!-- Ver Programa completo -->
<div class="modal fade" id="clase-bloqueada" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-felicitacion">

      <div class="modal-body text-center">
          <div class="margin-bottom-20"><?php /*include('theme/svg/logo.svg'); */ ?></div>
          <img src="<?= base_url() ?>theme/svg/home/logo.png" alt="Logo Linkids" style="width: 30%;"><br><br>
          <div class="font-weight-bold text-yellow">Lo sentimos :(</div>
          <div class="margin-bottom-30 text-white"><p>Aún no has llegado a esta clase, intenta de nuevo más adelante.</p></div>
      </div>

      <div class="modal-footer text-center">
        <button type="button" class="btn button-sm button-orange" data-dismiss="modal" style="width: 100%;">Aceptar</button>
      </div>

    </div>
  </div>
</div>
<!--  Ver Programa completo -->

<!-- Recuperar mi contraseña -->
<div class="modal fade" id="olvide-contrasena" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-felicitacion">
      <form method="post" id="form-modal" action="registro/forget" onsubmit="return sendForm(this,'.response');">
          <div class="modal-body text-center">
              <div class="margin-bottom-20"><?php /*include('theme/svg/logo.svg'); */ ?></div>
              <div class="font-weight-bold text-yellow">Recuperar mi contraseña</div>
              <div class="margin-bottom-30 text-white"><p>Ingresa el correo asociado con su cuenta</p></div>
              <label id="label-input">Correo registrado</label>
              <input type="email" id="email" name="email" placeholder="user@domain.com" required style="border: 0px;padding: 10px;">
              <div class="response"></div>
          </div>

          <div class="modal-footer text-center">
            <button type="submit" class="btn button-sm button-orange" style="width: 100%;">Enviar</button>
          </div>
          <button class="text-center" type="button" data-dismiss="modal" style="width: 100%;">Cancelar</button>
       </form>
    </div>
  </div>
</div>
<!-- Recuperar mi contraseña -->


<!-- Recuperar mi contraseña2 -->
<div class="modal fade" id="olvide-contrasena2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-felicitacion">

      <div class="modal-body text-center">
          <div class="margin-bottom-20"><?php /*include('theme/svg/logo.svg');*/ ?></div>
          <div class="font-weight-bold text-yellow">Recuperar mi contraseña</div>
          <div class="margin-bottom-30 text-white"><p>Hemos enviado un mensaje de confirmación a su correo electróinico desde el cuál podrá continuar con el reestable-cimiento de su contraseña.</p></div>
      </div>

      <div class="modal-footer text-center">
        <button type="button" class="btn button-sm button-orange" data-dismiss="modal" style="width: 100%;">Cerrar</button>
      </div>

    </div>
  </div>
</div>
<!-- Recuperar mi contraseña2-->

<?php if($this->user->log): ?>
<!-- Felicidades-->
<div class="modal fade" id="felicidades" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-felicitacion">

      <div class="modal-body">
        <div id="contenido-felicitacion">
          <div class="margin-bottom-20"><?php /*include('theme/svg/logo.svg'); */ ?></div>
          <div class="titulo-modal-felicitacion">
            Felicidades,<br><?= $this->user->nombre.' '.$this->user->apellido_paterno ?><br><span>Haz terminado un nivel más de tu idioma favorito.</span>
          </div>
        </div>
        <canvas id="canvas"></canvas>
      </div>

      <div class="modal-footer text-center">
        <button type="button" class="btn button-sm button-blue" data-dismiss="modal" style="width: 100%;">Aceptar</button>
      </div>

    </div>
  </div>
</div>
<script src="<?= base_url() ?>theme/assets/js/confetti.js"></script>
<!-- Felicidades -->
<?php endif ?>





<!-- Contacto escuela -->
<div class="modal fade" id="contacto-escuelas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-felicitacion">


      <form method="post" id="form-modal" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'.response');">
          <div class="modal-body text-center">
              <div class="margin-bottom-20"><?php /*include('theme/svg/logo.svg'); */ ?></div>

              <h2 class="subtitulo-home font-weight-bold text-white"><div class="font-cursiva">Contacto para escuelas</div></h2>

              <input type="hidden" name="extras[asunto]" value="Solicitud de información para contrato escolar">
              <label id="label-input" style="text-align: left;float: left; font-size: 18px;">Nombre completo<sup>*</sup></label>
              <input type="text" id="nombre" name="nombre" placeholder="Nombre" required style="border: 0px;padding: 10px;">
              <label id="label-input" style="text-align: left;float: left; font-size: 18px;">Escuela o Grupo<sup>*</sup></label>
              <input type="text" id="escuela" name="extras[escuela]" placeholder="Escuela" required style="border: 0px;padding: 10px;">
              <label id="label-input" style="text-align: left;float: left; font-size: 18px;">Teléfono<sup>*</sup></label>
              <input type="text" id="telefono" name="extras[telefono]" placeholder="Teléfono" required style="border: 0px;padding: 10px;">
              <label id="label-input" style="text-align: left;float: left; font-size: 18px;">Correo<sup>*</sup></label>
              <input type="email" id="email" name="email" placeholder="Correo" required style="border: 0px;padding: 10px;">
              <label id="label-input" style="text-align: left;float: left; font-size: 18px;">Cantidad de Alumnos<sup>*</sup></label>
              <input type="text" id="usuarios" name="extras[usuarios]" placeholder="Apróx." required style="border: 0px;padding: 10px;">
              <div class="response"></div>
          </div>

          <div class="modal-footer text-center">
            <button type="submit" class="btn button-sm button-orange" style="width: 100%;">Enviar</button>
          </div>
          <button class="text-center" type="button" data-dismiss="modal" style="width: 100%;">Cancelar</button>
       </form>


    </div>
  </div>
</div>
<!-- Contacto escuela  -->
