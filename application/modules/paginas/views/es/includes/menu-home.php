<!-- navbar -->
<nav class="navbar navbar-absolute navbar-fixed">
  <div class="container">
    <a class="navbar-brand img-menu-top" href="<?= base_url() ?>">
    	<img src="<?= base_url() ?>theme/svg/home/logo-snslogan.png" alt="Logo Linkids">
    </a>
    <ul class="nav">
      <li class="nav-item"><a class="nav-link" href="#inicio" id="menu-home"><i class="fab fa-fort-awesome-alt icon-menu"></i> Inicio</a></li>
      <li class="nav-item"><a class="nav-link" href="#nosotros" id="menu-home"><i class="fas fa-award icon-menu"></i> Nosotros</a></li>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>valoracion.html" id="menu-home"><i class="fas fa-book icon-menu"></i> Valoración científica</a></li>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>faq.html" id="menu-home"><i class="fas fa-question-circle icon-menu"></i> Ayuda</a></li>
      <li class="nav-item"><a class="nav-link" href="#contacto" id="menu-home"><i class="fas fa-id-badge icon-menu"></i> Contacto</a></li>
      <?php if(empty($_SESSION['user'])): ?>
      <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>login.html" id="menu-home"><i class="fas fa-user-circle icon-menu"></i> Iniciar sesión</a></li>
      <?php else: ?>
  	<li class="nav-item"><a class="nav-link" href="<?= base_url('panel') ?>" id="menu-home"><i class="fas fa-user-circle icon-menu"></i> Entrar</a></li>
  	<li class="nav-item"><a class="nav-link" href="<?= base_url('main/unlog') ?>" id="menu-home"><i class="fas fa-sign-out-alt icon-menu"></i> Cerrar sesión</a></li>
      <?php endif ?>
    </ul>
    <!-- Nav Toggle button -->
    <button class="nav-toggle-btn"><span class="lines"></span></button>
  </div>
</nav>
<!-- end navbar -->