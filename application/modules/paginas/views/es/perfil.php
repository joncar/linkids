<?php
	if(!$this->elements->canAccess()){
        if(empty($this->user->user_vinculacion)){
            redirect('finanzas/cargarPago');
        }else{
            redirect('clases');
        }

    }
?>
<link href='<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/slim.cropper/slim.min.front.css' type="text/css" rel="stylesheet">
<!-- Menu Top Desktop -->
<header class="d-none d-lg-block">
	<?php $this->load->view('es/includes/menu-top',array(),FALSE,'paginas');?>
	<!--<//?php $this->load->view('es/includes/progreso',array(),FALSE,'paginas');?>-->
</header>
<!-- Sidebar Navigation Desktop -->
<div class="sidebar-nav-left d-none d-lg-block">
	<?php $this->load->view('es/includes/sidebar-perfil',array(),FALSE,'paginas');?>
</div>
<!-- end Sidebar Navigation -->
<!-- Menu Top Movil -->
<header class="d-lg-none">
	<?php $this->load->view('es/includes/menu-top-perfil-movil',array(),FALSE,'paginas');?>
	<div class="alert text-white">
		<span class="closebtn" onclick="this.parentElement.style.display='none';"><i class="far fa-times-circle"></i></span>
		<div class="col-md-6 offset-md-3 text-center">
			<div class="text-message-top">Tu progreso de hoy:</div>

			<div class="wizard-progress">
			  <div class="step step--active"><i class="far fa-sun icon-active-progress"></i></div>
			  <div class="step"><i class="fas fa-cloud-sun icon-lock-progress"></i></div>
			  <div class="step"><i class="fas fa-moon icon-lock-progress"></i></div>
			</div>

		</div>
	</div>
</header>
<!-- Menu Top Movil -->
<div class="sidebar-wrapper-left">
	<!-- Inicia Contenido -->
	<div class="section container-white-top">
		<div class="container">
			<form method="post" onsubmit="insertar('seguridad/perfil/update/<?= $this->user->id ?>',this,'',function(data){$('.submit-result').show().find('.price-features').html(data.success_message);}); return false;">
				<div class="col-12 text-center">
					<h3 class="margin-bottom-20">Configuración de la Cuenta</h3>
				</div>
				<div class="col-12 text-center margin-bottom-30">

					<div class="profile profile-pic">
                        <div class="slim image mx-auto d-block" id="slim-foto"
                             data-service="seguridad/perfil/cropper/foto"
                             data-post="input, output, actions"
                             data-size="120,120"
                             data-instant-edit="true"
                             data-push="true"
                             data-did-upload="imageSlimUpload"
                             data-download="false"
                             data-remove="false"
                             data-will-save="addValueSlim"
                             data-label="Subir foto"
                             data-meta-name="foto"
                             data-force-size="120,120">
                             <div class="circle-1"></div>
							 <div class="circle-2"></div>
                             <img src="<?= $this->user->getFoto() ?>" width="70" height="70" alt="Perfil de usuario">
                             <input type="file" id="slim-foto"/>
                             <input type="hidden" data-val="<?= $this->user->foto ?>" name="foto" value="<?= $this->user->foto ?>" id="field-foto">
                        </div>
                    </div>
				</div>
				<div class="col-12 contact-form">
					<label id="label-input">Correo</label>
					<input type="email" id="email" name="email" placeholder="Correo" value="<?= @$this->user->email ?>">
					<label id="label-input">Nombre</label>
					<input type="text" id="usuario" name="nombre" placeholder="Usuario" value="<?= @$this->user->nombre ?>">
					<label id="label-input">Apellido</label>
					<input type="text" id="usuario" name="apellido_paterno" placeholder="Usuario" value="<?= @$this->user->apellido_paterno ?>">
					<label id="label-input">Password</label>
					<input type="password" id="contrasena" name="password" placeholder="Password" value="<?= @$this->user->password ?>">

					<?php if(!empty($this->user->user_vinculacion)): ?>
						<label id="label-input">Escuela</label>
						<input type="text" value="<?= $this->db->get_where('user',array('id'=>$this->user->user_vinculacion))->row()->nombre ?>" disabled="true">
					<?php endif ?>


					<input type="hidden" name="status" value="1">
					<input type="hidden" name="admin" value="<?= $this->user->admin ?>">
					<div class="row margin-top-30">
						<div class="col-12 col-sm-6 text-center"><button class="button button-sm button-blue-transparent" type="submit">Cancelar</button></div>
						<div class="col-12 col-sm-6 text-center"><button class="button button-sm button-blue" type="submit">Guardar</button></div>
					</div>

					<!-- Submit result -->
			          <div class="submit-result" style="display: none">
			            <div class="prices-box">
			              <img src="<?= base_url() ?>theme/svg/home/ingrediente.svg" alt="Linkids" class="mx-auto d-block margin-bottom-20">
			              <div class="price-features">
			                <ul class="text-white">
			                  <li><span id="success">Thank you! Your Message has been sent.</span></li>
			                  <li><span id="error">Something went wrong, Please try again!</span></li>
			                </ul>
			              </div>
			            </div>
			          </div>
			        <!-- Submit result -->

				</div><!-- end contact-form -->
			</form>
		</div><!-- end container -->
	</div>
			<!-- Termina Contenido -->
</div><!-- end sidebar-wrapper-left -->
<script src='<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/slim.croppic.min.js'></script>
