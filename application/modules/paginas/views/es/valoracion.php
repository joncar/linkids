<!-- Menu Top -->
    <header>
      <?php $this->load->view('es/includes/menu-valoracion',array(),FALSE,'paginas');?>
      <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy&display=swap" rel="stylesheet">
    </header>
    <!-- Menu Top -->

    <!-- Scroll to top button -->
    <div class="scrolltotop">
      <a class="button-circle button-circle-sm button-circle-dark" href="#">
        <i class="ti-arrow-up"></i>
      </a>
    </div>
    <!-- end Scroll to top button -->

    <!-- Social -->
    <div class="social">
      <ul>
        <li class="icon-whatsapp text-center"><a href="https://wa.me/5215517005534" title="Enviar mensaje"><i class="fab fa-whatsapp"></i></a></li>
        <li class="icon-planes text-center"><a href="<?= base_url() ?>main.html#planes" title="Ver Planes" id="margin-planes"><b>Planes</b></a></li>
      </ul>
    </div>

    <!-- Home section -->
    <div class="section-xl bg-image parallax bg-home-header hero-bkg-animated" style="background-image: url(<?= base_url() ?>theme/svg/home/header.svg)" id="inicio">
      <div class="container">
        <div class="row margin-top-50 margin-bottom-50 align-items-center">
          <div class="col-12 col-sm-12 text-center titulo-header-home">
          	<div class="margin-bottom-20">
	            <h1 class="font-weight-bold no-margin text-uppercase text-yellow margin-bottom-10 font-cursiva">
	              <span class="text-white"></span><br>Fundamentos científicos
	            </h1>
        	</div>
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Home section -->

    <!-- Services section -->
    <div class="section bg-orange">
      <div class="container">

        <div class="row container-white-2">
          <div class="col-12 col-md-12 bg-white-home">


              <div class="row margin-bottom-30">
                <div class="col-12 col-md-6">
                  <button type="button" class="btn" data-toggle="modal" data-target="#imagen01" title="Ver imagen">
                    <img src="<?= base_url() ?>theme/svg/valoracion-02.jpg" alt="Valoraciòn científica">
                  </button>
                </div>
                <div class="col-12 col-md-6">
                  <button type="button" class="btn" data-toggle="modal" data-target="#imagen02" title="Ver imagen">
                    <img src="<?= base_url() ?>theme/svg/valoracion-01.jpg" alt="Valoraciòn científica">
                  </button>
                </div>
              </div>

              <!-- Modal -->
              <div class="modal fade bd-example-modal-xl" id="imagen01" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-body" style="padding: 0;">
                      <img src="<?= base_url() ?>theme/svg/valoracion-02.jpg" alt="Valoraciòn científica">
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div>
                </div>
              </div>

              <!-- Modal -->
              <div class="modal fade bd-example-modal-xl" id="imagen02" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-body" style="padding: 0;">
                      <img src="<?= base_url() ?>theme/svg/valoracion-01.jpg" alt="Valoraciòn científica">
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row align-items-center bg-image bg-home-header text-center idiomas" style="background-image: url(<?= base_url() ?>theme/svg/home/clases.svg)">
                <div class="col-12 col-sm-6 idioma-borde link-idiomas" style="margin-bottom: 0px;">
                  <a href="#">
                    <!--<img src="<?= base_url() ?>theme/svg/home/idiomas.svg" alt="Linkids" class="mx-auto d-block">-->
                    <h3 class="text-white font-weight-bold margin-bottom-20">Música y<br>matemáticas</h3>
                    </a><a class="button button-md button-blue-home" href="<?= base_url() ?>login.html" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>

                </div>
                <div class="col-12 col-sm-6 idioma-borde link-idiomas2" style="margin-bottom: 0px;">
                  <a href="#">
                    <!--<img src="<?= base_url() ?>theme/svg/home/idiomas6.svg" alt="Linkids" class="mx-auto d-block">-->
                    <h3 class="text-white font-weight-bold margin-bottom-20">Niños más<br>exitosos y hábiles</h3>
                    </a><a class="button button-md button-blue-home" href="<?= base_url() ?>login.html" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>

                </div>
              </div>

          </div>
        </div><!-- end row -->


      </div><!-- end container -->
    </div>
    <!-- end Services section -->

    <footer id="form-home">
      <div class="footer bg-blue">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-6 text-center text-md-left">
              <p class="margin-top-10 text-white">
                &copy; 2019 Linkids - ¡Evolución educativa en idiomas!<br>
                <a href="<?= base_url() ?>aviso.html" style="text-color:white; text-decoration:underline;">Aviso de Privacidad</a> |
                <a href="<?= base_url() ?>terminos.html"  style="text-color:white; text-decoration:underline;">Términos y Condiciones</a>
              </p>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right">
              <ul class="list-horizontal-unstyled">
                <li style="font-size: 24px;"><a href="https://www.facebook.com/Estimulaci%C3%B3n-Temprana-en-Idiomas-y-M%C3%BAsica-341652089375058/" target="blank" class="text-white"><i class="fab fa-facebook-f"></i></a></li>
                <li style="font-size: 24px;"><a href="https://www.instagram.com/linkidsevolucioneducativa/?igshid=19d64owl0uuub" target="blank" class="text-white"><i class="fab fa-instagram"></i></a></li>
                <li style="font-size: 24px;"><a href="https://www.youtube.com/channel/UCdFU2NHJZyDv-ShOopzhX4A" target="blank" class="text-white"><i class="fab fa-youtube"></i></a></li>
              </ul>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div>
    </footer>
