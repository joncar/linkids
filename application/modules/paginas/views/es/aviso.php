<!-- Menu Top -->
    <header>
      <?php $this->load->view('es/includes/menu-valoracion',array(),FALSE,'paginas');?>
      <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy&display=swap" rel="stylesheet">
    </header>
    <!-- Menu Top -->

    <!-- Scroll to top button -->
    <div class="scrolltotop">
      <a class="button-circle button-circle-sm button-circle-dark" href="#">
        <i class="ti-arrow-up"></i>
      </a>
    </div>
    <!-- end Scroll to top button -->

    <!-- Social -->
    <div class="social">
      <ul>
        <li class="icon-whatsapp text-center"><a href="https://wa.me/5215517005534" title="Enviar mensaje"><i class="fab fa-whatsapp"></i></a></li>
        <li class="icon-planes text-center"><a href="<?= base_url() ?>main.html#planes" title="Ver Planes" id="margin-planes"><b>Planes</b></a></li>
      </ul>
    </div>

    <!-- Home section -->
    <div class="section-xl bg-image parallax bg-home-header hero-bkg-animated" style="background-image: url(<?= base_url() ?>theme/svg/home/header.svg)" id="inicio">
      <div class="container">
        <div class="row margin-top-50 margin-bottom-50 align-items-center">
          <div class="col-12 col-sm-12 text-center titulo-header-home">
          	<div class="margin-bottom-20">
	            <h1 class="font-weight-bold no-margin text-uppercase text-yellow margin-bottom-10 font-cursiva">
	              <span class="text-white"></span><br>Aviso de Privacidad
	            </h1>
        	   </div>
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Home section -->


    <!-- Services section -->
    <div class="section bg-orange">
      <div class="container">

        <div class="row container-white-2">
          <div class="col-12 col-md-12 bg-white-home">


              <div class="row margin-bottom-30">
                <div class="col-12 col-md-12">
                  <p class="text-home">
                    De conformidad con lo establecido en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, linkids.com.mx pone a su disposición el siguiente aviso de
                    privacidad.<br><br>

                    linkids.com.mx, es responsable del uso y protección de sus datos personales, en este sentido y atendiendo las obligaciones legales establecidas en la Ley Federal de Protección de Datos
                    Personales en Posesión de los Particulares, a través de este instrumento se informa a los titulares de los datos, la información que de ellos se recaba y los fines que se le darán a dicha
                    información.<br>

                    Todos los logotipos, imágenes, texto, música, y cualquier otro archivo utilizado en la página www.linkids.com.mx, son propiedad de LINKIDS, por lo que no puede hacer uso personal de
                    ellos sin la autorización previa de linkids.com.mx.<br>

                    Además de lo anterior, informamos que linkids.com.mx, tiene su domicilio ubicado en: Javier Barros Sierra No. 245, Col. Santa Fe, Delegación Álvaro Obregón, CP 01210, Ciudad de México.<br>
                    Los datos personales que recabamos serán utilizados para las siguientes finalidades, las cuales son necesarias para atender los servicios y/o pedidos solicitados por el usuario.<br>

                    Por otra parte, informamos que sus datos personales no serán compartidos con ninguna autoridad, empresa, organización o persona distintas a nosotros y serán utilizados exclusivamente para los fines señalados.<br>
                    El usuario tiene en todo momento el derecho a conocer los datos personales con los que cuenta la plataforma linkids.com.mx, el uso y las condiciones del uso que les damos (Acceso).<br>
                    Asimismo, es su derecho solicitar la corrección de su información personal en caso de que esté desactualizada, sea inexacta o incompleta (Rectificación); de igual manera, tiene derecho a que
                    su información se elimine de nuestros registros o bases de datos cuando considere que la misma no está siendo utilizada adecuadamente (Cancelación); así como también a oponerse al
                    uso de sus datos personales para fines específicos (Oposición). Estos derechos se conocen como derechos ARCO.<br><br>

                    Para el ejercicio de cualquiera de los derechos ARCO, se deberá presentar la solicitud respectiva a través de los formatos que estarán a su disposición en: linkids.com.mx<br>

                    En todo caso la respuesta se dará en las 72 horas siguientes una vez recibida su solicitud.<br>
                    Los datos de contacto de la persona o departamento de datos personales, que está a cargo de dar trámite a las solicitudes de derechos ARCO, son los siguientes:<br>
                    a) Nombre del responsable: Gianluca Ataides Huesca<br>
                    b) Domicilio: Javier Barros Sierra No. 245, Col. Santa Fe, Delegación Álvaro Obregón, CP 01210, Ciudad de México<br>
                    c) Teléfono: 55-1700-5534<br>

                    Cabe mencionar, que en cualquier momento usted puede revocar su consentimiento para el uso y tratamiento de sus datos personales. Asimismo, usted deberá considerar que para ciertos
                    fines, la revocación de su consentimiento implicará que no podamos seguir prestando el servicio que nos solicitó, o la conclusión de su relación con nosotros.<br>
                    Para revocar el consentimiento que usted otorga en este acto o para limitar su divulgación, se deberá presentar la solicitud respectiva a través de los formatos que estarán a su disposición
                    en: linkids.com.mx.<br><br>

                    Del mismo modo, podrá solicitar la información para conocer el procedimiento y requisitos para la revocación del consentimiento, así como limitar el uso y divulgación de su información
                    personal.<br>
                    En cualquier caso, la respuesta a las peticiones se dará a conocer en un plazo de 30 días naturales.<br><br>

                    En el supuesto de cambios en nuestro modelo de negocio, o por otras causas, nos comprometemos a mantenerlo informado sobre los cambios que pueda sufrir el presente aviso
                    de privacidad. Así mismo, puede solicitar la información respectiva a través de la siguiente dirección electrónica: contacto@linkids.com.mx<br><br>

                    <span class="font-weight-bold">
                      Última actualización:
                      26-junio-2019
                    </span>
                  </p>
                </div>
              </div>

              <div class="row align-items-center bg-image bg-home-header text-center idiomas" style="background-image: url(<?= base_url() ?>theme/svg/home/clases.svg)">
                <div class="col-12 col-sm-6 idioma-borde link-idiomas" style="margin-bottom: 0px;">
                  <a href="#">
                    <!--<img src="<?= base_url() ?>theme/svg/home/idiomas.svg" alt="Linkids" class="mx-auto d-block">-->
                    <h3 class="text-white font-weight-bold margin-bottom-20">Música y<br>matemáticas</h3>
                    </a><a class="button button-md button-blue-home" href="<?= base_url() ?>login.html" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>

                </div>
                <div class="col-12 col-sm-6 idioma-borde link-idiomas2" style="margin-bottom: 0px;">
                  <a href="#">
                    <!--<img src="<?= base_url() ?>theme/svg/home/idiomas6.svg" alt="Linkids" class="mx-auto d-block">-->
                    <h3 class="text-white font-weight-bold margin-bottom-20">Niños más<br>exitosos y hábiles</h3>
                    </a><a class="button button-md button-blue-home" href="<?= base_url() ?>login.html" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>
                </div>
              </div>

          </div>
        </div><!-- end row -->


      </div><!-- end container -->
    </div>
    <!-- end Services section -->


    <footer id="form-home">
      <div class="footer bg-blue">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-6 text-center text-md-left">
              <p class="margin-top-10 text-white">
                &copy; 2019 Linkids - ¡Evolución educativa en idiomas!<br>
                <a href="<?= base_url() ?>aviso.html" style="text-color:white; text-decoration:underline;">Aviso de Privacidad</a> |
                <a href="<?= base_url() ?>terminos.html"  style="text-color:white; text-decoration:underline;">Términos y Condiciones</a>
              </p>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right">
              <ul class="list-horizontal-unstyled">
                <li style="font-size: 24px;"><a href="https://www.facebook.com/Estimulaci%C3%B3n-Temprana-en-Idiomas-y-M%C3%BAsica-341652089375058/" target="blank" class="text-white"><i class="fab fa-facebook-f"></i></a></li>
                <li style="font-size: 24px;"><a href="https://www.instagram.com/linkidsevolucioneducativa/?igshid=19d64owl0uuub" target="blank" class="text-white"><i class="fab fa-instagram"></i></a></li>
                <li style="font-size: 24px;"><a href="https://www.youtube.com/channel/UCdFU2NHJZyDv-ShOopzhX4A" target="blank" class="text-white"><i class="fab fa-youtube"></i></a></li>
              </ul>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div>
    </footer>
