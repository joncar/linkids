
<!-- Menu Top Desktop -->
<header class="d-none d-lg-block">
	<?php $this->load->view('es/includes/menu-top',array(),FALSE,'paginas');?>
	<?php $this->load->view('es/includes/progreso',array(),FALSE,'paginas');?>
</header>

<!-- Menu Top -->
<!-- Sidebar Navigation Desktop -->
<div class="sidebar-nav-left d-none d-lg-block sticky">
	<?php $this->load->view('es/includes/sidebar-clases',array(),FALSE,'paginas');?>
</div>
<!-- end Sidebar Navigation -->


<header class="d-lg-none">
	<?php $this->load->view('es/includes/menu-top-clases-movil',array(),FALSE,'paginas');?>
	<div class="alert text-white">
		<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
		<div class="col-md-12 text-center">
			<div class="text-message-top">Tu progreso de hoy:</div>
			<div class="wizard-progress" id="progress-top">
				<div class="step complete"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
				<div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
				<div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-black"></div></div>
			</div>
		</div>
	</div>
</header>


<!-- Inicia Contenido -->
<div class="sidebar-wrapper-left">

	<?php
		if($clase):
	?>

		<!-- Full Video -->
		<div class="container-white-video">
			<div class="container">
				<div class="row" style="margin-bottom:0px;">
					<div class="col-12 col-sm-12" style="margin-bottom: 0px;">
						<div class="videoWrapper mx-auto d-block">
							<div id="divNext" class="nextVideoLabel"><div onclick="goNext()">Ir al siguiente video</div></div>
							<video id="reproductor" controls="true" style="width:100%; height:auto;">
								<source src="<?= $clase->video ?>" type="video/mp4" media="">
							</video>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Full Video -->
		<div>
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12">

					<!-- 1 -->
					<?php if(!empty($clase)): ?>
					<div class="row text-center">

						<div class="col-12 col-sm-12 col-lg-6" style="margin-bottom: 0px;">
							<h3 class="text-blue text-responsive">
							<!--<img src="https://picsum.photos/300/250/?image=75" alt="Idiomas" class="image-title-video">-->
							<img src="<?= $clase->videoInfo->bandera ?>" alt="Idiomas" class="image-title-video-flag">
							<?= $clase->videoInfo->nombre ?>
							</h3>
						</div>

						<div class="col-12 col-sm-12 col-lg-6 text-descarga-movil margin-top-10" >
							<a data-toggle="modal" data-target="#programa-completo" style="font-size: 12px; text-decoration: underline; cursor: pointer;">Ver Programa completo</a>
						</div>
					</div>

					<div class="row text-center margin-bottom-10">
						<div class="col-12">
							<h5>Lecciones para hoy:</h5>
						</div>
					</div>

					<div class="row text-center">

						<?php
							foreach($this->elements->getVideosHoy() as $c):
						?>

							<div class="col-6 col-sm-6 col-lg-4">
								<!--<div class="image-lesion"><img src="https://picsum.photos/35/35/?image=75"/ class="rounded-circle box-shadow borde-img-lock" alt="Lesiones"></div>-->
								<a href="<?= $c->activo?$c->link:'javacript:void(0)' ?>" title="Linkids Clases" <?= $c->activo?'':'data-toggle="modal" data-target="#clase-bloqueada"' ?>>
									<div id="gallery" class="text-white active">
										<div id="images">
											<div class="figure">
												<img class="image" src="<?= $c->thumb ?>" alt="<?= $c->nombre ?>">
												<div class="caption">
													<!--
													<div class="body">
														<div class="title text-white"><?= $c->nombre ?></div>
														<p class="text"><?= $c->descripcion ?></p>
													</div>
												-->
												</div>
											</div>
										</div>
									</div>
								</a>
								<div class="title-lessons">
									<a href="<?= $c->activo?$c->link:'javacript:void(0)' ?>" title="<?= $c->nombre ?>" <?= $c->activo?'':'data-toggle="modal" data-target="#clase-bloqueada"' ?>>
										<?= $c->nombre ?> <img src="<?= $c->idiomas->bandera ?>" alt="Idiomas">
									</a>
								</div>
							</div>

						<?php endforeach ?>
					</div>


					<?php else: ?>
						<h3 class="text-blue">No existen clases pendientes, te invitamos a regresar más tarde</h3>
					<?php endif ?>

				</div>
				</div><!-- end row -->
				</div><!-- end container -->
			</div>

			<?php else: ?>
				<h3 class="text-blue">No existen clases pendientes, te invitamos a regresar más tarde</h3>
			<?php endif ?>


	<script>
		var nextVideo = '<?= !is_numeric($clase->next) || $clase->next!=-1?base64_encode($clase->next->id):-1 ?>';
		var finished = false;
		jQuery(document).on('ready',function(){
			var video = document.getElementById('reproductor');
			console.log(video);
			video.onplay = function(){
				if(finished){
					$(".nextVideoLabel").removeClass('visible');
				}
				console.log('played '+video.currentTime+' '+video.duration);
			}
			video.onpause = function(){
				if(finished){
					$(".nextVideoLabel").addClass('visible');
				}
				if(video.currentTime == video.duration){
					finish();
				}
				console.log('pausado '+video.currentTime+' '+video.duration);
			}
		});

		function finish(){
			if(nextVideo!='-1'){
				$.post('<?= base_url('aperturar') ?>',{videos_id:nextVideo},function(data){
					data = JSON.parse(data);
					if(data.success){
						finished = true;
						$(".nextVideoLabel").addClass('visible');
						console.log('terminado');
					}else{
						alert(data.msj);
					}
				});
			}else{
				$(".nextVideoLabel").addClass('visible');
				$(".nextVideoLabel div").html('');
				$(".nextVideoLabel div").removeAttr('onclick');
				$("#felicidades").modal('show');
			}
		}

		function goNext(){
			document.location.href="<?= base_url() ?>clases/"+nextVideo;
		}
	</script>
	<?php $this->load->view('es/includes/modales',array(),FALSE,'paginas'); ?>
