<?php
  if(!empty($_SESSION['user'])): redirect('clases');
endif ?>
<?php
  if(!empty($_GET['pago']) && is_numeric($_GET['pago'])){
    $_SESSION['pago'] = $_GET['pago'];
  }
?>
<body data-preloader="2" class="loaded" id="scroll-registro">
    <!-- Sidebar Navigation Desktop -->
    <div class="sidebar-nav-left d-none d-lg-block">
      <button class="sidebar-nav-toggle" style="z-index: 999999999;"><span class="lines"></span></button>
      <div class="sidebar-nav-content" id="sidebar-nav-login" style="border: 0px; background-color: transparent;">

        <div class="col-12 col-sm-12 margin-bottom-30">
          <a href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/svg/home/logo.png" alt="Logo Linkids" style="width: 60%;" class="mx-auto d-block"></a>
        </div>

        <div class="col-12">
          <form method="post" action="<?= base_url('main/login') ?>">
            <label id="label-input">Correo</label>
            <input type="email" id="email" name="email" placeholder="Correo" required style="border: 0px;padding: 10px;">
            <label id="label-input">Contraseña</label>
            <input type="password" id="contrasena" name="pass" placeholder="contraseña" required style="border: 0px;padding: 10px;">

            <div class="olvide-login">
              <a data-toggle="modal" data-target="#olvide-contrasena"  style="font-size: 12px; text-decoration:underline;"><i class="far fa-question-circle"></i> Olvidé mi contraseña</a>
            </div>

            <div class="row margin-top-30">
              <div class="col-12 col-sm-8">
              		<button type="submit" class="button button-sm button-blue" style="width:100%">Ingresar</button>
          	  </div>
              <div class="col-12 col-sm-8">
                <a href="<?= base_url() ?>registro.html">
                  <button class="button button-sm button-blue-transparent" type="button" style="width:100%">Registrarme</button>
                </a>
              </div>
            </div>
          </form>
          <!-- Submit result -->
          <div class="submit-result">
            <div class="error"><?= @$_SESSION['msj']; ?></div>
          </div>
        </div><!-- end contact-form -->

      </div><!-- end sidebar-nav-content -->
    </div>
    <!-- end Sidebar Navigation -->

    <!-- Sidebar Navigation Movil -->
    <div class="d-lg-none" id="sidebar-nav-login-movil">
      <div class="col-12 col-sm-12 margin-bottom-30">
        <a href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/svg/home/logo.png" alt="Logo Linkids" class="logo-movil mx-auto d-block"></a>
      </div>

      <div class="col-12">
          <form method="post" action="<?= base_url('main/login') ?>">
            <label id="label-input">Correo</label>
            <input type="email" id="email" name="email" placeholder="micorreo@">
            <label id="label-input">Password</label>
            <input type="password" id="contrasena" name="pass" placeholder="-----">

            <div class="olvide-login">
              <a data-toggle="modal" data-target="#olvide-contrasena" style="text-decoration:underline;">Olvidé mi contraseña</a>
            </div>

            <div class="row margin-top-30">
              <div class="col-12 col-sm-6">
                <button class="button button-sm button-blue" type="submit">Ingresar</button>
              </div>
              <div class="col-12 col-sm-6">
                <a href="<?= base_url() ?>registro.html">
                  <button class="button button-sm button-blue-transparent" type="button">
                    Registrarme
                  </button>
                </a>
              </div>
            </div>
          </form>
          <!-- Submit result -->
          <div class="submit-result">
            <div class="error"><?= @$_SESSION['msj']; unset($_SESSION['msj']); ?></div>
          </div>
        </div><!-- end contact-form -->
    </div>

    <!-- SVG Desktop Login -->
    <div class="sidebar-wrapper-left container-profile d-none d-lg-block">
  		<div class="section container-white-none animacion-login">
        <span class="cloud cloud--small"></span>
        <span class="cloud cloud--medium"></span>
        <span class="cloud cloud--large"></span>
        <img src="<?= base_url() ?>theme/svg/hombre.svg" id="hombre">
        <img src="<?= base_url() ?>theme/svg/montana.svg" id="montana">


        <div class="sun" id="sol">
            <div class="ray_box">
                <div class="ray ray1"></div>
                <div class="ray ray2"></div>
                <div class="ray ray3"></div>
                <div class="ray ray4"></div>
                <div class="ray ray5"></div>
                <div class="ray ray6"></div>
                <div class="ray ray7"></div>
                <div class="ray ray8"></div>
                <div class="ray ray9"></div>
                <div class="ray ray10"></div>
            </div>
        </div>

      </div>
    </div>
</div>
