
<body data-preloader="2" class="loaded" id="scroll-registro">

<!-- Sidebar Navigation Desktop -->
    <div class="sidebar-nav-left d-none d-sm-block">
      <button class="sidebar-nav-toggle" style="z-index: 999999999;"><span class="lines"></span></button>
      <div class="sidebar-nav-content" id="sidebar-nav-registro" style="border: 0px; background-color: transparent;">

        <div class="col-12 col-sm-12 margin-bottom-10">
          <a href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/svg/home/logo.png" alt="Logo Linkids" style="width: 60%;" class="mx-auto d-block"></a>
        </div>

        <div class="col-12 contact-form" id="contenedor-registro">
          <form method="post" onsubmit="insertar('registro/index/insert/',this,'.submit-result'); $('.submit-result').show(); return false;">

            <div class="row">
              <div class="col-12 col-sm-6">
                <label id="label-input">Nombre</label>
                <input type="text" id="nombre" name="nombre" placeholder="Nombre" style="border: 0px;padding: 10px; height: 34px; font-size: 12px;">
              </div>

              <div class="col-12 col-sm-6">
                <label id="label-input">Apellido</label>
                <input type="text" id="apellido_paterno" name="apellido_paterno" placeholder="Apellido" style="border: 0px;padding: 10px; height: 34px; font-size: 12px;">
              </div>
            </div>

            <label id="label-input">Correo</label>
            <input type="email" id="email" name="email" placeholder="Correo" style="border: 0px;padding: 10px; height: 34px; font-size: 12px;">
            <label id="label-input">Clave de registro (Sólo en caso de que tu escuela te la haya proporcionado, con ella tendrás acceso a tus clases diarias)</label>
            <input type="text" id="contrasena" name="codigo_descuento" placeholder="EJ: JOCIXV79ET" style="border: 0px;padding: 10px; height: 34px; font-size: 12px;">

            <div class="row">
              <div class="col-12 col-sm-6">
                <label id="label-input">Contraseña</label>
                <input type="password" id="contrasena" name="password" placeholder="contraseña" style="border: 0px;padding: 10px; height: 34px; font-size: 12px;">
              </div>

              <div class="col-12 col-sm-6">
                <label id="label-input">Repetir contraseña</label>
                <input type="password" id="contrasena" name="password2" placeholder="repetir contraseña" style="border: 0px;padding: 10px; height: 34px; font-size: 12px;">
              </div>
            </div>

            <div class="margin-bottom-10">
              <div class="cntr">
                <input class="hidden-xs-up olvide-login" id="cbx" type="checkbox" name="politicas" style="visibility: hidden;"><label class="cbx" for="cbx"></label>
                <label class="lbl" for="cbx">Aceptar <a href="<?= base_url() ?>terminos.html" target="blank" style="text-decoration: underline;">Términos y Condiciones</a></label>
              </div>
            </div>

            <div class="row">
              <div class="col-12 col-sm-6">
            		<button class="button button-sm button-blue-transparent" type="submit" style="float: left;">Registrarme</button>
              </div>

              <div class="col-12 col-sm-6 olvide-login">
                <a href="<?= base_url() ?>login.html" style="font-size: 12px; text-decoration:underline;"><i class="far fa-smile-wink"></i> Ya tengo Cuenta</a>
              </div>
            </div>



          </form>

          <!-- Submit result -->
          <div class="submit-result" style="display: none;">
            <div class="prices-box">
              <img src="<?= base_url() ?>theme/svg/home/ingrediente.svg" alt="Linkids" class="mx-auto d-block margin-bottom-20">
              <div class="price-features">
                <ul class="text-white">
                  <li><span id="success">Thank you! Your Message has been sent.</span></li>
                  <li><span id="error">Something went wrong, Please try again!</span></li>
                </ul>
              </div>
            </div>
          </div>
          <!-- Submit result -->

        </div><!-- end contact-form -->

      </div><!-- end sidebar-nav-content -->
    </div>
    <!-- end Sidebar Navigation -->

    <!-- Sidebar Navigation Movil -->
    <div class="d-sm-none" id="sidebar-nav-login-movil">
      <div class="col-12 col-sm-12 margin-bottom-30">
        <a href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/svg/home/logo.png" alt="Logo Linkids" class="logo-movil mx-auto d-block"></a>
      </div>
      <div class="col-12">
          <form method="post" onsubmit="insertar('registro/index/insert/',this,'.submit-result'); $('.submit-result').show(); return false;">
            <label id="label-input">Nombres</label>
            <input type="text" id="nombre" name="nombre" placeholder="" style="border: 0px;">
            <label id="label-input">Apellidos</label>
            <input type="text" id="apellido_paterno" name="apellido_paterno" placeholder="" style="border: 0px;">
            <label id="label-input">Correo</label>
            <input type="email" id="email" name="email" placeholder="micorreo@" style="border: 0px;">
            <label id="label-input">Clave de registro</label>
            <input type="text" id="contrasena" name="codigo_descuento" placeholder="EJ: JOCIXV79ET" style="border: 0px;">
            <label id="label-input">Contraseña</label>
            <input type="password" id="contrasena" name="password" placeholder="----" style="border: 0px;">
            <label id="label-input">Repetir contraseña</label>
            <input type="password" id="contrasena" name="password2" placeholder="----" style="border: 0px;">

            <div class="margin-bottom-10">
              <div class="cntr">
                <input class="hidden-xs-up olvide-login" id="cbx" type="checkbox" name="politicas" style="visibility: hidden;"><label class="cbx" for="cbx"></label>
                <label class="lbl" for="cbx">Aceptar <a href="<?= base_url() ?>terminos.html" target="blank" style="text-decoration: underline;">Términos y Condiciones</a></label>
              </div>
            </div>

            <div class="row margin-top-30">
              <div class="col-12 col-sm-6"><button class="button button-sm button-blue" type="submit">Registrarme</button></div>
              <div class="col-12 col-sm-6"><a href="<?= base_url() ?>login.html"><button class="button button-sm button-blue-transparent" type="button">Ya tengo cuenta</button></a></div>
            </div>
          </form>
          <!-- Submit result -->
          <div class="submit-result">
            <span id="success">Thank you! Your Message has been sent.</span>
            <span id="error">Something went wrong, Please try again!</span>
          </div>
        </div><!-- end contact-form -->
    </div>

    <!-- SVG Desktop Login -->
    <div class="sidebar-wrapper-left container-profile d-none d-sm-block">
  		<div class="section container-white-none animacion-login">
        <span class="cloud cloud--small"></span>
        <span class="cloud cloud--medium"></span>
        <span class="cloud cloud--large"></span>
        <img src="<?= base_url() ?>theme/svg/hombre.svg" id="hombre">
        <img src="<?= base_url() ?>theme/svg/montana.svg" id="montana">

        <div class="sun" id="sol">
            <div class="ray_box">
                <div class="ray ray1"></div>
                <div class="ray ray2"></div>
                <div class="ray ray3"></div>
                <div class="ray ray4"></div>
                <div class="ray ray5"></div>
                <div class="ray ray6"></div>
                <div class="ray ray7"></div>
                <div class="ray ray8"></div>
                <div class="ray ray9"></div>
                <div class="ray ray10"></div>
            </div>
        </div>

      </div>
    </div>
</div>
