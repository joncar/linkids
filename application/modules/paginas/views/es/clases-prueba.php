<?php 
	$current = $this->elements->clases_prueba()->row(); 
?>
<header class="d-none d-lg-block">
	<?php $this->load->view('es/includes/menu-top-prueba',array(),FALSE,'paginas');?>
	<div class="alert text-white">
		<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
		<div class="col-md-6 offset-md-3 text-center">
			<div class="text-message-top margin-bottom-10">Tu progreso de hoy:</div>
			<div class="wizard-progress-top" id="progress-top">
				<div class="step complete">
					<div class="node">
						<img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green">
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>
<!-- Menu Top -->
<!-- Sidebar Navigation Desktop -->
<div class="sidebar-nav-left d-none d-lg-block sticky">
	<button class="sidebar-nav-toggle">
	<span class="lines"></span>
	</button>
	<div class="sidebar-nav-content bg-white text-blue">
		<div class="margin-top-20">
			<?php $this->load->view('es/includes/sidebar-clases-prueba'); ?>
		</div>
		</div><!-- end sidebar-nav-content -->
	</div>
	<!-- Inicia Contenido -->
	<div class="sidebar-wrapper-left">
		
		<!-- Full Video -->
		<div class="container-white-video">
			<div class="container">
				<div class="row" style="margin-bottom:0px;">
					<div class="col-12 col-sm-12" style="margin-bottom: 0px;">
						<div class="videoWrapper mx-auto d-block">							
							<video id="reproductor" controls="true" style="width:100%; height:auto;">
								<source src="<?= $current->video ?>" type="video/mp4" media="">
							</video>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div>
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12">
						<!-- 1 -->
						<div class="row text-center">
							<div class="col-12 col-sm-12 col-lg-6" style="margin-bottom: 0px;">
								<h3 class="text-blue text-responsive">									
									<img src="<?= $current->idioma ?>" alt="Idiomas" class="image-title-video-flag">
									<?= $current->nombre ?>
								</h3>
							</div>
							<div class="col-12 col-sm-12 col-lg-6 text-descarga-movil margin-top-10" >
								<a data-toggle="modal" data-target="#programa-completo" style="font-size: 12px; text-decoration: underline; cursor: pointer;">Ver Programa completo</a>
							</div>
						</div>
						<div class="row text-center margin-bottom-10">
							<div class="col-12">
								<h5>Lecciones para hoy:</h5>
							</div>
						</div>
						<div class="row text-center">
							
							<?php foreach($this->elements->clases_prueba(array(),FALSE)->result() as $v): ?>
								<div class="col-6 col-sm-6 col-lg-4">
									<!--<div class="image-lesion"><img src="https://picsum.photos/35/35/?image=75"/ class="rounded-circle box-shadow borde-img-lock" alt="Lesiones"></div>-->
									<a href="<?= $v->fichero ?>" title="Linkids Clases" >
										<div id="gallery" class="text-white active">
											<div id="images">
												<div class="figure">
													<img class="image" src="<?= $v->thumb ?>" alt="<?= $v->nombre ?>">
													<div class="caption">
														
													</div>
												</div>
											</div>
										</div>
									</a>
									<div class="title-lessons">
										<a href="<?= $v->fichero ?>" title="<?= $v->nombre ?>" >
											<?= $v->nombre ?> <img src="<?= $v->bandera ?>" alt="Idiomas">
										</a>
									</div>
								</div>
							<?php endforeach ?>
							
							
						</div>
						
					</div>
					</div><!-- end row -->
					</div><!-- end container -->
				</div>