
<!-- Menu Top Desktop -->
<header class="d-none d-lg-block">
	<?php $this->load->view('es/includes/menu-top',array(),FALSE,'paginas');?>
</header>

<header class="d-lg-none">
	<?php $this->load->view('es/includes/menu-top-perfil-movil',array(),FALSE,'paginas');?>
	<div class="alert text-white">
		<span class="closebtn" onclick="this.parentElement.style.display='none';"><i class="far fa-times-circle"></i></span>
	</div>
</header>

<!-- Inicia Contenido -->
	<div class="section container-white-top">
		<div class="container">			
			<div class="col-12 text-center">
				<h3 class="margin-bottom-20">Resultado de pago</h3>
			</div>
			<div class="col-12 text-center margin-bottom-30">
				<div class="row">
					<div class="col-12">
						Su pago no se pudo completar
					</div>
				</div>
			</div>			
		</div><!-- end container -->
	</div>