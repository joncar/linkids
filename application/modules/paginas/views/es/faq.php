<!-- Menu Top -->
    <header>
      <?php $this->load->view('es/includes/menu-valoracion',array(),FALSE,'paginas');?>
      <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy&display=swap" rel="stylesheet">
    </header>
    <!-- Menu Top -->

    <!-- Scroll to top button -->
    <div class="scrolltotop">
      <a class="button-circle button-circle-sm button-circle-dark" href="#">
        <i class="ti-arrow-up"></i>
      </a>
    </div>
    <!-- end Scroll to top button -->

    <!-- Social -->
    <div class="social">
      <ul>
        <li class="icon-whatsapp text-center"><a href="https://wa.me/5215517005534" title="Enviar mensaje"><i class="fab fa-whatsapp"></i></a></li>
        <li class="icon-planes text-center"><a href="<?= base_url() ?>main.html#planes" title="Ver Planes" id="margin-planes"><b>Planes</b></a></li>
      </ul>
    </div>

    <!-- Home section -->
    <div class="section-xl bg-image parallax bg-home-header hero-bkg-animated" style="background-image: url(<?= base_url() ?>theme/svg/home/header.svg)" id="inicio">
      <div class="container">
        <div class="row margin-top-50 margin-bottom-50 align-items-center">
          <div class="col-12 col-sm-12 text-center titulo-header-home">
          	<div class="margin-bottom-20">
	            <h1 class="font-weight-bold no-margin text-uppercase text-yellow margin-bottom-10 font-cursiva">
	              <span class="text-white"></span><br>Sección de Ayuda
	            </h1>
        	</div>
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Home section -->

    <!-- Services section -->
    <div class="section bg-orange" id="nosotros">
      <div class="container">

        <div class="row container-white-2">
          <div class="col-12 col-md-12 bg-white-home">

            <div class="row margin-bottom-30">
              <div class="col-12 col-sm-12">
                <ul class="accordion accordion-oneopen">
                  <!-- 1 -->
                  <li class="active margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿En qué consiste este Sistema de</span><br><div class="font-cursiva">Estimulación LINKIDS?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        El Sistema LINKIDS, consiste en aprovechar uno de de los principales momentos en la formación del cerebro humano, ésta etapa de desarrollo se presenta de manera natural y con una capacidad de absorción única durante los 
                        primeros momentos de vida ( desde el vientre materno y hasta finalizar la primera infancia) , por tal motivo trae los ingredientes necesarios para facilitar el aprendizaje de manera fácil y práctica durante toda la vida.
                      </p>
                    </div>
                  </li>
                  <!-- 2 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Cómo</span><br><div class="font-cursiva">lo uso?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        La metodología LINKIDS, es muy sencilla de usar, se trata de ver las clases que duran 9 minutos cada una, de manera constante, EJ: Lunes vemos en la mañana la clase 1, por la tarde la clase 2 y en la noche la clase 3 , al otro día en 
                        la mañana la clase 4, en la tarde la clase 5 y así todos los días, si algún día no lo ve , no hay mucho problema,  el secreto para que funcione, es la constancia. 
                      </p>
                    </div>
                  </li>
                  <!-- 3 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Qué pasa si</span><br><div class="font-cursiva">me salto una clase?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        Lo ideal sería llevar el programa como está presentado para lograr mayor contraste entre los diferentes idiomas, si UD, decide modificar la forma de aplicarlo, no le garantizamos que al año su hijo logre la asimilación de la información y 
                        la práctica en la secuencia adecuada.
                      </p>
                    </div>
                  </li>
                  <!-- 4 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Puedo ver más de</span><br><div class="font-cursiva">5 clases al día?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        NO sería lo recomendado, mínimo 3 clases al día y máximo 5, porque las clases están diseñadas justamente para lograr una mayor absorción entre una y otra. El cerebro debe descansar un tiempo considerado para lograr mejores resultados 
                        entre sesión y sesión, por ese motivo no se lo recomendamos.
                      </p>
                    </div>
                  </li>
                  <!-- 5 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿A qué edad puedo comenzar a utilizar</span><br><div class="font-cursiva">LINKIDS con mi hijo?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        Estudios científicos han revelado que el oído es el primer sentido que se desarrolla en el bebé y comienza desde las 18 semanas, NO desde la 32 como se pensaba. El cerebro se forma en el ambiente que le presentemos, y desde el 
                        vientre comienza esta gran oportunidad, ya que cuando nazca, nacerá con un cerebro mas ágil para enfrentar los retos durante toda su vida.<br>
                        Nuestro idioma materno fue desde el vientre y por eso lo dominamos muy bien, sucede exactamente lo mismo si le acercas otros idiomas.<br>
                        Que maravilla que exista LINKIDS, ¿verdad?
                      </p>
                    </div>
                  </li>
                  <!-- 6 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Cuánto dura</span><br><div class="font-cursiva">una clase?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        Cada clase dura entre 9 y 10 minutos y ya están programadas, de manera que cuando termine su clase, lo regresará al menú.
                      </p>
                    </div>
                  </li>
                  <!-- 7 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Si mi hijo no lo ve,</span><br><div class="font-cursiva">le sirve igual?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        Por su puesto que SI le sirve, el hecho que no vea las clases, NO significa que no oiga, y auditivamente se estaría estimulando. Ese sería el caso de bebés pequeños menores a 3 meses 
                        que su desarrollo auditivo es mayor que el visual.
                      </p>
                    </div>
                  </li>
                  <!-- 8 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Qué pasa si mi hijo</span><br><div class="font-cursiva">aún no sabe leer?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        No hay de que preocuparse, porque el sistema LINKIDS está diseñado justamente para activar el área del conocimiento que necesita para comenzar a leer.
                      </p>
                    </div>
                  </li>
                  <!-- 9 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Qué pasa si yo como papá no conozco</span><br><div class="font-cursiva">todos los idiomas?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        No hay ningún problema, porque justamente el sistema es quien se encargaría que su hijo los entienda rápidamente, gracias a la práctica, secuencia y exposición constante, y sin
                        dificultades debido a la plasticidad neuronal que se presenta en el período de la primera infancia.
                      </p>
                    </div>
                  </li>
                  <!-- 10 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Qué pasa si mi hijo tiene</span><br><div class="font-cursiva">capacidades diferentes?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        La Estimulación Temprana abarca a todos los niños por igual, y más a los que tienen capacidades diferentes porque son quienes más lo necesitan, 
                        convirtiéndose en una Intervención Temprana por excelencia.
                      </p>
                    </div>
                  </li>
                  <!-- 11 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Si mi hijo aún no habla, se podría confundir al exponerlo</span><br><div class="font-cursiva">a más idiomas?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        Si le llevará unos meses más de tiempo para empezar a hablar en la lengua materna, porque tendrá que organizar neurológicamente hablando mayor información. Sin embargo su hijo podrá hacer uso del idioma que requiera para 
                        comunicarse con las personas que le rodean, porque lo seleccionará de acuerdo al hablante de manera natural.
                      </p>
                    </div>
                  </li>
                  <!-- 12 -->
                  <li class="margin-bottom-20">
                    <div class="accordion-title">
                      <h2 class="margin-bottom-20 subtitulo-home text-blue"><span style="color:#000000;">¿Cuánto dura el</span><br><div class="font-cursiva">programa LINKIDS?</div></h2>
                    </div>
                    <div class="accordion-content">
                      <p>
                        El programa LINKIDS es ilimitado, cuanto más lo vea, más le sirve, la recomendación es que lo use año a año durante la primera infancia.<br>
                        Al Ud, notar en el primer año de uso los positivos avances de conocimiento de su hijo, seguro lo repetirá.
                      </p>
                    </div>
                  </li>

                </ul>
              </div>
            </div><!-- end row -->

          </div>
        </div>

      </div><!-- end container -->
    </div>
    <!-- end Services section -->

    <footer id="form-home">
      <div class="footer bg-blue">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-6 text-center text-md-left">
              <p class="margin-top-10 text-white">
                &copy; 2019 Linkids - ¡Evolución educativa en idiomas!<br>
                <a href="<?= base_url() ?>aviso.html" style="text-color:white; text-decoration:underline;">Aviso de Privacidad</a> |
                <a href="<?= base_url() ?>terminos.html"  style="text-color:white; text-decoration:underline;">Términos y Condiciones</a>
              </p>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right">
              <ul class="list-horizontal-unstyled">
                <li style="font-size: 24px;"><a href="https://www.facebook.com/Estimulaci%C3%B3n-Temprana-en-Idiomas-y-M%C3%BAsica-341652089375058/" target="blank" class="text-white"><i class="fab fa-facebook-f"></i></a></li>
                <li style="font-size: 24px;"><a href="https://www.instagram.com/linkidsevolucioneducativa/?igshid=19d64owl0uuub" target="blank" class="text-white"><i class="fab fa-instagram"></i></a></li>
                <li style="font-size: 24px;"><a href="https://www.youtube.com/channel/UCdFU2NHJZyDv-ShOopzhX4A" target="blank" class="text-white"><i class="fab fa-youtube"></i></a></li>
              </ul>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div>
    </footer>
