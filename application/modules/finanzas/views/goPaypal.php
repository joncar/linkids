<!DOCTYPE html>
<html lang="en">
<head>
  <title>MEMBRESIA EXPIRADA</title>
  <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
  <meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" />   
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>  
  <link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/> 
  <script>window.wURL = window.URL; var URL = '<?= base_url() ?>';</script>

  <!-- CSS -->
  <link href="<?= base_url() ?>theme/assets/plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>theme/assets/plugins/magnific-popup/magnific-popup.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>theme/assets/css/app.css" rel="stylesheet">
  <!-- Main -->
  <link href="<?= base_url() ?>theme/assets/css/main.css" rel="stylesheet">
  <link href="<?= base_url() ?>theme/assets/css/animaciones.css" rel="stylesheet">
  <link href="<?= base_url() ?>theme/assets/css/queries.css" rel="stylesheet">
  <!-- Fonts/Icons -->
  <link href="<?= base_url() ?>theme/assets/plugins/font-awesome/css/all.css" rel="stylesheet">
  <link href="<?= base_url() ?>theme/assets/plugins/themify/themify-icons.min.css" rel="stylesheet">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900" rel="stylesheet"> 

  <!-- SEO -->
  <link rel="canonical" href="https://www.linkids.com.mx" />
  <link rel='shortlink' href='https://www.linkids.com.mx' />
  <link rel="icon" href="#" sizes="32x32" />
  <link rel="icon" href="#" sizes="192x192" />
  <link rel="apple-touch-icon-precomposed" href="#" /> <!-- 180x180 -->
  <meta name="msapplication-TileImage" content="#" /> <!-- 270 x 270 -->

  <meta name="DC.title" content="LINKIDS">
  <meta name="DC.subject" content="Estimulación | 5 idiomas">
  <meta name="DC.creator" content="linkids">

  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@linkids">
  <meta name="twitter:creator" content="linkids">
  <meta name="twitter:title" content="Estimulación | 5 idiomas">
  <meta name="twitter:description" content="Imágenes y música">
  <meta name="twitter:image:src" content="#"> <!-- 660 x 1024 -->
  <meta name="twitter:domain" content="https://twitter.com/linkids">

  <meta property="og:url" content="www.linkids.com.mx">
  <meta property="og:title" content="Estimulación | 5 idiomas" >
  <meta property="og:description" content="Imágenes y música" />
  <meta property="og:image" content="#" > <!-- 660 x 1024 -->

  <link rel="canonical" href="www.linkids.com.mx" />
  <!-- SEO -->

  <script src="<?= base_url() ?>theme/assets/plugins/jquery.min.js"></script>
</head>
<body data-preloader="2" style="overflow: hidden;">

    <div class="section-fullscreen bg-image parallax bg-home-header bg-animate" style="background-image: url(<?= base_url() ?>theme/svg/home/header.svg)" id="inicio">
        <div class="row align-items-center position-middle text-center">
          <div class="col-12 col-sm-12 titulo-header-home text-center">
            <div class="margin-bottom-50"><img src="http://bluepixel.mx/linkids/theme/svg/logo-login.svg" alt="Logo Linkids" style="width: 20%;"></div>
            <h1 class="font-weight-bold no-margin text-uppercase text-yellow" style="line-height: 0.4;">
              ¡Gracias!<br><br><span class="text-white">Por seleccionar tu plan.<br> Te estamos redireccionando a Paypal, espera un momento</span>
            </h1>
          </div>
        </div><!-- end row -->
    </div>

    <!-- Librerias -->
    <?php $this->load->view('es/includes/librerias.php',array(),FALSE,'paginas');?>
    <!-- Modales -->
    <?php $this->load->view('es/includes/modales.php',array(),FALSE,'paginas');?>
  
  <form id="form" action="https://www.paypal.com/cgi-bin/webscr" method="post">
  <!--<form id="form" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">-->
    <input type="hidden" name="cmd" value="_xclick-subscriptions">
    <input type="hidden" name="item_name" value="LIN<?= $trans ?> - <?= $membresia->nombre ?>">     
    <input type="hidden" name="item_number" value="<?= $trans ?>">     
    <!--<input type="hidden" name="image_url" value="<?= base_url() ?>theme/svg/home/logo.png">-->
    <!--<input type="hidden" name="business" value="esteban_ataides-facilitator@hotmail.com">-->
    <input type="hidden" name="business" value="esteban_ataides@hotmail.com">
    <input type="hidden" name="currency_code" value="MXN">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="a3" value="<?= round($membresia->precio,0) ?>">
    <input type="hidden" name="p3" value="<?= $membresia->intervalo ?>"> 
    <input type="hidden" name="t3" value="<?= $membresia->frecuencia ?>">
    <input type="hidden" name="src" value="1">
    <input type="hidden" name="sra" value="1">
    <input type="hidden" name="sra" value="<?= $trans ?>">
    <input type="hidden" name="return" value="<?= base_url() ?>pagook.html">
    <input type="hidden" name="return_cancel" value="<?= base_url() ?>pagoko.html">     
    <input type="hidden" name="notify_url" value="<?= base_url() ?>finanzas/frontend/procesarPagoPaypal/<?= $trans ?>">
    Redireccionando a paypal, por favor espere.
  </form>
		<script>
			setTimeout(function(){document.getElementById('form').submit();},2000);
		</script>
  </body>
</html>