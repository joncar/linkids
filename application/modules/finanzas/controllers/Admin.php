<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function membresias(){
        	$crud = $this->crud_function('','');
        	$crud->field_type('periocidad','dropdown',array('30'=>'Mensual','90'=>'Trimestral','180'=>'Semestral','365'=>'Anual'));
        	$crud->display_as('cantidad_cuentas','Cantidad máxima de cuentas a registrar (0 = ilimitado)');
            $crud->field_type('foto','image',array('path'=>'img/membresias','width'=>'300px','height'=>'300px'));
        	$crud = $crud->render();
        	$this->loadView($crud);
        }

        function transacciones(){
            $crud = $this->crud_function('','');
            $crud->field_type('procesado','dropdown',array('1'=>'Aperturado','2'=>'Procesado'));
            $crud->field_type('tipo_pago','dropdown',array('1'=>'Paypal','2'=>'Stripe','3'=>'Manual'));
            $crud->display_as('procesado','Estado');
            $crud->callback_column('se8701ad4',function($val,$row){
                return '<a href="'.base_url('seguridad/user/edit/'.$row->user_id).'">'.$val.'</a>';
                return $val;
            });
            $crud->unset_edit()->unset_read();
            $crud->display_as('user_id','Cliente');
            $crud->columns('user_id','importe','fecha','procesado');
            $crud->callback_before_delete(function($primary){
                if(get_instance()->db->get_where('transacciones',array('id'=>$primary))->row()->procesado==2){
                    return false;
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
