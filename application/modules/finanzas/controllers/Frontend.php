<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();            
        }        

        function procesarPagoPaypal($id = ''){
            //correo('joncar.c@gmail.com','TEST '.$id,print_r($_POST,TRUE));
            //$_POST = array('transaction_subject'=>'LIN0 - Plan Familiar','payment_date'=>'01:10:13 Jul 23, 2019 PDT','txn_type'=>'subscr_payment','subscr_id'=>'I-CSPFMBW6CFFG','last_name'=>'Test','residence_country'=>'MX','item_name'=>'LIN0 - Plan Familiar','payment_gross] =>','mc_currency'=>'MXN','business'=>'esteban_ataides-facilitator@hotmail.com','payment_type'=>'instant','protection_eligibility'=>'Eligible','verify_sign'=>'AOe5GMXPVi.RDWyRpZd2KpGKFCL5AB4pPyOb1YceCcFUxcmkoymZ-HNV','payer_status'=>'verified','test_ipn'=>'1','payer_email'=>'test@linkids.com','txn_id'=>'1FR31454S9030811P','receiver_email'=>'esteban_ataides-facilitator@hotmail.com','first_name'=>'Test','payer_id'=>'6ZKL55UXTE4CG','receiver_id'=>'RKC79SYHA5M9L','contact_phone'=>'939-653-2321','item_number'=>'0','payment_status'=>'Completed','payment_fee] =>','mc_fee'=>'16.55','mc_gross'=>'260.00','charset'=>'windows-1252','notify_version'=>'3.9','ipn_track_id'=>'81cc54cca6d90');
            $venue = $this->db->get_where('transacciones',array('transacciones.id'=>$id));               
            if($venue->num_rows()>0 && !empty($_POST)){
                $post = $_POST;
                if($post['payment_status']=='Completed' || $post['payer_status'] == 'verified'){

                    if($venue->row()->procesado!=2){
                        $this->db->update('transacciones',array(
                            'procesado'=>2,
                            'payer_id'=>$_POST['payer_id'],
                            'subscribe_id'=>$_POST['subscr_id'],
                            'verify_sign'=>$_POST['verify_sign']
                        ),array('id'=>$id));
                    }else{
                        $this->db->insert('transacciones',array(
                            'importe'=>$venue->row()->importe,
                            'descripcion'=>$venue->row()->descripcion,
                            'fecha'=>date("Y-m-d"),
                            'user_id'=>$venue->row()->user_id,
                            'tipo_pago'=>$venue->row()->tipo_pago,
                            'procesado'=>2,
                            'payer_id'=>$_POST['payer_id'],
                            'subscribe_id'=>$_POST['subscr_id'],
                            'verify_sign'=>$_POST['verify_sign']
                        ));
                        $id = $this->db->insert_id();
                    }
                    $this->db->delete('transacciones',array('procesado'=>1,'user_id'=>$venue->row()->user_id));
                    $this->db->select('fecha,importe,descripcion');
                    $transaccion = $this->db->get_where('transacciones',array('id'=>$id))->row();
                    $user = $this->db->get_where('user',array('id'=>$venue->row()->user_id))->row();
                    $datos = '';
                    foreach($transaccion as $n=>$v){
                        $datos.='<p><b>'.$n.'</b>: '.$v.'</p>';
                    }
                    $datos.='<p><b>Vencimiento</b>: '.$user->fecha_vencimiento.'</p>';
                    $post = (object)array('email'=>$user->email,'nombre'=>$user->nombre,'datos'=>$datos);
                    //correo('joncar.c@gmail.com','Pago recibido por PAYPAL de linkids',print_r($_POST,TRUE));
                    $this->enviarcorreo($post,15);
                    $this->enviarcorreo($post,15,'contacto@linkids.com.mx');
                }else{
                    correo('joncar.c@gmail.com','Pago recibido por PAYPAL de linkids pero fue rechazado',print_r($_POST,TRUE));
                }
            }
        }

        function procesarPagoPaypalGeneric(){
            correo('joncar.c@gmail.com','Pago recibido por renovación',print_r($_POST,TRUE));            
        }
    }
?>
