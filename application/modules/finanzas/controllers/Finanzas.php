<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Finanzas extends Panel{        
        function __construct() {
            parent::__construct();            
        }

        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posees permisos para realizar esta operación','403');
                }
                else{                    
                    if(!empty($param->output)){
                        $panel = $this->user->admin==1?'panel':'panelUsuario';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = 'template';
                    $this->load->view($template,$param);
                }                
            }
        }

        function cargarPago(){
        	$this->loadView(array(
        		'view'=>'read',
        		'title'=>'Clases',
        		'page'=>$this->load->view('es/cargarPago',array(),TRUE,'paginas'),        		
        	));
        }

        function pagarConPaypal($id){
            $membresia = $this->elements->membresias(array('id'=>$id));
            if($membresia->num_rows()>0){
                $this->db->insert('transacciones',array(
                    'user_id'=>$this->user->id,
                    'procesado'=>1,
                    'fecha'=>date("Y-m-d H:i:s"),
                    'importe'=>$membresia->row()->precio,
                    'descripcion'=>'Pago por renovación '.$membresia->row()->nombre,
                    'tipo_pago'=>1
                ));                
                $vencimiento = date("Y-m-d",strtotime(date("Y-m-d").' +'.$membresia->row()->_periocidad.' days'));                                
                $trans = $this->db->insert_id();
                $this->db->update('user',array('membresias_id'=>$membresia->row()->id,'fecha_vencimiento'=>$vencimiento),array('id'=>$this->user->id));
                $this->load->view('goPaypal',array('membresia'=>$membresia->row(),'trans'=>$trans));
            }
        }

        function procesarPagoPaypal($id = ''){
            mail('joncar.c@gmail.com','TEST '.$id,print_r($_POST,TRUE));
            $venue = $this->db->get_where('transacciones',array('transacciones.id'=>$id));               
            if($venue->num_rows()>0 && !empty($_POST)){
                $post = $_POST;
                if($post['payment_status']=='Completed'){
                    $this->db->update('transacciones',array('procesado'=>2),array('id'=>$id));
                    $user = $this->db->get_where('transacciones',array('id'=>$id))->row()->user_id;
                    $this->db->delete('transacciones',array('procesado'=>1,'user_id'=>$user));
                    mail('joncar.c@gmail.com','Pago recibido por PAYPAL de linkids',print_r($_POST,TRUE));
                }
            }
        }

        function pagarConStripe($id){
            $membresia = $this->elements->membresias(array('id'=>$id));
            if($membresia->num_rows()>0 && !empty($_GET['token'])){
                $token = json_decode($_GET['token']);
                $this->db->insert('transacciones',array(
                    'user_id'=>$this->user->id,
                    'procesado'=>1,
                    'fecha'=>date("Y-m-d H:i:s"),
                    'importe'=>$membresia->row()->precio,
                    'descripcion'=>'Pago por renovación '.$membresia->row()->nombre,
                    'tipo_pago'=>2
                ));
                $trans = $this->db->insert_id();
                $this->load->view('goStripe',array('membresia'=>$membresia->row(),'trans'=>$trans,'token'=>$token));
            }
        }
    }
?>
