<aside id="sidebar" class="sidebar col-md-4 col-sm-12">
  
  <!-- Widget -->
  <div class="widget with-bg">
    
    <div class="search-holder">
      <form class="contact-form style-2" action="<?= base_url('blog') ?>">
        <button type="submit" class="search-button"></button>
        <div class="wrapper">
          <input type="text" name="direccion" placeholder="Search">
        </div>
      </form>
    </div>
  </div>
  
  <!-- Widget -->
  <div class="widget with-bg">    
    <h5 class="widget-title">Categories</h5>
    <ul class="minus-list">
      <?php foreach($categorias->result() as $c): ?>
      <li> 
          <a href="<?= base_url('blog').'?categorias_id='.$c->id ?>"> 
              <?= $c->blog_categorias_nombre ?>              
          </a>
      </li> 
      <?php endforeach ?>
    </ul>
  </div>
  
  <!-- Widget -->
  <div class="widget with-bg">
    <h5 class="widget-title">Latest Articles</h5>
    <?php foreach($recientes->result() as $r): ?>    
      <div class="entry-box entry-small">        
        <!-- entry -->
        <div class="entry">          
          <!-- - - - - - - - - - - - - - Entry attachment - - - - - - - - - - - - - - - - -->
          <div class="thumbnail-attachment">
            <a href="<?= $r->link ?>"><img src="<?= $r->titulo ?>" alt=""></a>
          </div>          
          <!-- - - - - - - - - - - - - - Entry body - - - - - - - - - - - - - - - - -->
          <div class="entry-body">
            <div class="entry-meta">
              <time class="entry-date" datetime="<?= $r->fecha ?>"><?= $r->fecha ?></time>
            </div>
            <h6 class="entry-title"><a href="<?= $r->link ?>"><?= $r->titulo ?></a></h6>
          </div>
        </div>
        
      </div>
    <?php endforeach ?>
  </div>
  
  <!-- Widget -->
  <div class="widget with-bg">
    
    <h5 class="widget-title">Tags</h5>
    <div class="tagcloud">
      <?php foreach($tags as $t): ?>
        <a href="<?= base_url('blog') ?>?direccion=<?= $t ?>"><?= $t ?></a>
      <?php endforeach ?>
    </div>
  </div>
  
</aside>