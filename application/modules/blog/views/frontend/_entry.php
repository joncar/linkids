<a href="<?= base_url('blog') ?>?page=1" class="prev-page"><i class="fa fa-angle-left"></i></a>

<?php for($i=1;$i<=$total_pages;$i++): ?>
  
    <a href='<?= base_url('blog') ?>?page=<?= $i ?>' class="<?= $i==$current_page?'active':'' ?>">
      <?= $i ?>
    </a>
  
<?php endfor ?>
<a href="<?= base_url('blog') ?>?page=<?= $total_pages ?>" class="next-page"><i class="fa fa-angle-right"></i></a>