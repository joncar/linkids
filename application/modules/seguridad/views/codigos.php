<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title">Generador de códigos</h1>
	</div>
	<div class="panel-body">
		<form action="seguridad/codigos/<?= $x ?>/generate" onsubmit="if(confirm('Seguro que desea generar estos códigos?'))sendForm(this,'.result'); return false;">
		  <div class="form-group">
		    <label for="exampleInputEmail1">Cantidad de códigos a generar</label>
		    <input type="number" name="codigos" class="form-control" placeholder="Cantidad de códigos">
		  </div>	
		  <div class="form-group">
		    <label for="exampleInputEmail1">Vencimiento</label>
		    <input type="date" name="vencimiento" class="form-control" placeholder="dia/mes/año (<?= date("d/m/Y") ?>)">
		  </div>
		  <div class="result"></div>	  
		  <button type="submit" class="btn btn-default">Generar</button>
		</form>
	</div>
</div>