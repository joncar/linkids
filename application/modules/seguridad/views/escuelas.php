<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Ficha</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Códigos</a></li>
    <li role="presentation"><a href="#profile2" aria-controls="profile" role="tab" data-toggle="tab">Pagos</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <?= $output->output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <?= $output->codigos ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile2">
      <?= $output->transacciones ?>
    </div>
  </div>

</div>