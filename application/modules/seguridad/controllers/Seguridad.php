<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        const IDROLUSERADMIN = 1;
        const IDROLUSER = 2;

        function __construct() {
            parent::__construct();
        }

        function menus(){
            $crud = $this->crud_function('','');  
            $crud->set_relation('menus_id','menus','nombre',array('menus_id'=>NULL));                 
            $output = $crud->render();
            $this->loadView($output);
        }

        function cookies(){
            $this->loadView(array(
                'view'=>'panel',
                'crud'=>'user',
                'output'=>$this->load->view('cookies',array(),TRUE)
            ));
        }

        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->unset_add()->unset_delete()->unset_read()->unset_export()->unset_print();
            $crud->field_type('keywords','tags');
            $crud->columns('id');
            $crud->field_type('analytics','string')
                 ->field_type('cookies','string')
                 ->field_type('idiomas','tags')
                 ->set_field_upload('favicon','img')
                 ->set_field_upload('logo','img')
                 ->set_field_upload('logo_light','img')
                 ->set_field_upload('fondo','img')
                 ->field_type('mapa','tags')
                 ->display_as('idiomas','Idiomas (Separados por coma[,])')
                 ->display_as('mapa','Mapa (Formato[Lat, Lon])');
            $crud = $crud->render();
            $this->loadView($crud);
        } 
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}');
            $crud->set_relation_n_n('miembros','user_group','user','grupo','user','{email}');                                 
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user($x = '',$y = ''){
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            $crud->set_subject('Administradores');
            $crud->field_type('status','true_false',array('0'=>'Inactivo','1'=>'Activo'));
            $crud->field_type('admin','hidden',1);
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password')
                 ->field_type('clave_escolar','hidden')
                 ->field_type('escuela','hidden')
                 ->field_type('user_vinculacion','hidden')
                 ->field_type('codigo_descuento','hidden')
                 ->field_type('tipo_usuario','hidden',1);
            if($crud->getParameters()=='add'){
                $crud->set_rules('repetir_password','Repetir Password','required');            
                $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
            }            
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                $post['fecha_registro'] = date("Y-m-d");
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::IDROLUSERADMIN));
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);                
                return $post;
            });
            $crud->where('admin',1);
            $crud->columns('foto','nombre','apellido_paterno','email','status');
            $crud->add_action('Usar usuario','',base_url('seguridad/usar/').'/');
            $output = $crud->render();
            $output->title = 'Administradores';
            $this->loadView($output);
        }

        function usar($id){
            //session_destroy();
            $this->user->login_short($id);
            redirect('panel');
        }

        function individuos($x = '',$y = ''){
            $this->as['individuos'] = 'user';
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            $crud->set_subject('Individuos');
            $crud->field_type('status','true_false',array('0'=>'Inactivo','1'=>'Activo'));
            $crud->field_type('admin','hidden',0);
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password')
                 ->field_type('clave_escolar','hidden')
                 ->field_type('escuela','hidden')
                 ->field_type('user_vinculacion','hidden')
                 ->field_type('codigo_descuento','hidden')
                 ->field_type('tipo_usuario','hidden',1)
                 ->field_type('fecha_vencimiento','hidden')
                 ->set_relation('membresias_id','membresias','{periocidad} dias - {nombre}')
                 ->display_as('apellido_paterno','Apellidos')
                 ->display_as('membresias_id','Tipo de cuenta');
            if($crud->getParameters()=='add'){
                $crud->set_rules('repetir_password','Repetir Password','required');            
                $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
            }else{
                $crud->field_type('generar_factura','hidden',0);
            }            
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');
            $crud->callback_before_insert(function($post){
                $membresia = get_instance()->db->get_where('membresias',array('id'=>$post['membresias_id']))->row();
                $post['password'] = md5($post['password']);
                $post['fecha_registro'] = date("Y-m-d");
                $post['admin'] = 0;
                $post['fecha_vencimiento'] = date("Y-m-d",strtotime(date("Y-m-d").' +'.$membresia->periocidad.' days'));
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::IDROLUSER));
                if($post['generar_factura']==1){
                    //Generamos cobro para que pueda acceder
                    $membresia = get_instance()->db->get_where('membresias',array('id'=>$post['membresias_id']))->row();
                    get_instance()->db->insert('transacciones',array('importe'=>$membresia->precio,'tipo_pago'=>'3','descripcion'=>'creación manual de usuario','fecha'=>date("Y-m-d"),'user_id'=>$primary,'procesado'=>2));
                }
            });
            $crud->callback_before_update(function($post,$primary){
                if($post['membresias_id']!=get_instance()->db->get_where('user',array('id'=>$primary))->row()->membresias_id){
                    $membresia = get_instance()->db->get_where('membresias',array('id'=>$post['membresias_id']))->row();    
                    $post['fecha_vencimiento'] = date("Y-m-d",strtotime(date("Y-m-d").' +'.$membresia->periocidad.' days'));
                }
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);                
                return $post;
            });
            $crud->callback_after_delete(function($primary){
                get_instance()->db->delete('user_group',array('user'=>$primary));                
                get_instance()->db->delete('transacciones',array('user_id'=>$primary));                
            });
            $crud->where('tipo_usuario',1);
            $crud->where('admin !=',1);
            $crud->columns('foto','nombre','apellido_paterno','email','status','membresias_id');
            $crud->add_action('Usar usuario','',base_url('seguridad/usar/').'/');
            $output = $crud->render();
            $output->title = 'Individuos';
            $this->loadView($output);
        }

        function escuelas($x = '',$y = ''){
            $this->as['escuelas'] = 'user';
            $this->norequireds = array('apellido_paterno','foto','admin');
            $crud = $this->crud_function($x,$y);  
            $crud->set_subject('Escuelas');
            $crud->field_type('status','true_false',array('0'=>'Inactivo','1'=>'Activo'));
            $crud->field_type('admin','hidden',0);
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password')
                 ->field_type('clave_escolar','hidden')
                 ->field_type('apellido_paterno','hidden')
                 ->field_type('escuela','hidden')
                 ->field_type('user_vinculacion','hidden')
                 ->field_type('codigo_descuento','hidden')
                 ->field_type('tipo_usuario','hidden',2)
                 ->field_type('fecha_vencimiento','hidden')
                 ->display_as('membresias_id','Tipo de cuenta');
            if($crud->getParameters()=='add'){
                $crud->set_rules('repetir_password','Repetir Password','required');            
                $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
            } else{
                $crud->field_type('generar_factura','hidden',0);
            }           
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');
            $crud->callback_before_insert(function($post){
                $membresia = get_instance()->db->get_where('membresias',array('id'=>$post['membresias_id']))->row();
                $post['apellido_paterno'] = 'Escuela';
                $post['admin'] = 0;
                $post['password'] = md5($post['password']);
                $post['fecha_registro'] = date("Y-m-d");
                $post['fecha_vencimiento'] = date("Y-m-d",strtotime(date("Y-m-d").' +'.$membresia->periocidad.' days'));
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::IDROLUSER));
                if($post['generar_factura']==1){
                    //Generamos cobro para que pueda acceder
                    $membresia = get_instance()->db->get_where('membresias',array('id'=>$post['membresias_id']))->row();
                    get_instance()->db->insert('transacciones',array('importe'=>$membresia->precio,'tipo_pago'=>'3','descripcion'=>'creación manual de usuario','fecha'=>date("Y-m-d"),'user_id'=>$primary,'procesado'=>2));
                }
            });
            $crud->callback_before_update(function($post,$primary){
                if($post['membresias_id']!=get_instance()->db->get_where('user',array('id'=>$primary))->row()->membresias_id){
                    $membresia = get_instance()->db->get_where('membresias',array('id'=>$post['membresias_id']))->row();    
                    $post['fecha_vencimiento'] = date("Y-m-d",strtotime(date("Y-m-d").' +'.$membresia->periocidad.' days'));
                }
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);                
                return $post;
            });
            $crud->callback_after_delete(function($primary){
                get_instance()->db->delete('user_group',array('user'=>$primary));                
                get_instance()->db->delete('transacciones',array('user_id'=>$primary));                
            });
            $crud->where('tipo_usuario',2);
            $crud->columns('foto','nombre','email','status','membresias_id');
            $crud->add_action('Usar usuario','',base_url('seguridad/usar/').'/');
            $output = $crud->render();
            if($crud->getParameters()=='edit' && is_numeric($y)){
                $output->codigos = $this->codigos($y);
                $output->transacciones = $this->transacciones($y)->output;
                $output->js_files = array_merge($output->js_files,$output->codigos->js_files);
                $output->css_files = array_merge($output->css_files,$output->codigos->css_files);
                $output->codigos = $output->codigos->output;
                $output->output = $this->load->view('escuelas',array('output'=>$output),TRUE,'seguridad');
            }
            $output->title = 'Escuelas';
            $this->loadView($output);
        }

        function codigos($x = '',$y = ''){
            if($y=='generate'){
                if(empty($_POST['codigos']) || !is_numeric($_POST['codigos']) || empty($x)){
                    echo $this->error('Debe indicar el número de códigos a generar');
                    die();
                }
                if(empty($_POST['vencimiento'])){
                    echo $this->error('Debe indicar la fecha de vencimiento');
                    die();
                }
                for($i = 0;$i<$_POST['codigos'];$i++){
                    $vencimiento = $_POST['vencimiento'];
                    $vencimiento = date("Y-m-d",strtotime(str_replace('/','-',$vencimiento)));
                    $this->db->insert('codigos',array(
                        'user_id'=>$x,
                        'codigo'=>uniqid(),
                        'fecha'=>date("Y-m-d H:i:s"),
                        'vencimiento'=>$vencimiento
                    ));
                }
                echo $this->success('Códigos generados <a href="'.base_url('seguridad/escuelas/edit/'.$x).'">Volver al listado</a>');
            }else{
                $this->as['escuelas'] = 'codigos';
                $crud = $this->crud_function('','');
                $crud->set_subject('Codigos');
                $crud->where('user_id',$x)
                     ->field_type('user_id','hidden',$x)
                     ->field_type('fecha','hidden')
                     ->unset_columns('user_id');
                $crud->callback_column('usado',function($val){return empty($val)?'NO':'SI';});
                $crud->set_url('seguridad/codigos/'.$x.'/');
                if($this->router->fetch_method()!='codigos'){
                    $crud = $crud->render(1);
                    return $crud;
                }            
                $crud->unset_edit()
                     ->unset_read();
                $output = $crud->render();
                if($crud->getParameters()=='add'){
                    $output->output = $this->load->view('codigos',array('x'=>$x),TRUE);
                }
                $this->loadView($output);
            }
        }

        function transacciones($x = '',$y = ''){
            $this->as['escuelas'] = 'transacciones';
            $crud = $this->crud_function('','');
            $crud->set_subject('Transacciones');
            $crud->where('user_id',$x)
                 ->field_type('user_id','hidden',$x)                 
                 ->unset_columns('user_id')
                 ->display_as('procesado','Estado')
                 ->field_type('tipo_pago','dropdown',array('1'=>'paypal','2'=>'stripe'))
                 ->field_type('procesado','dropdown',array('1'=>'Aperturado','2'=>'Procesado','3'=>'Manual'));
            $crud->set_url('seguridad/transacciones/'.$x.'/');
            if($this->router->fetch_method()!='transacciones'){
                $crud = $crud->render(1);
                return $crud;
            }
            $output = $crud->render();
            $this->loadView($output);
        }



        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'user';
            $crud = $this->crud_function($x,$y);    
            $crud->where('id',$this->user->id);
            /*$crud->fields('nombre','apellido','password','centro','curso','nro_grupo','foto');*/
            $crud->field_type('password','password')
                 ->field_type('admin','hidden')
                 ->field_type('status','hidden');
            $crud->field_type('foto','image',array('path'=>'img/fotos','width'=>'300px','height'=>'300px'));
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
            });
            $crud->unset_back_to_list()->unset_list()->unset_add()->unset_delete()->unset_export()->unset_print()->unset_read();
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password']?md5($post['password']):$post['password'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }
    }
?>
