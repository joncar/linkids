<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	/*
	DAR PROGRESO A TODOS LOS VIDEOS DE ROOT
	INSERT INTO progresos SELECT NULL,'1',view_videos_orden.id,100,'2019-05-27'FROM view_videos_orden LEFT JOIN progresos ON progresos.videos_id = view_videos_orden.id AND progresos.user_id = '1' ORDER BY clasesorden
	*/

	function getVideosEnOrden(){				
		$this->db->select('view_videos_orden.*,IF(progresos.videos_id IS NULL,0,1) as activo, progresos.fecha_apertura,progresos.id as progresoid',FALSE);
		$this->db->join('progresos','progresos.videos_id = view_videos_orden.id AND progresos.user_id = '.$this->user->id,'LEFT');		
		$this->db->order_by('clasesorden','ASC');
		return $this->db->get_where('view_videos_orden');
	}
	function getProgreso($id = '',$forceApertura = false){
		if(is_numeric($id)){
			$this->db->where(array('view_videos_orden.id'=>$id));
		}	
		$this->db->where('IF(progresos.videos_id IS NULL,0,1) = 1',NULL,FALSE);
		$progreso = $this->getVideosEnOrden();

		if($progreso->num_rows()==0){
			//Aperturamos la primera clase
			$this->db->where('IF(progresos.videos_id IS NULL,0,1) = 1',NULL,FALSE);
			$videos = $this->getVideosEnOrden();			
			if($videos->num_rows()==0 || $forceApertura){					
				$videos = $this->getVideosEnOrden();				
				if(!is_numeric($id)){
					$id = $videos->num_rows()>0?$videos->row()->id:null;
				}		
				if($id){
					$this->db->insert('progresos',array('user_id'=>$this->user->id,'videos_id'=>$id,'progreso'=>0,'fecha_apertura'=>date("Y-m-d H:i:s")));
					return $this->db->get_where('progresos',array('id'=>$this->db->insert_id()))->row();
				}
			}
			
		}else{			
			return $progreso->row($progreso->num_rows()-1); //Retornamos el ultimo aperturado
		}
		return false;
	}

	function progresos($where = array()){		
		$this->db->where('IF(progresos.videos_id IS NULL,0,1) = 1 AND fecha_apertura = \''.date("Y-m-d").'\'',NULL,FALSE);
		$data = $this->getVideosEnOrden();					
		return $data;
	}

	function getCurrentVideoData(){
		$this->db->order_by('progresos.id','DESC');
		$videos = $this->getVideosEnOrden();
		return $videos;
	}

	function getCurrentVideo($id){
		$videos = $this->getCurrentVideoData();
		if($videos->num_rows()>0){
			if($videos->row()->id==$id){
				return true;
			}
		}
		return false;
	}


	function videos($where = array()){		
		$data = $this->db->get_where('view_videos_orden',$where);
		foreach($data->result() as $n=>$v){
			$data->row($n)->_fichero = $v->fichero;
			$data->row($n)->fichero = base_url('clases/videos/'.base64_encode($v->id));
			$data->row($n)->thumb = base_url('img/thumbs/'.$v->thumb);
			$data->row($n)->activo = $this->getProgreso($v->id);			
			$data->row($n)->activo = $data->row($n)->activo && $data->num_rows()>0?true:false;			$data->row($n)->link = $data->row($n)->activo?base_url('clases/'.base64_encode($v->id)):'javascript:$(\'#clase-bloqueada\').modal(\'toggle\')';
			$data->row($n)->current = $this->getCurrentVideo($v->id);
			$data->row($n)->idiomas = $this->idiomas_clases(array('idiomas_clases.id'=>$v->idiomas_clases_id))->row();
			$data->row($n)->idioma = $data->row($n)->idiomas->nombre;
			$data->row($n)->bandera = $data->row($n)->idiomas->bandera;
			$data->row($n)->categorias = $this->db->get_where('clases_categorias',array('videos_id'=>$v->id));
		}
		return $data;
	}

	function idiomas_clases($where = array()){		
		$data = $this->db->get_where('idiomas_clases',$where);
		foreach($data->result() as $n=>$v){
			$data->row($n)->bandera = base_url('img/idiomas/'.$v->bandera);
		}
		return $data;
	}

	function clases($where = array()){
		$this->db->order_by('orden','ASC');
		$data = $this->db->get_where('clases',$where);
		foreach($data->result() as $n=>$v){
			$data->row($n)->videos = $this->videos(array('clases_id'=>$v->id));
			$data->row($n)->idiomas = $this->idiomas_clases(array('idiomas_clases.id'=>$v->idiomas_clases_id))->row();
		}
		return $data;
	}



	function getNext(){
		$progreso = $this->getProgreso();

		if($progreso){
			$videos = $this->videos();
			foreach($videos->result() as $n=>$v){				
				if($v->id==$progreso->videos_id && ($n+1)<=$videos->num_rows()){				
					return $videos->row($n+1);
				}
			}
			return false;
		}else{
			return false;
		}
	}

	function videos_ocacionales($where = array()){
		$this->db->order_by('orden','ASC');		
		$data = $this->db->get_where('videos_ocacionales',$where);		
		foreach($data->result() as $n=>$v){
			$data->row($n)->_fichero = $v->video;
			$data->row($n)->fichero = base_url('musica-completa/'.$v->_fichero);
			$data->row($n)->thumb = base_url('img/thumbs/'.$v->miniatura);			
			$data->row($n)->link = base_url('videos_ocacionales/'.base64_encode($v->id));
			$data->row($n)->idiomas_clases = $this->idiomas_clases(array('id'=>$v->idiomas_clases_id))->row();
		}
		return $data;
	}

	/* Retorna array 0 = mas_alta, 1 = actual*/
	function getRacha(){		
		$progresos = $this->db->query('SELECT COUNT(id) as videos,fecha_apertura FROM progresos WHERE user_id='.$this->user->id.' GROUP BY fecha_apertura ORDER BY fecha_apertura ASC');
		
		if($progresos->num_rows()==0){
			return array(0,0);
		}
		if($progresos->num_rows()==1){
			return array(1,1);
		}

		$mas_alta = $progresos->row(0)->videos;
		$actual = $progresos->row(0)->videos;

		for($i=1;$i<$progresos->num_rows();$i++){
			$hoy = strtotime($progresos->row($i)->fecha_apertura.' -1 days');
			$ayer = strtotime($progresos->row($i-1)->fecha_apertura);			
			if($hoy==$ayer){
				$actual+= $progresos->row($i)->videos;
				$mas_alta = $actual>$mas_alta?$actual:$mas_alta;
			}else{
				$actual = $progresos->row($i)->videos;
			}
		}		
		return array($mas_alta,$actual);
	}

	function getVideosHoy(){		
		$data = $this->getVideosEnOrden();
		foreach($data->result() as $n=>$d){
			if($d->fecha_apertura==date("Y-m-d")){
				$ret = array();
				$ret[] = $this->videos(array('view_videos_orden.id'=>$data->row($n)->id))->row();
				if($n+1<$data->num_rows()){
					$ret[] = $this->videos(array('view_videos_orden.id'=>$data->row($n+1)->id))->row();
				}
				if($n+2<$data->num_rows()){
					$ret[] = $this->videos(array('view_videos_orden.id'=>$data->row($n+2)->id))->row();
				}
				if($n+3<$data->num_rows()){
					$ret[] = $this->videos(array('view_videos_orden.id'=>$data->row($n+3)->id))->row();
				}

				if($n+4<$data->num_rows()){
					$ret[] = $this->videos(array('view_videos_orden.id'=>$data->row($n+4)->id))->row();
				}

				if($n+5<$data->num_rows()){
					$ret[] = $this->videos(array('view_videos_orden.id'=>$data->row($n+5)->id))->row();
				}
				return $ret;
			}
		}
		//Si no hay aperturados hoy se apertura
		return array();
	}

	function getMaxVideos(){
		$grupo = $this->user->getGroups();
		$grupo = $grupo->row();
		return $grupo->cant_max_visualizaciones;
	}

	function insignias($where = array()){
		$this->db->order_by('orden','ASC');		
		$data = $this->db->get_where('insignias',$where);		
		foreach($data->result() as $n=>$v){			
			$data->row($n)->imagen = base_url('img/insignias/'.$v->imagen);			
		}
		return $data;
	}

	function transacciones($where = array()){
		$where['user_id'] = $this->user->id;
		$data = $this->db->get_where('transacciones',$where);
		return $data;
	}

	function membresias($where = array()){	
		$data = $this->db->get_where('membresias',$where);
		$tipos = array('30'=>'Mensual','90'=>'Trimestral','180'=>'Semestral','365'=>'Anual');
		$frecuencias = array('30'=>'M','90'=>'M','180'=>'M','365'=>'Y');
		$frecuencias_full = array('30'=>'month','90'=>'month','180'=>'Semestral','365'=>'year');
		$intervalos = array('30'=>'1','90'=>'3','180'=>'6','365'=>'1');
		foreach($data->result() as $n=>$v){			
			$data->row($n)->foto = base_url('img/membresias/'.$v->foto);	
			$data->row($n)->cantidad_cuentas = $v->cantidad_cuentas==0?'cuentas ilimitadas':$v->cantidad_cuentas.' cuentas';					
			$data->row($n)->frecuencia = $frecuencias[$v->periocidad];
			$data->row($n)->frecuencia_full = $frecuencias_full[$v->periocidad];			
			$data->row($n)->intervalo = $intervalos[$v->periocidad];
			$data->row($n)->_periocidad = $v->periocidad;
			$data->row($n)->periocidad = $tipos[$v->periocidad];
		}
		return $data;
	}

	function canAccess(){
		//Si no esta el vencimiento en el object user se extrae
		if(empty($this->user->vencimiento) && !empty($this->user->codigo_descuento)){
			$codigo = $this->db->get_where('codigos',array('codigo'=>$this->user->codigo_descuento));
			if($codigo->num_rows()>0){
				$_SESSION['vencimiento'] = $codigo->row()->vencimiento;
				$this->user->login_short($this->user->id);
			}
		}

		if(!empty($this->user->user_vinculacion)){
			$vencimiento = strtotime($this->user->vencimiento);
			if(empty($this->user->vencimiento) || time()>$vencimiento){
				$this->db->where('transacciones.id',-1);
			}else{
				$this->db->where('procesado = 2 AND (user_id = '.$this->user->id.' OR user_id = '.$this->user->user_vinculacion.')',NULL,TRUE);
			}
		}else{
			$this->db->where('procesado',2);		
			$this->db->where('user_id',$this->user->id);
		}

		$trans = $this->db->get_where('transacciones');
		if($trans->num_rows()>0){
			return true;
		}
		return false;
	}

	function clases_prueba($where = array(),$filterURL = TRUE){		
		$this->db->select('clases_prueba.id as cid,videos.*');
		$this->db->join('videos','videos.id = clases_prueba.videos_id');
		//Where
		if($filterURL){
			if(!empty($_GET['v'])){
				$id = base64_decode($_GET['v']);
				if(is_numeric($id)){
					$this->db->where('clases_prueba.id',$id);
				}
			}
		}
		$data = $this->db->get_where('clases_prueba',$where);
		foreach($data->result() as $n=>$v){
			$data->row($n)->_fichero = $v->fichero;
			$data->row($n)->video = base_url('clases/frontend/videos/'.base64_encode($v->cid));
			$data->row($n)->fichero = base_url('clases-prueba.html?v='.base64_encode($v->cid));			
			$data->row($n)->thumb = base_url('img/thumbs/'.$v->thumb);						
			$data->row($n)->idiomas = $this->idiomas_clases(array('idiomas_clases.id'=>$v->idiomas_clases_id))->row();
			$data->row($n)->idioma = $data->row($n)->idiomas->nombre;
			$data->row($n)->bandera = $data->row($n)->idiomas->bandera;
			$data->row($n)->categorias = $this->db->get_where('clases_categorias',array('videos_id'=>$v->id));
		}
		return $data;
	}

	
}