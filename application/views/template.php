<!doctype html>
<html lang="es">
	<title><?= empty($title) ? 'WEB' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">




	<!--<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/><!-->
	<!--<<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	<!-->
	<script>window.wURL = window.URL; var URL = '<?= base_url() ?>';</script>


<!-- Favicon -->
<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>theme/svg/home/logo.png"><!-- 180 x 180 -->
<link rel="icon" type="image/jpg" sizes="32x32" href="<?= base_url() ?>theme/svg/home/logo.png"> <!-- 32 x 32 -->
<link rel="icon" type="image/jpg" sizes="16x16" href="<?= base_url() ?>theme/svg/home/logo.png"> <!-- 32 x 32 -->
<link rel="manifest" href="<?= base_url() ?>theme/svg/home/logo.png">
<link rel="mask-icon" href="<?= base_url() ?>theme/svg/home/logo.png" color="#FB1E40">






	<!-- CSS -->
	<link href="<?= base_url() ?>theme/assets/plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/magnific-popup/magnific-popup.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/css/app.css" rel="stylesheet">
	<!-- Main -->
	<link href="<?= base_url() ?>theme/assets/css/main.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/css/animaciones.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/css/queries.css" rel="stylesheet">
	<!-- Fonts/Icons -->
	<link href="<?= base_url() ?>theme/assets/plugins/font-awesome/css/all.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/themify/themify-icons.min.css" rel="stylesheet">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Luckiest+Guy&display=swap" rel="stylesheet"> 

	<!-- SEO -->
	<link rel="canonical" href="https://www.linkids.com.mx" />
	<link rel='shortlink' href='https://www.linkids.com.mx' />
	<link rel="icon" href="#" sizes="32x32" />
	<link rel="icon" href="#" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="#" /> <!-- 180x180 -->
	<meta name="msapplication-TileImage" content="#" /> <!-- 270 x 270 -->

	<meta name="DC.title" content="LINKIDS">
	<meta name="DC.subject" content="Estimulación | 5 idiomas">
	<meta name="DC.creator" content="linkids">

	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@linkids">
	<meta name="twitter:creator" content="linkids">
	<meta name="twitter:title" content="Estimulación | 5 idiomas">
	<meta name="twitter:description" content="Imágenes y música">
	<meta name="twitter:image:src" content="#"> <!-- 660 x 1024 -->
	<meta name="twitter:domain" content="https://twitter.com/linkids">

	<meta property="og:url" content="www.linkids.com.mx">
	<meta property="og:title" content="Estimulación | 5 idiomas" >
	<meta property="og:description" content="Imágenes y música" />
	<meta property="og:image" content="#" > <!-- 660 x 1024 -->

	<link rel="canonical" href="www.linkids.com.mx" />
	<!-- SEO -->



	<script src="<?= base_url() ?>theme/assets/plugins/jquery.min.js"></script>
</head>

<body data-preloader="2">
	<?php $this->load->view($view); ?>
	<!-- Librerias -->
    <?php $this->load->view('es/includes/librerias',array(),FALSE,'paginas');?>
    <!-- Modales -->
    <?php $this->load->view('es/includes/modales',array(),FALSE,'paginas');?>
</body>
</html>
