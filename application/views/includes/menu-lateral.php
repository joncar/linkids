<!-- Sidebar scroll-->
<div class="scroll-sidebar">
    <!-- User profile -->
    <div class="user-profile" style="background: url(assets/images/background/user-info.jpg) no-repeat;">
        <!-- User profile image -->
        <div class="profile-img"> 
            <img src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="user" /> 
        </div>
    </div>
    <!-- End User profile text-->
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
        <ul id="sidebarnav">
            <?php  
                $menu = $this->user->filtrarMenu();
                echo getMenu($menu); 
             ?>
        </ul>
    </nav>
    <!-- End Sidebar navigation -->
</div>
<!-- End Sidebar scroll-->
<!-- Bottom points-->
<div class="sidebar-footer">
<!--<a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
<a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>-->
<a href="<?= base_url('main/unlog') ?>" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
<!-- End Bottom points-->