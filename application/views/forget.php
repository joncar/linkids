<!doctype html>
<html lang="es">
	<title><?= empty($title) ? 'WEB' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" /> 	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<script>window.wURL = window.URL; var URL = '<?= base_url() ?>';</script>

	<!-- CSS -->
	<link href="<?= base_url() ?>theme/assets/plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/magnific-popup/magnific-popup.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/css/app.css" rel="stylesheet">
	<!-- Main -->
	<link href="<?= base_url() ?>theme/assets/css/main.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/css/animaciones.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/css/queries.css" rel="stylesheet">
	<!-- Fonts/Icons -->
	<link href="<?= base_url() ?>theme/assets/plugins/font-awesome/css/all.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/themify/themify-icons.min.css" rel="stylesheet">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900" rel="stylesheet"> 

	<!-- SEO -->
	<link rel="canonical" href="https://www.linkids.com.edu/" />
	<link rel='shortlink' href='https://www.linkids.com.edu/' />
	<link rel="icon" href="#" sizes="32x32" />
	<link rel="icon" href="#" sizes="192x192" />
	<link rel="apple-touch-icon-precomposed" href="#" /> <!-- 180x180 -->
	<meta name="msapplication-TileImage" content="#" /> <!-- 270 x 270 -->

	<meta name="DC.title" content="LINKIDS">
	<meta name="DC.subject" content="Linkids | Cursos de capacitación en idiomas">
	<meta name="DC.creator" content="linkids">

	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@linkids">
	<meta name="twitter:creator" content="linkids">
	<meta name="twitter:title" content="LINKIDS / CURSOS">
	<meta name="twitter:description" content="Cursos de capacitación en idiomas...">
	<meta name="twitter:image:src" content="#"> <!-- 660 x 1024 -->
	<meta name="twitter:domain" content="https://twitter.com/linkids">

	<meta property="og:url" content="https://www.linkids.com.edu/">
	<meta property="og:title" content="LINKIDS / CURSOS" > 
	<meta property="og:description" content="Cursos de capacitación en idiomas..." /> 
	<meta property="og:image" content="#" > <!-- 660 x 1024 -->

	<link rel="canonical" href="https://www.linkids.com.edu" />
	<!-- SEO --> 
	<script src="<?= base_url() ?>theme/assets/plugins/jquery.min.js"></script>
</head>

<body data-preloader="2">


<?php if(!empty($_SESSION['user'])): redirect('clases'); endif ?>
<!-- Sidebar Navigation Desktop -->
<div class="sidebar-nav-left d-none d-lg-block">
  <button class="sidebar-nav-toggle" style="z-index: 999999999;"><span class="lines"></span></button>
  <div class="sidebar-nav-content" id="sidebar-nav-login" style="border: 0px; background-color: transparent;">

    <div class="col-12 col-sm-12 margin-bottom-30">
      <a href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/assets/images/logo-linkids.png" alt="Logo Linkids" srcset="<?= base_url() ?>theme/svg/logo-login.svg"></a>
    </div>

    <div class="col-12 text-center">
      <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
            <?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?>
            <?= !empty($msj)?$msj:'' ?>
            <?= input('email','Email','email') ?>        
            <div align='center'><button type="submit" class="btn btn-success">Recuperar contraseña</button></div>
        </form>
    </div><!-- end contact-form -->

  </div><!-- end sidebar-nav-content -->
</div>
<!-- end Sidebar Navigation -->

<!-- Sidebar Navigation Movil -->
<div class="d-lg-none" id="sidebar-nav-login-movil">
  <div class="col-12 col-sm-12 margin-bottom-30">
    <a href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/assets/images/logo-linkids.png" alt="Logo Linkids" srcset="<?= base_url() ?>theme/svg/logo-login.svg" class="logo-movil mx-auto d-block"></a>
  </div>

  <div class="col-12 text-center">
       <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
            <?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?>
            <?= !empty($msj)?$msj:'' ?>
            <?= input('email','Email','email') ?>        
            <div align='center'><button type="submit" class="btn btn-success">Recuperar contraseña</button></div>
        </form>
    </div><!-- end contact-form -->
</div>

<!-- SVG Desktop Login -->
<div class="sidebar-wrapper-left container-profile d-none d-lg-block">
		<div class="section container-white-none animacion-login">
    <span class="cloud cloud--small"></span>
    <span class="cloud cloud--medium"></span>
    <span class="cloud cloud--large"></span>
    <img src="<?= base_url() ?>theme/svg/hombre.svg" id="hombre">
    <img src="<?= base_url() ?>theme/svg/montana.svg" id="montana">


    <div class="sun" id="sol">
        <div class="ray_box">
            <div class="ray ray1"></div>
            <div class="ray ray2"></div>
            <div class="ray ray3"></div>
            <div class="ray ray4"></div>
            <div class="ray ray5"></div>
            <div class="ray ray6"></div>
            <div class="ray ray7"></div>
            <div class="ray ray8"></div>
            <div class="ray ray9"></div>
            <div class="ray ray10"></div>
        </div>
    </div>

  </div>
</div>

	<!-- Librerias -->
    <?php $this->load->view('es/includes/librerias',array(),FALSE,'paginas');?>
    <!-- Modales -->
    <?php $this->load->view('es/includes/modales',array(),FALSE,'paginas');?>
</body>
</html>