<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Registro extends Main {
        const IDROLUSER = 2;
        const NOTIFID = 12;
        public function __construct()
        {
            parent::__construct();         
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }
        
        public function loadView($param = array('view'=>'main'))
        {
                if(!empty($param->output)){
                    $panel = 'panel';
                    $param->view = empty($param->view)?$panel:$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                if(is_string($param)){
                    $param = array('view'=>$param);
                }
                $template = 'templateadmin';
                $this->load->view($template,$param);
        }

        public function index($url = 'main',$page = 0)
        {
            if($this->router->fetch_class()=='registro'){
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('bootstrap2');
                $crud->set_table('user');
                $crud->set_subject('<span style="font-size:18px">Nuevo usuario</span>');
                //Fields

                //unsets
                $crud->unset_back_to_list()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_edit()
                     ->unset_list()
                     ->unset_export()
                     ->unset_print()
                     ->field_type('fecha_registro','invisible')
                     ->field_type('fecha_actualizacion','invisible')
                     ->field_type('status','invisible')
                     ->field_type('foto','invisible')
                     ->field_type('admin','invisible')
                     ->field_type('password','password')
                     ->field_type('cedula','invisible')
                     ->field_type('apellido_materno','invisible')
                     ->field_type('tiene_android','hidden',1);

                $crud->display_as('password','Contraseña nuevo usuario')
                     ->display_as('apellido_paterno','Apellidos')                                         
                     ->display_as('email','Email de contacto');

                $redirect = empty($_SESSION['carrito'])?'panel':'store/checkout';
                $crud->set_lang_string('insert_success_message','Usuario creado con éxito <script>setTimeout(function(){document.location.href="'.base_url($redirect).'"; },2000)</script>');                
                $crud->set_lang_string('form_add','');
                $crud->required_fields('password','email','nombre','apellido','centro','nro_grupo','curso');
                $crud->callback_field('referencia_grupo',function(){
                    return form_input('referencia_grupo','','class="form-control" id="field-referencia_grupo" placeholder="Ej. M19001 (Nº Documento Reserva)"');
                })->callback_field('Instituto',function(){
                    return form_input('Instituto','','class="form-control" id="field-instituto"').
                           '<div style="margin-top:10px;"><b>Importante</b> Recordar la contraseña siempre que quieras entrar al sistema</div>';
                });
                //Displays             
                $crud->set_lang_string('form_save','REGISTRAR');

                $crud->callback_before_insert(array($this,'binsertion'));
                $crud->callback_after_insert(array($this,'ainsertion'));
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
                $crud->set_rules('codigo_descuento','Clave de registro','callback_validate_clave');
                if($crud->getParameters(FALSE)=='insert_validation'){
                    $crud->fields('nombre','apellido_paterno','email','codigo_descuento','password','password2','politicas');
                    $crud->set_rules('politicas','Politicas','required');
                }
                $crud->required_fields('password','email','nombre','apellido_paterno','direccion','codigo_postal','telefono','ciudad','provincias_id','referencia_grupo','Instituto');
                $output = $crud->render();
                $output->view = 'registro';
                $output->crud = 'user';
                $output->title = 'REGISTRAR';
                $output->output = $output->output;
                $output->scripts = get_header_crud($output->css_files,$output->js_files,TRUE);
                $this->loadView($output);   
            }
        }     

        function validate_clave(){
            $clave = $_POST['codigo_descuento'];
            if(!empty($clave)){
                //VAlidamos                
                $this->db->where('codigo = \''.$clave.'\' AND usado IS NULL',NULL,FALSE);
                if($this->db->get_where('codigos')->num_rows()==0){
                    $this->form_validation->set_message('validate_clave','La clave de registro introducida no es correcta');
                    return false;
                }
            }      
        }         
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {            
            $post['status'] = 1;
            $post['admin'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            $post['tipo_usuario'] = 1;
            get_instance()->contrasena = $post['password'];
            $post['password'] = md5($post['password']);
            if(!empty($post['codigo_descuento'])){
                //Buscamos el user
                $post['user_vinculacion'] = $this->db->get_where('codigos',array('codigo'=>$post['codigo_descuento']))->row()->user_id;
            }
            return $post;
        }
        
        function ainsertion($post,$primary)
        {              
            //Asignar rol
            get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::IDROLUSER));            
            get_instance()->user->login_short($primary);    
            $post['contrasena'] = get_instance()->contrasena;
            get_instance()->enviarcorreo((object)$post,self::NOTIFID);
            get_instance()->db->update('codigos',array('usado'=>$primary),array('codigo'=>$post['codigo_descuento']));
            return true;
        }
        
        function reset(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('pass','Contraseña','required|min_length[8]');
            $this->form_validation->set_rules('pass2','Contraseña','required|min_length[8]|matches[pass]');            
            if($this->form_validation->run())
            {
                $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$this->input->post('email')));
                session_unset();
                $this->loadView(array('view'=>'registro','msj'=>$this->success('Se ha restablecido su contraseña <a href="'.base_url().'">Volver al inicio</a>')));
            }
            else{
                $key = empty($_POST['email'])?'':$_POST['email'];
                $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
            }
        }
       
        function forget($key = '',$ajax = '')
        {
            if(empty($_POST) && empty($key)){
                $this->loadView(array('view'=>'forget'));
            }
            else
            {                                
                if(empty($key)){
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    if($this->form_validation->run())
                    {
                        $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                        if($user->num_rows()>0){
                            $_SESSION['key'] = md5($this->input->post('email'));
                            $_SESSION['email'] = $this->input->post('email');
                            $user->row()->link = base_url('registro/forget/'.base64_encode($_POST['email']));
                            $this->enviarcorreo($user->row(),13);
                            echo $this->success('Se han enviado los pasos de restauración a su correo');
                            
                        }
                        else{
                            echo $this->error('El correo utilizado no esta registrado');
                        }
                    }
                    else{
                        echo $this->error($this->form_validation->error_string());
                    }
                }
                else
                {
                    $key = base64_decode($key);
                    $this->loadView(array('view'=>'recover','key'=>$key));                    
                }
            }
        }        
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
