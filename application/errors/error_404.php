<!DOCTYPE html>
<html lang="en">
<head>
  <?php $this->load->view('es/includes/head.php',array(),FALSE,'paginas');?>
</head>
<body data-preloader="2" style="overflow: hidden;">

    <div class="section-fullscreen bg-image parallax bg-home-header bg-animate" style="background-image: url(SVG/Home/header.svg)" id="inicio">
        <div class="row align-items-center position-middle text-center">
          <div class="col-12 col-sm-12 titulo-header-home text-center">
            <div class="margin-bottom-50"><img src="http://bluepixel.mx/linkids/theme/svg/logo-login.svg" alt="Logo Linkids" style="width: 20%;"></div>
            <h1 class="font-weight-bold no-margin text-uppercase text-yellow" style="line-height: 0.2;">
              ¡UPS!<br><br><span class="text-white">Error 404.<br>Lo sentimos, pero esta página no está disponible</span>
            </h1>
            <a class="button button-md button-green-home margin-top-30" href="http://bluepixel.mx/linkids/">Volver a linkids.com</a>
          </div>
        </div><!-- end row -->
    </div>

    <!-- Librerias -->
    <?php $this->load->view('es/includes/librerias.php',array(),FALSE,'paginas');?>
    <!-- Modales -->
    <?php $this->load->view('es/includes/modales.php',array(),FALSE,'paginas');?>

  </body>
</html>

