<!-- navbar -->
<nav class="navbar navbar-absolute navbar-fixed">
  <div class="container">
    <a class="navbar-brand img-menu-top" href="index.php"><img src="svg/home/logo.svg" alt="Logo Linkids"></a>
    <ul class="nav">
      <li class="nav-item"><a class="nav-link" href="#inicio" id="menu-home"><i class="fab fa-fort-awesome-alt icon-menu"></i> Inicio</a></li>
      <li class="nav-item"><a class="nav-link" href="#nosotros" id="menu-home"><i class="fas fa-award icon-menu"></i> Nosotros</a></li>
      <li class="nav-item"><a class="nav-link" href="#contacto" id="menu-home"><i class="fas fa-id-badge icon-menu"></i> Contacto</a></li>
      <li class="nav-item"><a class="nav-link" href="login.php" id="menu-home"><i class="fas fa-user-circle icon-menu"></i> Iniciar sesión</a></li>
    </ul>
    <!-- Nav Toggle button -->
    <button class="nav-toggle-btn"><span class="lines"></span></button>
  </div>
</nav>
<!-- end navbar -->
