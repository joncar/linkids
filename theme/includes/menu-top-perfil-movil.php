<!-- navbar -->
<nav class="navbar navbar-sticky poppins">
  <div class="container text-center">

    <ul class="nav">
      <div class="name-movil"><span>Bienvenido a Linkids</span><br>Alan Torres</div>
      <li>
        <form action="" class="search">
          <div class="field">
            <input type="text" class="input-search" id="input-search" name="input-search" required>
            <label for="input-search">Buscar</label>
          </div> <!-- /field -->
        </form>
      </li>
      <li class="nav-item"><a class="nav-link" href="index.php"><i class="fas fa-user-circle text-pink"></i> Iniciar sesión/Perfil</a></li>
      <li class="nav-item"><a class="nav-link" href="clases.php"><i class="fas fa-play-circle text-pink"></i> Clases</a></li>
      <li class="nav-item"><a class="nav-link" href="videos.php"><i class="fas fa-headphones-alt text-pink"></i> Música</a></li>
      <li class="nav-item"><a class="nav-link" href="index.php"><i class="fab fa-fort-awesome text-pink"></i> Sobre Nosotros</a></li>
      <li class="nav-item salir-movil"><a class="nav-link" href="index.php" style="color: white;">Salir de Linkids <i class="fas fa-sign-out-alt"></i></a></li>
    </ul><!-- end nav -->

    <a class="navbar-brand img-menu-top" href="index.php"><img src="assets/images/logo-linkids.png" alt="Logo Linkids" srcset="svg/logo-login.svg"></a>

    <!-- Nav Toggle button -->
    <button class="nav-toggle-btn">
        <span class="lines"></span>
    </button>

  </div><!-- end container -->
</nav><!-- end navbar -->
