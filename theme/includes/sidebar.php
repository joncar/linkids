  <button class="sidebar-nav-toggle">
      <span class="lines"></span>
  </button>
  <div class="sidebar-nav-content bg-gradient-nav">

  <div class="sidebar-logo"><h4 class="no-margin">Mono</h4></div>

  <ul class="sidebar-menu">
    <li>
      <a href="#">Link only</a>
    </li>
    <li>
      <a class="sidebar-dropdown-link" href="#">Dropdown</a>
      <!-- Dropdown -->
      <ul class="sidebar-dropdown">
        <li>
          <a href="#">Dropdown 1</a>
        </li>
        <li>
          <a href="#">Dropdown 2</a>
        </li>
      </ul>
    </li>
    <li>
      <a class="sidebar-dropdown-link" href="#">Sub Dropdown</a>
      <!-- Dropdown -->
      <ul class="sidebar-dropdown">
        <li>
          <a class="sidebar-dropdown-link" href="#">Dropdown 1</a>
          <!-- Sub Dropdown -->
          <ul class="sidebar-dropdown">
            <li>
              <a href="#">Sub Dropdown 1</a>
            </li>
            <li>
              <a href="#">Sub Dropdown 2</a>
            </li>
          </ul>
        </li>
        <li>
          <a class="sidebar-dropdown-link" href="#">Dropdown 2</a>
          <!-- Sub Dropdown -->
          <ul class="sidebar-dropdown">
            <li>
              <a href="#">Sub Dropdown 1</a>
            </li>
            <li>
              <a href="#">Sub Dropdown 2</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul><!-- end sidebar-menu -->
  <div class="sidebar-bottom">
    <ul class="list-horizontal-unstyled margin-bottom-10">
      <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
      <li><a href="#"><i class="fab fa-twitter"></i></a></li>
      <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
    </ul>
    <p>&copy; 2019 Linkids</p>
  </div><!-- end sidebar-bottom -->
</div><!-- end sidebar-nav-content -->
