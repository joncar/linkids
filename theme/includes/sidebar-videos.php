
<button class="sidebar-nav-toggle">
    <span class="lines"></span>
</button>

<div class="sidebar-nav-content bg-white" id="sidebar-videos">

    <form action="" class="search">
      <div class="field">
        <input type="text" class="input-search" id="input-search-sidebar" name="input-search-sidebar" required>
        <label for="input-search-sidebar">Buscar</label>
      </div> <!-- /field -->
    </form>

    <div class="margin-top-20">
      <!-- List Video -->
      <a href="#" title="Video Linkids">
        <div class="row video-list align-items-center">
          <div class="col-12 col-sm-2 no-padding" style="margin-bottom: 0px;"><img src="assets/images/videos/video-01.jpg" alt="Imagen Video"></div>
          <div class="col-12 col-sm-8" style="margin-bottom: 0px;"><span class="font-weight-bold">Animal songs compil…</span><br>Jennifer T. Coy</div>
          <div class="col-12 col-sm-2 no-padding text-center" style="margin-bottom: 0px;">03:16</div>
        </div>
      </a>
      <!-- List Video -->

      <!-- List Video -->
      <a href="#" title="Video Linkids">
        <div class="row video-list align-items-center">
          <div class="col-12 col-sm-2 no-padding" style="margin-bottom: 0px;"><img src="assets/images/videos/video-02.jpg" alt="Imagen Video"></div>
          <div class="col-12 col-sm-8" style="margin-bottom: 0px;"><span class="font-weight-bold">Animal songs compil…</span><br>Jennifer T. Coy</div>
          <div class="col-12 col-sm-2 no-padding text-center" style="margin-bottom: 0px;">03:16</div>
        </div>
      </a>
      <!-- List Video -->

      <!-- List Video -->
      <a href="#" title="Video Linkids">
        <div class="row video-list align-items-center">
          <div class="col-12 col-sm-2 no-padding" style="margin-bottom: 0px;"><img src="assets/images/videos/video-03.jpg" alt="Imagen Video"></div>
          <div class="col-12 col-sm-8" style="margin-bottom: 0px;"><span class="font-weight-bold">Animal songs compil…</span><br>Jennifer T. Coy</div>
          <div class="col-12 col-sm-2 no-padding text-center" style="margin-bottom: 0px;">03:16</div>
        </div>
      </a>
      <!-- List Video -->

      <!-- List Video -->
      <a href="#" title="Video Linkids">
        <div class="row video-list align-items-center">
          <div class="col-12 col-sm-2 no-padding" style="margin-bottom: 0px;"><img src="assets/images/videos/video-04.jpg" alt="Imagen Video"></div>
          <div class="col-12 col-sm-8" style="margin-bottom: 0px;"><span class="font-weight-bold">Animal songs compil…</span><br>Jennifer T. Coy</div>
          <div class="col-12 col-sm-2 no-padding text-center" style="margin-bottom: 0px;">03:16</div>
        </div>
      </a>
      <!-- List Video -->

      <!-- List Video -->
      <a href="#" title="Video Linkids">
        <div class="row video-list align-items-center">
          <div class="col-12 col-sm-2 no-padding" style="margin-bottom: 0px;"><img src="assets/images/videos/video-05.jpg" alt="Imagen Video"></div>
          <div class="col-12 col-sm-8" style="margin-bottom: 0px;"><span class="font-weight-bold">Animal songs compil…</span><br>Jennifer T. Coy</div>
          <div class="col-12 col-sm-2 no-padding text-center" style="margin-bottom: 0px;">03:16</div>
        </div>
      </a>
      <!-- List Video -->

      <!-- List Video -->
      <a href="#" title="Video Linkids">
        <div class="row video-list align-items-center">
          <div class="col-12 col-sm-2 no-padding" style="margin-bottom: 0px;"><img src="assets/images/videos/video-06.jpg" alt="Imagen Video"></div>
          <div class="col-12 col-sm-8" style="margin-bottom: 0px;"><span class="font-weight-bold">Animal songs compil…</span><br>Jennifer T. Coy</div>
          <div class="col-12 col-sm-2 no-padding text-center" style="margin-bottom: 0px;">03:16</div>
        </div>
      </a>
      <!-- List Video -->

      <!-- List Video -->
      <a href="#" title="Video Linkids">
        <div class="row video-list align-items-center">
          <div class="col-12 col-sm-2 no-padding" style="margin-bottom: 0px;"><img src="assets/images/videos/video-07.jpg" alt="Imagen Video"></div>
          <div class="col-12 col-sm-8" style="margin-bottom: 0px;"><span class="font-weight-bold">Animal songs compil…</span><br>Jennifer T. Coy</div>
          <div class="col-12 col-sm-2 no-padding text-center" style="margin-bottom: 0px;">03:16</div>
        </div>
      </a>
      <!-- List Video -->
    </div>

</div><!-- end sidebar-nav-content -->