<!-- navbar -->
<nav class="navbar navbar-sticky poppins">
  <div class="container text-center">

    <ul class="nav">
      <li class="nav-item"><a class="nav-link" href="clases.php"><i class="fas fa-play-circle text-pink"></i> Clases</a></li>
      <li class="nav-item"><a class="nav-link" href="videos.php"><i class="fas fa-headphones-alt text-pink"></i>  Música</a></li>
      <li class="nav-item"><a class="nav-link" href="index.php"><i class="fab fa-fort-awesome text-pink"></i> Sobre Nosotros</a></li>
    </ul><!-- end nav -->

    <a class="navbar-brand img-menu-top" href="index.php"><img src="assets/images/logo-linkids.png" alt="Logo Linkids" srcset="svg/logo-login.svg"></a>

    <ul class="list-horizontal-unstyled position-center position-lg-right margin-menu-search nav">
      <li>
        <form action="" class="search">
          <div class="field">
            <input type="text" class="input-search" id="input-search" name="input-search" required>
            <label for="input-search">Buscar</label>
          </div> <!-- /field -->
        </form>
      </li>
      
      <!-- Dropdown -->
      <li class="nav-item nav-dropdown">
        <a class="nav-link img-profile-top" href="#"><img src="https://picsum.photos/g/25/25" class="rounded-circle box-shadow borde-img-top" alt="Perfil Linkids"></a>
            <ul class="dropdown-menu">
          <li><a href="perfil.php"><i class="fas fa-user-circle"></i> Ver mi Perfil</a></li>
          <li><a href="index.php"><i class="fas fa-sign-out-alt"></i> Salir</a></li>
        </ul>
      </li>
    </ul>

    <!-- Nav Toggle button -->
    <button class="nav-toggle-btn">
        <span class="lines"></span>
    </button>

  </div><!-- end container -->
</nav><!-- end navbar -->
