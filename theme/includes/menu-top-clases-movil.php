<!-- navbar -->
<nav class="navbar navbar-sticky poppins">
  <div class="container text-center">

    <ul class="nav">
      <div class="name-movil"><span>Bienvenido a Linkids</span><br>Alan Torres</div>
      <li>
        <form action="" class="search">
          <div class="field">
            <input type="text" class="input-search" id="input-search" name="input-search" required>
            <label for="input-search">Buscar</label>
          </div> <!-- /field -->
        </form>
      </li>
      <li class="nav-item"><a class="nav-link" href="index.php"><i class="fas fa-user-circle text-pink"></i> Iniciar sesión/Perfil</a></li>
      <li class="nav-item"><a class="nav-link" href="clases.php"><i class="fas fa-play-circle text-pink"></i> Clases</a></li>
      <li class="nav-item"><a class="nav-link" href="videos.php"><i class="fas fa-headphones-alt text-pink"></i> Música</a></li>
      <li class="nav-item"><a class="nav-link" href="index.php"><i class="fab fa-fort-awesome text-pink"></i> Sobre Nosotros</a></li>
      <li class="progreso-movil" style="margin-bottom: 20px;">
        <div class="font-weight-bold text-center">Nombre de la clase</div>
        <img class="mx-auto d-block" src="assets/images/videos/video-03.jpg"/ alt="Videos Linkids">
        <p class="text-sidebar-bar">
          <span style="color:green;">¡Completada!</span><br>
          <a href="#" title="Clase Linkids">Clase 35: Inglés <img src="assets/images/banderas/United-States-of-America(USA).png" alt="Idiomas"></a>
          <div class="margin-top-10 list-menu-sidebar list-movil">
            <ul>
              <li style="border-bottom: #e2e2e2 solid 1px; padding: 10px 0px;"><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Ciudades <span class="badge">¡PLAY!</span></a></li>
              <li style="border-bottom: #e2e2e2 solid 1px; padding: 10px 0px;"><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Números <span class="badge">¡PLAY!</span></a></li>
              <li style="border-bottom: #e2e2e2 solid 1px; padding: 10px 0px;"><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Elementos</a></li>
              <li style="border-bottom: #e2e2e2 solid 1px; padding: 10px 0px;"><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Elementos</a></li>
              <li style="border-bottom: #e2e2e2 solid 1px; padding: 10px 0px;"><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Elementos</a></li>
              <li style="border-bottom: #e2e2e2 solid 1px; padding: 10px 0px;"><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Elementos</a></li>
            </ul>
          </div>
        </p>
      </li>
      <li class="nav-item salir-movil"><a class="nav-link" href="index.php" style="color: white;">Salir de Linkids <i class="fas fa-sign-out-alt"></i></a></li>
    </ul><!-- end nav -->

    <a class="navbar-brand img-menu-top" href="index.php"><img src="assets/images/logo-linkids.png" alt="Logo Linkids" srcset="svg/logo-login.svg"></a>

    <!-- Nav Toggle button -->
    <button class="nav-toggle-btn">
        <span class="lines"></span>
    </button>

  </div><!-- end container -->
</nav><!-- end navbar -->
