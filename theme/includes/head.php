<meta lang="es">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,  user-scalable=no">
<meta name="description" content="CURSOS DE CAPACITACIÓN EN IDIOMAS">
<title>Linkids</title>
<!-- Favicon -->
<link rel="icon" 
      type="image/png" 
      href="favicon.ico">
<!-- CSS -->
<link href="assets/plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
<link href="assets/plugins/magnific-popup/magnific-popup.min.css" rel="stylesheet">
<link href="assets/css/app.css" rel="stylesheet">
<!-- Main -->
<link href="assets/css/main.css" rel="stylesheet">
<link href="assets/css/animaciones.css" rel="stylesheet">
<link href="assets/css/queries.css" rel="stylesheet">
<!-- Fonts/Icons -->
<link href="assets/plugins/font-awesome/css/all.css" rel="stylesheet">
<link href="assets/plugins/themify/themify-icons.min.css" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700,900" rel="stylesheet"> 

<!-- SEO -->
<link rel="canonical" href="https://www.linkids.com.edu/" />
<link rel='shortlink' href='https://www.linkids.com.edu/' />
<link rel="icon" href="#" sizes="32x32" />
<link rel="icon" href="#" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="#" /> <!-- 180x180 -->
<meta name="msapplication-TileImage" content="#" /> <!-- 270 x 270 -->

<meta name="DC.title" content="LINKIDS">
<meta name="DC.subject" content="Linkids | Cursos de capacitación en idiomas">
<meta name="DC.creator" content="linkids">

<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@linkids">
<meta name="twitter:creator" content="linkids">
<meta name="twitter:title" content="LINKIDS / CURSOS">
<meta name="twitter:description" content="Cursos de capacitación en idiomas...">
<meta name="twitter:image:src" content="#"> <!-- 660 x 1024 -->
<meta name="twitter:domain" content="https://twitter.com/linkids">

<meta property="og:url" content="https://www.linkids.com.edu/">
<meta property="og:title" content="LINKIDS / CURSOS" > 
<meta property="og:description" content="Cursos de capacitación en idiomas..." /> 
<meta property="og:image" content="#" > <!-- 660 x 1024 -->

<link rel="canonical" href="https://www.linkids.com.edu" />
<!-- SEO -->
