<div class="container text-center">
  <ul class="list-horizontal-unstyled icon-lg">
    <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
    <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
    <li><a href="#"><i class="fab fa-behance"></i></a></li>
    <li><a href="#"><i class="fab fa-instagram"></i></a></li>
  </ul>
  <p class="margin-top-20">&copy; 2018 Flatheme</p>
</div><!-- end container -->
