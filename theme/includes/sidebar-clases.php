<button class="sidebar-nav-toggle">
    <span class="lines"></span>
</button>

<div class="sidebar-nav-content bg-white text-blue">

    <form action="" class="search">
      <div class="field">
        <input type="text" class="input-search" id="input-search-sidebar" name="input-search-sidebar" required>
        <label for="input-search-sidebar">Buscar</label>
      </div> <!-- /field -->
    </form>

    <div class="margin-top-20">
      <ul id="progress-sidebar">
          <li>
            <div class="node green"></div>
            <p class="text-sidebar-bar">
              <span>¡Completada!</span><br>
              <a href="#" title="Clase Linkids">Clase 35: Inglés <img src="assets/images/banderas/United-States-of-America(USA).png" alt="Idiomas"></a>
              <div class="margin-top-10 list-menu-sidebar">
                <ul>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Ciudades</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Números</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Elementos</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Elementos</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Elementos</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-check"></i> Elementos</a></li>
                </ul>
              </div>
            </p>
          </li>

          <li><div class="divider grey"></div></li>

          <li>
            <div class="node green"></div>
            <p class="text-sidebar-bar">
              <span>¡Completada!</span><br>
              <a href="#" title="Clase Linkids">Clase 36: Inglés <img src="assets/images/banderas/United-States-of-America(USA).png" alt="Idiomas"></a>
              <div class="margin-top-10 list-menu-sidebar">
                <ul>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-angle-right"></i> Ciudades</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-angle-right"></i> Números</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-angle-right"></i> Elementos</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-angle-right"></i> Elementos</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-angle-right"></i> Elementos</a></li>
                  <li><a href="#" title="Clase Linkids"><i class="fas fa-angle-right"></i> Elementos</a></li>
                </ul>
              </div>
            </p>
          </li>

          <li><div class="divider grey"></div></li>

          <li>
            <div class="node grey"></div>
            <p class="text-sidebar-bar-inactive">
              <span>Bloqueado</span><br>
              Clase 37 Alemán <img src="assets/images/banderas/Germany.png" alt="Idiomas">
            </p>
          </li>

          <li><div class="divider grey"></div></li>

          <li>
            <div class="node grey"></div>
            <p class="text-sidebar-bar-inactive">
              <span>Bloqueado</span><br>
              Clase 38 Chino <img src="assets/images/banderas/China.png" alt="Idiomas">
            </p>
          </li>

          <li><div class="divider grey"></div></li>

          <li>
            <div class="node grey"></div>
            <p class="text-sidebar-bar-inactive">
              <span>Bloqueado</span><br>
              Clase 39 Chino <img src="assets/images/banderas/China.png" alt="Idiomas">
            </p>
          </li>
      </ul>
    </div>

</div><!-- end sidebar-nav-content -->

