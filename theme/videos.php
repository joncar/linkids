<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu Top Desktop -->
    <header class="d-none d-lg-block">
      <?php include('includes/menu-top.php');?>
      <div class="alert text-white">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <div class="col-md-6 offset-md-3 text-center">
            <div class="text-message-top">Tu progreso de hoy:</div>
            <div class="wizard-progress-top" id="progress-top">
              <div class="step complete"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-black"></div></div>
            </div>
        </div>
      </div>
    </header>
    <!-- Menu Top -->

    <!-- Sidebar Navigation Desktop -->
    <div class="sidebar-nav-left d-none d-lg-block">
      <?php include('includes/sidebar-videos.php');?>
    </div>
    <!-- end Sidebar Navigation -->

    <!-- Menu Top Movil -->
    <header class="d-lg-none">
      <?php include('includes/menu-top-videos-movil.php');?>
      <div class="alert text-white">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <div class="col-md-6 offset-md-3 text-center">
            <div class="text-message-top">Tu progreso de hoy:</div>
            <div class="wizard-progress-top" id="progress-top">
              <div class="step complete"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-black"></div></div>
            </div>
        </div>
      </div>
    </header>
    <!-- Menu Top Movil -->

    <!-- Termina Contenido -->
    <div class="sidebar-wrapper-left">
      <!-- Inicia Contenido -->

      <!-- Full Video -->
      <div class="container-white-video">
        <div class="container no-padding">

          <div class="row">
            <div class="col-12 col-sm-12">
              <div class="videoWrapper">
                  <iframe width="560" height="349" src="https://www.youtube.com/embed/GzEpafzfi00" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          </div><!-- end row -->


          <div class="row text-center margin-bottom-20">
            <div class="col-12 col-sm-12">
              <a data-toggle="modal" data-target="#felicidades">Link para abrir felicitación</a>
            </div>
          </div>

          <div class="row margin-top-10">
            <div class="col-12 col-sm-12 text-center titulo-video">
              <img src="assets/images/banderas/United-States-of-America(USA).png" alt="Idiomas" class="image-title-video-flag-videos"><br>
              <small>Reproduciendo:</small><br>
              <span class="font-weight-bold text-blue">Total Eclipse of the Dog</span>
            </div>
            <div class="col-12 col-sm-12 description-video">
              <p>
                <span class="font-weight-bold">Descripción:</span><br>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed gravida massa mauris, ut elementum mauris rhoncus nec. Aliquam fringilla leo sed enim convallis semper. Integer sed neque rutrum lectus aliquet pretium vel sit amet mi. Integer nulla tellus, 
                facilisis non neque sit amet, gravida dictum neque. Sed et fringilla ex, sit amet consectetur diam. Pellentesque eget tellus ultricies, mattis tellus eget, mollis lectus. Sed aliquet accumsan aliquam. Quisque lectus erat, venenatis faucibus vulputate nec, f
                ringilla ac lectus. Aliquam erat volutpat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nam pharetra ultrices nunc sed faucibus. Donec vel congue neque, quis pulvinar elit. Curabitur tempor nunc eros, sed vestibulum velit pellentesque ut. 
                Proin a dolor quis justo suscipit ullamcorper. Donec egestas iaculis eros a eleifend. Quisque ac nunc tempus, tincidunt risus non, ullamcorper nisl.<br><br>

                Fusce at quam vitae tellus ornare blandit. Praesent at accumsan libero, nec ultrices diam. Sed id blandit augue. Nam sed est eros. Duis posuere magna arcu, sed blandit ligula laoreet id. Sed aliquet ante sed ante auctor cursus. Vestibulum varius dui mollis 
                purus gravida, et iaculis nisl consequat. Maecenas sagittis tortor sed turpis porta condimentum. Maecenas interdum efficitur massa, ac ullamcorper nunc rhoncus eu. Proin nibh ex, luctus nec ex vitae, auctor molestie mi. Suspendisse mollis magna ut sem 
                facilisis, eget tincidunt lorem molestie. 
              </p>
            </div>
          </div><!-- end row -->

        </div>
      </div>
      <!-- Full Video -->
    </div>

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>

    <!-- Modales -->
    <?php include('includes/modales.php');?>

  </body>
</html>
