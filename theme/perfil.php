<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body data-preloader="2">

    <!-- Menu Top Desktop -->
    <header class="d-none d-lg-block">
      <?php include('includes/menu-top.php');?>
      <div class="alert text-white">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <div class="col-md-6 offset-md-3 text-center">
            <div class="text-message-top">Tu progreso de hoy:</div>
            <div class="wizard-progress-top" id="progress-top">
              <div class="step complete"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-black"></div></div>
            </div>
        </div>
      </div>
    </header>
    <!-- Menu Top -->

    <!-- Sidebar Navigation Desktop -->
    <div class="sidebar-nav-left d-none d-lg-block">
      <?php include('includes/sidebar-perfil.php');?>
    </div>
    <!-- end Sidebar Navigation -->

    <!-- Menu Top Movil -->
    <header class="d-lg-none">
      <?php include('includes/menu-top-perfil-movil.php');?>
      <div class="alert text-white">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <div class="col-md-6 offset-md-3 text-center">
            <div class="text-message-top">Tu progreso de hoy:</div>
            <div class="wizard-progress-top" id="progress-top">
              <div class="step complete"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-black"></div></div>
            </div>
        </div>
      </div>
    </header>
    <!-- Menu Top Movil -->

    <div class="sidebar-wrapper-left">
      <!-- Inicia Contenido -->
  		<div class="section container-white-top">
  			<div class="container">
          <div class="col-12 text-center">
            <h3 class="margin-bottom-20">Configuración de la Cuenta</h3>
          </div>

          <div class="col-12 text-center margin-bottom-30">
            <div class="profile profile-pic">
              <div class="image mx-auto d-block">
                <div class="circle-1"></div>
                <div class="circle-2"></div>
                <img src="http://100dayscss.com/codepen/jessica-potter.jpg" width="70" height="70" alt="Perfil de usuario">
                <button type="button" data-toggle="modal" data-target="#edit-profile"><div class="edit"><a href="#"><i class="fas fa-user-edit"></i></a></div></button>
              </div>
            </div>
          </div>

  				<div class="col-12 text-center contact-form">
  					<form method="post" id="contactform">
              <label id="label-input">Correo</label>
  						<input type="email" id="email" name="email" placeholder="Correo" required>
              <label id="label-input">Usuario</label>
              <input type="text" id="usuario" name="usuario" placeholder="Usuario" required>
              <label id="label-input">Password</label>
              <input type="password" id="contrasena" name="contrasena" placeholder="Password" required>
              <label id="label-input">Clave Escolar</label>
              <input type="password" id="clave" name="clave" placeholder="Clave Escolar" required>
              <label id="label-input">Escuela</label>
              <input type="text" id="escuela" name="escuela" placeholder="Escuela" required>

              <div class="row margin-top-30">
  						  <div class="col-12 col-sm-6"><button class="button button-sm button-blue-transparent" type="submit">Cancelar</button></div>
                <div class="col-12 col-sm-6"><button class="button button-sm button-blue" type="submit">Guardar</button></div>
              </div>
  					</form>
  					<!-- Submit result -->
  					<div class="submit-result">
  						<span id="success">Thank you! Your Message has been sent.</span>
  						<span id="error">Something went wrong, Please try again!</span>
  					</div>
  				</div><!-- end contact-form -->

  			</div><!-- end container -->
  		</div>
      <!-- Termina Contenido -->

    </div><!-- end sidebar-wrapper-left -->

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
    <!-- Modales -->
    <?php include('includes/modales.php');?>

  </body>
</html>

