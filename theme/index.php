<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu Top -->
    <header>
      <?php include('includes/menu-home.php');?>
    </header>
    <!-- Menu Top -->

    <!-- Scroll to top button -->
		<div class="scrolltotop">
			<a class="button-circle button-circle-sm button-circle-dark" href="#"><i class="ti-arrow-up"></i></a>
		</div>
		<!-- end Scroll to top button -->

    <!-- Home section -->
		<div class="section-xl bg-image parallax bg-home-header bg-animate" style="background-image: url(SVG/Home/header.svg)" id="inicio">
			<div class="container">
				<div class="row margin-top-50 margin-bottom-50 align-items-center">
					<div class="col-12 col-sm-6 titulo-header-home">
						<h1 class="font-weight-bold no-margin text-uppercase text-yellow">
              <span class="text-white">Bienvenidos</span><br>Alumnos
            </h1>
            <p class="text-white margin-bottom-20">Al programa de aprendizaje más efectivo en desarrollo cognitivo. Aprendizaje en 5 idiomas simultáneos.</p>
            <a class="button button-md button-green-home" href="login.php" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Iniciar sesión</a>
					</div>
          <div class="col-12 col-sm-6 img-header-home">
						<img src="svg/home/home-kid.png" alt="Linkids" class="mx-auto d-block">
					</div>
				</div><!-- end row -->
			</div><!-- end container -->
		</div>
		<!-- end Home section -->

    <!-- Services section -->
    <div class="section bg-orange" id="nosotros">
      <div class="container">

        <div class="row container-white-2">
          <div class="col-12 col-md-12 bg-white-home">
            <div class="row align-items-center margin-bottom-30">
              <div class="col-12 col-sm-6 img-kid2">
                <img src="svg/home/home-kid2.png" alt="Linkids" class="mx-auto d-block">
              </div>
              <div class="col-12 col-sm-6">
                <h2 class="margin-bottom-20 subtitulo-home font-weight-bold"><span>¿Qué es</span><br>Linkids?</h2>
                <p class="text-home">
                  Especialistas recomiendan música durante el embarazo y la primera infancia, la música son sonidos y los idiomas también, así que si los presentamos de manera atractiva, divertida y ordenada, hemos logrado un estímulo completo para un mejor desarrollo 
                  cognitivo para tu hijo! <span class="font-weight-bold">!Esto es linkids!</span><br><br>

                  El riesgo que un cerebro presente problemas de aprendizaje es demasiado alto como para no atenderlo a tiempo.<br>
                  <span class="font-weight-bold">LINKIDS</span> es la solución a los problemas de aprendizaje que se puedan presentar durante toda la vida.<br><br>

                  <span class="font-weight-bold">¿Por qué?</span><br>
                  Simplemente porque reúne los estímulos necesarios para un mejor desarrollo cognitivo a través de la música y los idiomas, potencializando de ésta forma, la atención, la inteligencia, la percepción, el lenguaje y la memoria, 
                  logrando un aprendizaje ágil y para siempre.<br><br>

                  Gracias al descubrimiento de las Neurociencias podremos detener el problema que hoy mucha gente presenta: <span class="font-weight-bold">APRENDER UN SEGUNDO IDIOMA</span> e incluso la lógica matemática entre otros, logrando así, un óptimo desarrollo cognitivo.
                </p>
              </div>
            </div> <!-- row -->

            <div class="row align-items-center bg-image bg-home-header text-center idiomas" style="background-image: url(SVG/Home/clases.svg)">
              <div class="col-12 col-sm-6 idioma-borde link-idiomas" style="margin-bottom: 0px;">
                <a href="#">
                  <img src="svg/home/idiomas.svg" alt="Linkids" class="mx-auto d-block">
                  <h3 class="text-white font-weight-bold margin-bottom-20">Idiomas<br>disponibles</h3>
                  <a class="button button-md button-blue-home" href="login.php" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>
                </a>
              </div>
              <div class="col-12 col-sm-6 idioma-borde link-idiomas2" style="margin-bottom: 0px;">
                <a href="#">
                  <img src="svg/home/idiomas2.svg" alt="Linkids" class="mx-auto d-block">
                  <h3 class="text-white font-weight-bold margin-bottom-20">Clases<br>diarias</h3>
                  <a class="button button-md button-blue-home" href="login.php" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>
                </a>
              </div>
            </div> <!-- row -->
          </div>
        </div>

        <div class="row align-items-center margin-top-30">
          <div class="col-12 col-sm-12">
            <h2 class="margin-bottom-20 subtitulo-home font-weight-bold text-white"><span class="text-yellow">¿Porqué es importante alentar</span><br>el desarrollo en la primera infancia?</h2>
            <p class="text-white">
              El cerebro humano tiene unos límites temporales para el desarrollo de ciertas habilidades. Son períodos críticos, esto se presenta en los primeros momentos en formación del cerebro, durante los primeros años de vida, la primera infancia.<br><br>

              Si no logramos aprovechar estas capacidades en el momento exacto, la oportunidad se perdería para siempre, lo cuál sería muy complicado lograrlo a lo largo de la vida.<br>
              <span class="font-weight-bold">CONCLUSIÓN:</span> Nunca perdemos la plasticidad cerebral, simplemente a medida que vamos creciendo, se va perdiendo esta gran oportunidad y de ésta manera lograremos que nuestros hijos 
              creen nuevas sinapsis y que aumenten el desarrollo madurativo de sus neuronas en el momento apropiado. Eso les ayudará a potencializar las funciones cognitivas.
            </p>
          </div>
        </div> <!-- row -->

      </div><!-- end container -->
    </div>
    <!-- end Services section -->

    <!-- Services section -->
    <div class="section bg-gray-home" id="nosotros">
      <div class="container">

        <div class="row">
            <div class="col-12 col-sm-7" style="margin-bottom:0px;">
              <h2 class="margin-bottom-20 subtitulo-home font-weight-bold text-blue"><span class="text-yellow">¿Cómo lo</span><br>Logramos?</h2>
            </div>
        </div> <!-- row -->

        <div class="row align-items-center margin-top-30 container-logro">
            <div class="col-12 col-sm-7" style="margin-bottom:0px;">
              <p class="text-home margin-bottom-20">
                Como todo en la vida, se logra con la constancia, con solo seguir el orden de las clases y mantener mínimo 2 a 3 sesiones diarias lograremos un avance significativo, la clase de la mañana y de la noche deben de ser infaltables en este proceso, y 
                mantener una distancia de 2 horas entre clase y clase, con esta dedicación estaremos asegurando un mejor futuro para nuestros pequeños, los avances se verían posterior al primer año de entrenamiento, es un proceso lento pero muy firme.<br><br>

                El programa de linkids está perfectamente diseñado, muy bien pensado porque aprovecha los últimos hallazgos científicos sobre la formación del cerebro, <span class="font-weight-bold">NEUROCIENCIAS!</span><br>
                Desde el vientre materno y hasta los primeros años se forma el 50% de las conexiones neuronales, a partir de los 3 años comienza a disminuir este fenómeno natural hasta terminar la primera infancia. Casi se detiene este maravilloso proceso 
                conocido como <span class="font-weight-bold">“PLASTICIDAD NEURONAL”</span>, es decir, el cerebro de un niño durante la primera infancia desarrollará las ramificaciones neuronales que utilizará toda su vida, consideramos un gran acierto acercarles a 
                los papás, un programa de Estimulación temprana en idiomas y música totalmente completo logrando así niños más inteligentes, atentos, más perceptivos, mejor memoria, mejorando notablemente el  área de lenguaje y con ello el niño logrará un mejor 
                funcionamiento de su cerebro, como un aprendizaje ágil, práctico y duradero.
              </p>
              <a class="button button-md button-green-home" href="login.php" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>
            </div>
            <div class="col-12 col-sm-5">
              <img src="svg/home/home-kid4.png" alt="Linkids" class="mx-auto d-block">
            </div>
        </div> <!-- row -->

      </div><!-- end container -->
    </div>
    <!-- end Services section -->

    <!-- Services section -->
    <div class="section bg-image bg-home-header parallax" id="nosotros"  style="background-image: url(SVG/Home/header.svg)">
      <div class="container">

        <div class="row">
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="container-ingredientes-principal text-white">
                <p>
                  <img src="svg/home/ingrediente.svg" alt="Linkids" class="mx-auto d-block margin-bottom-20">
                  Contamos con 3 ingredientes fundamentales:
                </p>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="container-ingredientes text-white">
                <p>
                  <img src="svg/home/ingrediente03.svg" alt="Linkids" class="mx-auto d-block">
                  <b>Música Neurolingüística</b><br>
                  Conocida también como el efecto Mozart que es el descubrimiento de los múltiples beneficios que se producen en la formación del cerebro por sus amplias melodías, rangos sonoros, etc.
                </p>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="container-ingredientes text-white">
                <p>
                  <img src="svg/home/ingrediente02.svg" alt="Linkids" class="mx-auto d-block">
                  <b>Idiomas</b><br>
                  Está comprobado que cada idioma requiere crear su propia red neuronal independiente, no son las mismas redes neuronales que ocupas para hablar ingles que español , definitivamente son otras
                  y estas se generan en la primera infancia con mayor facilidad
                </p>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="container-ingredientes text-white">
                <p>
                  <img src="svg/home/ingrediente01.svg" alt="Linkids" class="mx-auto d-block">
                  <b>Bit de Inteligencia (GLENN DOMAN)</b><br>
                  IMAGEN, FONDO, VOZ Y PALABRA
                </p>
              </div>
            </div>
        </div> <!-- row -->

      </div><!-- end container -->
    </div>
    <!-- end Services section -->

    <!-- Play video section -->
    <div class="section-xl bg-image bg-home-header" style="background-image: url(SVG/Home/video.png)" id="inicio">
      <div class="bg-black-06">
        <div class="container text-center">
          <div class="row">
            <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
              <h1 class="font-weight-thin text-white">Linkids<br><span class="font-weight-bold">Evolución educativa</span></h1>
              <a class="button-circle button-circle-xl button-circle-outline-white-2 margin-top-20 popup-youtube" data-youtube-src="https://www.youtube.com/embed/1aux_hLPf4c?autoplay=1&rel=0" href="#"><i class="fa fa-play"></i></a>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div>
    </div>
    <!-- end Play video section -->

    <!-- Prices section -->
    <div class="section bg-image bg-home-header"  style="background-image: url(SVG/Home/precios.svg)">
      <div class="container">
        <div class="text-center container-precios margin-bottom-50">
          <h2 class="subtitulo-home font-weight-bold text-white"><span>Programa linkids</span><br>Nuestros precios</h2>
          <h3 class="text-white font-weight-bold">Prueba gratis de 15 días</h3>
          <a class="button button-md button-blue-home" href="login.php" title="Iniciar Sesión en Linkids"><i class="fas fa-hand-point-up icon-btn-home"></i>Quiero iniciar a probar Linkids</a>
        </div>
        <div class="row">
          <!-- Prices box 1 -->
          <div class="col-12 col-md-4">
            <div class="prices-box">
              <img src="svg/home/ingrediente.svg" alt="Linkids" class="mx-auto d-block margin-bottom-20">
              <h3 class="text-white font-weight-bold">Plan<br>Individual</h3>
              <div class="price margin-bottom-20">
                <h1 class="text-white">490<span>/anual</span></h1>
              </div>
              <div class="price">
                <h1 class="text-white">290<span>/mensual</span></h1>
              </div>
              <div class="price-features">
                <ul class="text-white">
                  <li>-Aprovecha una prueba gratuita de 7 días</li>
                  <li>-Disfruta de 8 clases diarias</li>
                  <li>-Método garantizado de aprendizaje</li>
                  <li>-Ideal para todas las edades</li>
                </ul>
              </div>
              <a class="button button-md button-blue-home" href="login.php" title="Iniciar Sesión en Linkids"><i class="fas fa-hand-point-up icon-btn-home"></i>¡Lo quiero!</a>
            </div>
          </div>
          <!-- Prices box 2 -->
          <div class="col-12 col-md-4">
            <div class="prices-box">
              <img src="svg/home/ingrediente.svg" alt="Linkids" class="mx-auto d-block margin-bottom-20">
              <h3 class="text-white font-weight-bold">Plan<br>Colegio</h3>
              <div class="price margin-bottom-20">
                <h1 class="text-white">245<span>/anual</span></h1>
              </div>
              <div class="price">
                <h1 class="text-white">145<span>/mensual</span></h1>
              </div>
              <div class="price-features">
                <ul class="text-white">
                  <li>-Aprovecha una prueba gratuita de 7 días</li>
                  <li>-Disfruta de 8 clases diarias</li>
                  <li>-Precios por alumno</li>
                  <li>-Escuelas con más de 500 alumnos, pregunta por nuestros beneficios</li>
                </ul>
              </div>
              <a class="button button-md button-green-home" href="login.php" title="Iniciar Sesión en Linkids"><i class="fas fa-hand-point-up icon-btn-home"></i>¡Me interesa!</a>
            </div>
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Prices section -->

    <!-- Contact section -->
    <div class="section bg-image bg-home-header" id="nosotros"  style="background-image: url(SVG/Home/contacto.svg)">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-3 text-white">
            <div class="margin-bottom-30">
              <h6 class="heading-uppercase no-margin" style="color: #FFE26B;font-weight: 900;">Correo</h6>
              <p>contacto@linkids.edu</p>
            </div>
          </div>
          <div class="col-12 col-md-9">
            <div class="contact-form">
              <form method="post" id="contactform">
                <div class="form-row margin-bottom-30">
                  <div class="col-12 col-sm-6">
                    <input type="text" id="name" name="name" placeholder="Tu nombre" required>
                  </div>
                  <div class="col-12 col-sm-6">
                    <input type="email" id="email" name="email" placeholder="Tu correo" required>
                  </div>
                </div>
                <textarea name="message" id="message" placeholder="Mensaje para Linkids"></textarea>
                <a class="button button-md button-green-home margin-top-30 text-white" type="submit" title="Enviar mi comentario a Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Enviar</a>
              </form>
              <!-- Submit result -->
              <div class="submit-result">
                <span id="success">Thank you! Your Message has been sent.</span>
                <span id="error">Something went wrong, Please try again!</span>
              </div>
            </div>
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Contact section -->

    <footer id="form-home">
      <div class="footer bg-blue">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-6 text-center text-md-left">
              <p class="margin-top-10 text-white">&copy; 2019 Linkids. Todos los Derechos Reservados</p>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right">
              <ul class="list-horizontal-unstyled">
                <li style="font-size: 24px;"><a href="#" class="text-white"><i class="fab fa-facebook-f"></i></a></li>
                <li style="font-size: 24px;"><a href="#" class="text-white"><i class="fab fa-instagram"></i></a></li>
              </ul>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div>
    </footer>

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
  </body>
</html>
