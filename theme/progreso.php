<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body data-preloader="2">

    <!-- Menu Top Desktop -->
    <header class="d-none d-lg-block">
      <?php include('includes/menu-top.php');?>
    </header>
    <!-- Menu Top -->

    <!-- Sidebar Navigation Desktop -->
    <div class="sidebar-nav-left d-none d-lg-block">
      <?php include('includes/sidebar-perfil.php');?>
    </div>
    <!-- end Sidebar Navigation -->

    <!-- Menu Top Movil -->
    <header class="d-lg-none">
      <?php include('includes/menu-top-perfil-movil.php');?>
    </header>
    <!-- Menu Top Movil -->

    <div class="sidebar-wrapper-left">
      <!-- Inicia Contenido -->
  		<div class="section container-white-inside">
  			<div class="container">
          <div class="col-12 text-center">
            <h3 class="margin-bottom-20">Continúa con tus lecciones</h3>
          </div>

  				<div class="row text-center contact-form">

              <div class="col-12 col-sm-4">
                <div class="image-lesion">
                  <img src="https://picsum.photos/35/35/?image=75"/ class="rounded-circle box-shadow borde-img-active" alt="Lesiones">
                </div>
                <a href="videos-aleman.php">
                  <div id="gallery" class="text-white active">
                    <div id="images">
                      <div class="figure"><img class="image" src="assets/images/videos/video-01.jpg"/>
                        <div class="caption">
                          <div class="body">
                            <i class="fas fa-check-circle icon-check"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="title-lessons">
                  <a href="videos-aleman.php">Medios de Transporte <img src="assets/images/banderas/Spain.png" alt="Idiomas"></a>
                </div>
              </div>

              <div class="col-12 col-sm-4">
                <div class="image-lesion">
                  <img src="https://picsum.photos/35/35/?image=75"/ class="rounded-circle box-shadow borde-img-active" alt="Lesiones">
                </div>
                <a href="videos-chino.php">
                  <div id="gallery" class="text-white active">
                    <div id="images">
                      <div class="figure"><img class="image" src="assets/images/videos/video-02.jpg"/>
                        <div class="caption">
                          <div class="body">
                            <i class="fas fa-check-circle icon-check"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="title-lessons">
                  <a href="videos-chino.php">Medios de Transporte <img src="assets/images/banderas/Spain.png" alt="Idiomas"></a>
                </div>
              </div>

              <div class="col-12 col-sm-4">
                <div class="image-lesion">
                  <img src="https://picsum.photos/35/35/?image=75"/ class="rounded-circle box-shadow borde-img-lock" alt="Lesiones">
                </div>
                <a href="videos-frances.php" title="Lessión Abierta">
                  <div id="gallery" class="text-white lock">
                    <div id="images">
                      <div class="figure"><img class="image" src="assets/images/videos/video-03.jpg"/>
                        <div class="caption">
                          <div class="body">
                            <div class="title text-white">Lesión 01</div>
                            <p class="text">A little bit of interesting information about this fantastic image</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="title-lessons">
                    <a href="videos-frances.php">Medios de Transporte <img src="assets/images/banderas/Spain.png" alt="Idiomas"></a>
                  </div>
                </a>
              </div>

  				</div>

  			</div><!-- end container -->
  		</div>

      <div class="section container-white-inside">
        <div class="container">

          <div class="col-12 text-center">
            <h3 class="margin-bottom-20">Tu racha</h3>
          </div>

          <!-- Dias de la semana Desktop -->
          <div class="col-12 text-center d-none d-lg-block">
              <div class="wizard-progress">
                <div class="step complete text-uppercase font-weight-bold text-blue">
                  Lunes <div class="node" id="check-gray"></div>
                  <button data-toggle="tooltip" data-placement="top" title="En Progreso">
                    <div class="image-progress"><img src="assets/images/progreso/icon-azul.jpg" class="rounded-circle box-shadow image-progress-blue"></div>
                  </button>
                </div>
                <div class="step complete text-uppercase font-weight-bold text-blue">
                  Martes <div class="node" id="check-green"></div>
                  <button data-toggle="tooltip" data-placement="top" title="En Progreso">
                    <div class="image-progress"><img src="assets/images/progreso/icon-azul.jpg" class="rounded-circle box-shadow image-progress-blue"></div>
                  </button>
                </div>
                <div class="step complete text-uppercase font-weight-bold text-blue">
                  Miércoles <div class="node" id="check-green"></div>
                  <button data-toggle="tooltip" data-placement="top" title="En Progreso">
                    <div class="image-progress"><img src="assets/images/progreso/icon-01.png" class="rounded-circle box-shadow image-progress-blue"></div>
                  </button>
                </div>
                <div class="step complete text-uppercase font-weight-bold text-blue">
                  Jueves <div class="node" id="check-green"></div>
                  <div class="image-progress"><img src="assets/images/progreso/icon-01.png" class="rounded-circle box-shadow image-progress-blue"></div>
                </div>
                <div class="step in-progress text-uppercase font-weight-bold text-pink">
                  Viernes <div class="node"></div>
                  <div class="image-progress"><img src="assets/images/progreso/icon-02.png" class="rounded-circle box-shadow image-progress-pink"></div>
                </div>
                <div class="step text-uppercase font-weight-bold text-pink">
                  Sábado <div class="node"></div>
                  <div class="image-progress"><img src="assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-pink"></div>
                </div>
                <div class="step text-uppercase font-weight-bold text-yellow">
                  Domingo <div class="node"></div>
                  <div class="image-progress"><img src="assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"></div>
                </div>
                <!-- Dia de la semana 
                <div class="step text-uppercase font-weight-bold text-blue">
                  Lunes <div class="node"></div>
                  <div class="link">
                      <a href="#"><div class="image-progress"><img src="https://picsum.photos/g/150/150" class="rounded-circle box-shadow image-progress-blue"></div></a>
                      <span>Completo</span>
                  </div>
                </div>-->
              </div>
          </div>
          <!-- Dias de la semana Desktop -->

          <!-- Dias de la semana Movil -->
          <div class="col-12 text-center d-lg-none">

            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Día se la semana</th>
                  <th scope="col">Observaciones</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><div class="image-progress"><img src="assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-blue">Lunes</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-gray"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-blue">Martes</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-green"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-blue">Miércoles</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-green"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-blue">Jueves</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-green"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-pink">Viernes</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-gray"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-pink">Sábado</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-gray"></i><br>Observaciones</div></td>
                </tr>
                <tr>
                  <td><div class="image-progress"><img src="assets/images/progreso/icon-blanco.jpg" class="rounded-circle box-shadow image-progress-yellow"><br><span class="font-weight-bold text-yellow">Domingo</span></div></td>
                  <td><div class="margin-top-20"><i class="fas fa-check" id="check-gray"></i><br>Observaciones</div></td>
                </tr>
              </tbody>
            </table>

          </div>
          <!-- Dias de la semana Movil -->

          <div class="row col-12 col-md-8 offset-md-2 text-center margin-top-10">
              <div class="col-6 col-sm-4 number-profile">
                <h2 class="font-weight-bold">46</h2><small class="text-blue text-uppercase">Mejor Racha</small>
              </div>

              <div class="col-6 col-sm-4 number-profile">
                <h2 class="font-weight-bold">46</h2><small class="text-blue text-uppercase">Racha Actual</small>
              </div>

              <div class="col-12 col-sm-4">
                <button class="button button-sm button-blue margin-top-10" type="submit">¡Continuar!</button>
              </div>
          </div>
    
        </div><!-- end container -->
      </div>

      <!-- Termina Contenido -->

    </div><!-- end sidebar-wrapper-left -->

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>

  </body>
</html>
