<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2" style="overflow: hidden;" id="body-login">

    <!-- Sidebar Navigation Desktop -->
    <div class="sidebar-nav-left d-none d-lg-block">
      <button class="sidebar-nav-toggle" style="z-index: 999999999;"><span class="lines"></span></button>
      <div class="sidebar-nav-content" id="sidebar-nav-login">

        <div class="col-12 col-sm-12 margin-bottom-30">
          <a href="index.php"><img src="assets/images/logo-linkids.png" alt="Logo Linkids" srcset="svg/logo-login.svg"></a>
        </div>

        <div class="col-12 text-center contact-form">
          <form method="post" id="contactform">
            <label id="label-input">Correo</label>
            <input type="email" id="email" name="email" placeholder="Correo" required>
            <label id="label-input">Contraseña</label>
            <input type="password" id="contrasena" name="contrasena" placeholder="xxxxx" required>

            <div class="olvide-login">
              <a data-toggle="modal" data-target="#olvide-contrasena">Olvidé mi contraseña</a>
            </div>

            <div class="row margin-top-30">
              <div class="col-12 col-sm-6"><a href="registro.php"><button class="button button-sm button-blue-transparent" type="submit">Registrarme</button></a></div>
              <div class="col-12 col-sm-6"><a href="clases.php"><button class="button button-sm button-blue">Ingresar</button></a></div>
            </div>
          </form>
          <!-- Submit result -->
          <div class="submit-result">
            <span id="success">Thank you! Your Message has been sent.</span>
            <span id="error">Something went wrong, Please try again!</span>
          </div>
        </div><!-- end contact-form -->

      </div><!-- end sidebar-nav-content -->
    </div>
    <!-- end Sidebar Navigation -->

    <!-- Sidebar Navigation Movil -->
    <div class="d-lg-none" id="sidebar-nav-login-movil">
      <div class="col-12 col-sm-12 margin-bottom-30">
        <a href="index.php"><img src="assets/images/logo-linkids.png" alt="Logo Linkids" srcset="svg/logo-login.svg" class="logo-movil mx-auto d-block"></a>
      </div>

      <div class="col-12 text-center">
          <form method="post" id="contactform">
            <label id="label-input">Correo</label>
            <input type="email" id="email" name="email" placeholder="Correo" required>
            <label id="label-input">Password</label>
            <input type="password" id="contrasena" name="contrasena" placeholder="Password" required>

            <div class="olvide-login">
              <a href="#">Olvidé mi contraseña</a>
            </div>

            <div class="row margin-top-30">
              <div class="col-12 col-sm-6"><button class="button button-sm button-blue-transparent" type="submit">Registrarme</button></div>
              <div class="col-12 col-sm-6"><button class="button button-sm button-blue" type="submit">Ingresar</button></div>
            </div>
          </form>
          <!-- Submit result -->
          <div class="submit-result">
            <span id="success">Thank you! Your Message has been sent.</span>
            <span id="error">Something went wrong, Please try again!</span>
          </div>
        </div><!-- end contact-form -->
    </div>

    <!-- SVG Desktop Login -->
    <div class="sidebar-wrapper-left container-profile d-none d-lg-block">
  		<div class="section container-white-none animacion-login">
        <span class="cloud cloud--small"></span>
        <span class="cloud cloud--medium"></span>
        <span class="cloud cloud--large"></span>
        <img src="svg/hombre.svg" id="hombre">
        <img src="svg/montana.svg" id="montana">


        <div class="sun" id="sol">
            <div class="ray_box">
                <div class="ray ray1"></div>
                <div class="ray ray2"></div>
                <div class="ray ray3"></div>
                <div class="ray ray4"></div>
                <div class="ray ray5"></div>
                <div class="ray ray6"></div>
                <div class="ray ray7"></div>
                <div class="ray ray8"></div>
                <div class="ray ray9"></div>
                <div class="ray ray10"></div>
            </div>
        </div>

      </div>
    </div>

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
    <!-- Modales -->
    <?php include('includes/modales.php');?>

  </body>
</html>

