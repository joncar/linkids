<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2" style="overflow: hidden;">

    <div class="section-fullscreen bg-image parallax bg-home-header bg-animate" style="background-image: url(SVG/Home/header.svg)" id="inicio">
        <div class="row align-items-center position-middle text-center">
          <div class="col-12 col-sm-12 titulo-header-home text-center">
            <div class="margin-bottom-50"><img src="http://bluepixel.mx/linkids/theme/svg/logo-login.svg" alt="Logo Linkids" style="width: 20%;"></div>
            <h1 class="font-weight-bold no-margin text-uppercase text-yellow" style="line-height: 0.2;">
              ¡Gracias!<br><br><span class="text-white">Por seleccionar tu membresía.<br> Te estamos redireccionando a Paypal, espera un momento</span>
            </h1>
          </div>
        </div><!-- end row -->
    </div>

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
    <!-- Modales -->
    <?php include('includes/modales.php');?>

  </body>
</html>

