<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu Top Desktop -->
    <header class="d-none d-lg-block">
      <?php include('includes/menu-top.php');?>
      <div class="alert text-white">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <div class="col-md-6 offset-md-3 text-center">
            <div class="text-message-top">Tu progreso de hoy:</div>
            <div class="wizard-progress-top" id="progress-top">
              <div class="step complete"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-black"></div></div>
            </div>
        </div>
      </div>
    </header>
    <!-- Menu Top -->

    <!-- Sidebar Navigation Desktop -->
    <div class="sidebar-nav-left d-none d-lg-block">
      <?php include('includes/sidebar-clases.php');?>
    </div>
    <!-- end Sidebar Navigation -->

    <!-- Menu Top Movil -->
    <header class="d-lg-none">
      <?php include('includes/menu-top-clases-movil.php');?>
      <div class="alert text-white">
        <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
        <div class="col-md-6 offset-md-3 text-center">
            <div class="text-message-top">Tu progreso de hoy:</div>
            <div class="wizard-progress-top" id="progress-top">
              <div class="step complete"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-green"></div></div>
              <div class="step in-progress"><div class="node"><img src="https://picsum.photos/300/300" class="rounded-circle box-shadow borde-img-top-black"></div></div>
            </div>
        </div>
      </div>
    </header>
    <!-- Menu Top Movil -->

    <!-- Inicia Contenido -->
    <div class="sidebar-wrapper-left">

      <!-- Full Video -->
      <div class="container-white-video">
        <div class="container no-padding">
          <div class="row">
            <div class="col-12 col-sm-12">
              <div class="videoWrapper">
                  <iframe width="560" height="349" src="https://www.youtube.com/embed/GzEpafzfi00" frameborder="0" allowfullscreen></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Full Video -->

      <div class="section container-white-none">
        <div class="container">

          <div class="row">
            <div class="col-12 col-sm-12">
              <ul class="accordion accordion-oneopen">
                <!-- 1 -->
                <li class="active">
                  <div class="accordion-title">
                    <h3 class="text-blue text-responsive">
                      <img src="https://picsum.photos/300/250/?image=75" alt="Idiomas" class="image-title-video">
                      Animales en Español 
                      <img src="assets/images/banderas/Spain.png" alt="Idiomas" class="image-title-video-flag">
                    </h3>
                  </div>

                  <div class="accordion-content">


                    <div class="row text-center">
                      <div class="col-12 text-center">
                        <h5>Lecciones para hoy</h5>
                      </div>

                      <div class="col-12 col-sm-6 col-lg-4">
                        <div class="image-lesion">
                          <img src="https://picsum.photos/35/35/?image=75"/ class="rounded-circle box-shadow borde-img-lock" alt="Lesiones">
                        </div>
                        <a href="videos-aleman.php" title="Linkids Clases">
                          <div id="gallery" class="text-white active">
                            <div id="images">
                              <div class="figure">
                                <img class="image" src="assets/images/videos/video-01.jpg" alt="Videos Linkids">
                                <div class="caption">
                                    <div class="body">
                                      <div class="title text-white">Lesión 01</div>
                                      <p class="text">A little bit of interesting information about this fantastic image</p>
                                    </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        <div class="title-lessons">
                          <a href="videos-ingles.php" title="Linkids Clases">Medios de Transporte <img src="assets/images/banderas/Spain.png" alt="Idiomas"></a>
                        </div>
                      </div>

                      <div class="col-12 col-sm-6 col-lg-4">
                        <div class="image-lesion">
                          <img src="https://picsum.photos/35/35/?image=75"/ class="rounded-circle box-shadow borde-img-lock" alt="Lesiones">
                        </div>
                        <a href="videos-frances.php" title="Linkids Clases">
                          <div id="gallery" class="text-white active">
                            <div id="images">
                              <div class="figure">
                                <img class="image" src="assets/images/videos/video-02.jpg"/ alt="Videos Linkids">
                                <div class="caption">
                                    <div class="body">
                                      <div class="title text-white"><i class="fas fa-play-circle"></i><br>Reproduciendo</div>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </a>
                        <div class="title-lessons">
                          <a href="videos-ingles.php" title="Linkids Clases">Medios de Transporte <img src="assets/images/banderas/Spain.png" alt="Idiomas"></a>
                        </div>
                      </div>

                      <div class="col-12 col-sm-6 col-lg-4">
                        <div class="image-lesion">
                          <img src="https://picsum.photos/35/35/?image=75"/ class="rounded-circle box-shadow borde-img-lock" alt="Lesiones">
                        </div>
                        <a href="videos-chino.php" title="Linkids Clases">
                          <div id="gallery" class="text-white lock">
                            <div id="images">
                              <div class="figure">
                                <img class="image" src="assets/images/videos/video-03.jpg"/ alt="Videos Linkids">
                                <div class="caption">
                                  <div class="body">
                                    <div class="title text-white">Lesión 03</div>
                                    <p class="text">A little bit of interesting information about this fantastic image</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="title-lessons">
                            <a href="videos-ingles.php" title="Linkids Clases">Medios de Transporte <img src="assets/images/banderas/Spain.png" alt="Idiomas"></a>
                          </div>
                        </a>
                      </div>
                  </div>


                  </div>
                </li>


              </ul>
            </div>
          </div><!-- end row -->


        </div><!-- end container -->
      </div>

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>

  </body>
</html>
