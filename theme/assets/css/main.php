<!-- Menu Top -->
    <header>
      <?php $this->load->view('es/includes/menu-home',array(),FALSE,'paginas');?>
      <link href="https://fonts.googleapis.com/css?family=Luckiest+Guy&display=swap" rel="stylesheet">
    </header>
    <!-- Menu Top -->

    <!-- Scroll to top button -->
    <div class="scrolltotop">
      <a class="button-circle button-circle-sm button-circle-dark" href="#">
        <i class="ti-arrow-up"></i>
      </a>
    </div>
    <!-- end Scroll to top button -->

    <!-- Home section -->
    <div class="section-xl bg-image parallax bg-home-header bg-animate" style="background-image: url(<?= base_url() ?>theme/svg/home/header.svg)" id="inicio">
      <div class="container">
        <div class="row margin-top-50 margin-bottom-50 align-items-center">
          <div class="col-12 col-sm-6 titulo-header-home">
          	<div class="margin-bottom-20">
	            <h1 class="font-weight-bold no-margin text-uppercase text-yellow margin-bottom-10 font-cursiva">
	              <span class="text-white"></span><br>Bienvenidos
	            </h1>
	            <h4 class="font-weight-bold text-white">A la mejor experiencia de aprendizaje en 5 idiomas simultáneos para tus hijos</h4>
        	</div>
            <a class="button button-md button-green-home" href="<?= base_url('login.html') ?>" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Iniciar sesión</a>
          </div>
          <div class="col-12 col-sm-6 img-header-home">
            <img src="<?= base_url() ?>theme/svg/home/home-kid.png" alt="Linkids" class="mx-auto d-block">
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Home section -->

    <!-- Services section -->
    <div class="section bg-orange" id="nosotros">
      <div class="container">

        <div class="row container-white-2">
          <div class="col-12 col-md-12 bg-white-home">
            <div class="row align-items-center margin-bottom-30">
              <div class="col-12 col-sm-6 img-kid2">
                <img src="<?= base_url() ?>theme/svg/home/home-kid2.png" alt="Linkids" class="mx-auto d-block">
              </div>
              <div class="col-12 col-sm-6">
                <h2 class="margin-bottom-20 subtitulo-home font-weight-bold"><span>¿Por qué</span><br><div class="font-cursiva">Linkids?</div></h2>
                <p class="text-home">
                  Los especialistas en Neurodesarrollo, Estimulación Temprana, Psicología Infantil, en Pediatría, Obstetras, recomiendan música durante el embarazo y la primera infancia. Los idiomas, las imágenes y la música, son 
                  los elementos que apoyan esta estimulación neurológica en las áreas del lenguaje, la atención, la percepción, la memoria, el análisis de la información, entre muchas otras. Al presentarlos de manera atractiva, 
                  divertida y ordenada, estos logran un estímulo completo, lo que da como resultado un mejor desarrollo del conocimiento de tu hijo.<br><br>

                  <div style="background-color: #004CB1; padding:15px; border-radius: 0px 10px 0px 0px;" class="text-white font-weight-bold margin-bottom-10">
                  <span class="font-weight-bold">LINKIDS</span> es una herramienta que puede prevenir la dislexia, problemas de lectura o escritura así como de matemáticas. Todo esto se logra estimulando la actividad cerebral, lo que desarrolla una mayor 
                  cantidad de conexiones neuronales que le permitirán desarrollar un aprendizaje ágil y práctico, brindándole una gran ventaja a lo largo de su vida.
                  </div>
                </p>
              </div>
            </div> <!-- row -->

            <div class="row align-items-center bg-image bg-home-header text-center idiomas" style="background-image: url(<?= base_url() ?>theme/svg/home/clases.svg)">
              <div class="col-12 col-sm-6 idioma-borde link-idiomas" style="margin-bottom: 0px;">
                <a href="#">
                  <!--<img src="<?= base_url() ?>theme/svg/home/idiomas.svg" alt="Linkids" class="mx-auto d-block">-->
                  <h3 class="text-white font-weight-bold margin-bottom-20">Música y<br>matemáticas</h3>
                  <a class="button button-md button-blue-home" href="<?= base_url('login.html') ?>" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>
                </a>
              </div>
              <div class="col-12 col-sm-6 idioma-borde link-idiomas2" style="margin-bottom: 0px;">
                <a href="#">
                  <!--<img src="<?= base_url() ?>theme/svg/home/idiomas6.svg" alt="Linkids" class="mx-auto d-block">-->
                  <h3 class="text-white font-weight-bold margin-bottom-20">Niños más<br>exitosos y hábiles</h3>
                  <a class="button button-md button-blue-home" href="<?= base_url('login.html') ?>" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>
                </a>
              </div>
            </div> <!-- row -->
          </div>
        </div>

        <div class="row align-items-center margin-top-30">
          <div class="col-12">
            <h2 class="subtitulo-home font-weight-bold text-white font-cursiva">¿Por qué es importante alentar el desarrollo del</span> cerebro en la primera infancia?</h2>
          </div>
          <div class="col-12 col-sm-8">
            <p class="text-white">
              El cerebro humano tiene unos límites temporales para el desarrollo de ciertas habilidades. Son períodos críticos, que se presentan durante los primeros años de vida.
              Si desaprovechamos este vital momento en el cual las capacidades de desarrollo se encuentran en su nivel máximo, la oportunidad se perderá para siempre.<br><br>
              Con <span class="font-weight-bold">LINKIDS</span>, nuestros hijos obtienen las herramientas para formar un mejor desarrollo desde el comienzo de su formación cerebral, facilitando la capacidad 
              mental más compleja <span class="font-weight-bold">¡la inteligencia!</span>
            </p>
          </div>

          <div class="col-12 col-sm-4">
            <img src="<?= base_url() ?>theme/svg/home/foco.svg" alt="Linkids" class="mx-auto d-block" style="width: 60%;">
          </div>
        </div> <!-- row -->

      </div><!-- end container -->
    </div>
    <!-- end Services section -->

    <!-- Services section -->
    <div class="section bg-gray-home" id="nosotros">
      <div class="container">

        <div class="row">
            <div class="col-12 col-sm-7" style="margin-bottom:0px;">
              <h2 class="margin-bottom-20 subtitulo-home font-weight-bold text-blue"><span style="color:#000000;">¿Cómo lo</span><br><div class="font-cursiva">Logramos?</div></h2>
            </div>
        </div> <!-- row -->

        <div class="row align-items-center margin-top-30 container-logro">
            <div class="col-12 col-sm-7" style="margin-bottom:0px;">
              <p class="text-home margin-bottom-20">
                <span class="font-weight-bold"><span style="background-color: #5C6FFB; padding:5px; border-radius: 0px 10px 0px 0px;" class="text-white font-weight-bold">1) Sesiones diarias</span></span><br>
                Siguiendo el orden de las clases, realizaremos de 2 a 3 sesiones diarias con una duración de 9 minutos cada una.<br><br>

                <span class="font-weight-bold"><span style="background-color: #5C6FFB; padding:5px; border-radius: 0px 10px 0px 0px;" class="text-white font-weight-bold">2) En la noche y la mañana</span></span><br>
                Las sesiones de práctica durante la mañana y la noche son las más importantes de este proceso. Las sesiones adicionales deberán de tener una separación de dos horas entre una 
                y otra para una mejor absorción.<br><br>

                <span class="font-weight-bold"><span style="background-color: #5C6FFB; padding:5px; border-radius: 0px 10px 0px 0px;" class="text-white font-weight-bold">3) Observemos los resultados</span></span><br>
                Con esta constancia, estaremos asegurando un mejor futuro para nuestros pequeños, los avances se notarán desde los primeros meses; sin embargo, después del primer año de entrenamiento lograremos ver 
                como el potencial de nuestros hijos va en aumento.<br><br>

                Creamos esta herramienta para brindar a padres y maestros un programa de estimulación temprana integral ¡La excelencia de nuestros hijos está en nuestras manos, <span class="font-weight-bold">¡LINKIDS es el medio para lograrlo!</span>
              </p>
            </div>
            <div class="col-12 col-sm-5 text-center">
              <img src="<?= base_url() ?>theme/svg/home/home-kid4.png" alt="Linkids" class="mx-auto d-block"><br>
              <a class="button button-md button-green-home" href="<?= base_url('login.html') ?>" title="Iniciar Sesión en Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Me interesa</a>
            </div>
        </div> <!-- row -->

      </div><!-- end container -->
    </div>
    <!-- end Services section -->

    <!-- Services section -->
    <div class="section bg-image bg-home-header" id="nosotros"  style="background-color: #ffbd41;">
      <div class="container">

        <div class="row">
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="container-ingredientes-principal text-white">
                <img src="<?= base_url() ?>theme/svg/home/ingrediente.svg" alt="Linkids" class="mx-auto d-block margin-bottom-20">
                <p>
                  Contamos con 3 ingredientes fundamentales:
                </p>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="container-ingredientes text-white">
                <p class="text-left">
                  <img src="<?= base_url() ?>theme/svg/home/ingrediente03.svg" alt="Linkids" class="mx-auto d-block">
                  <b>Música Neurolingüística</b><br>
                  Es el descubrimiento de los múltiples beneficios que se producen en la formación del cerebro por sus amplias melodías, rangos sonoros, etc.
                </p>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="container-ingredientes text-white">
                <p class="text-left">
                  <img src="<?= base_url() ?>theme/svg/home/ingrediente02.svg" alt="Linkids" class="mx-auto d-block">
                  <b>Idiomas</b><br>
                  -Inglés <br>
                  -Chino Mandarín <br>
                  -Francés <br>
                  -Alemán <br>
                  -Español
                </p>
              </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="container-ingredientes text-white">
                <p class="text-left">
                  <img src="<?= base_url() ?>theme/svg/home/ingrediente01.svg" alt="Linkids" class="mx-auto d-block">
                  <b>Bit de Inteligencia (GLENN DOMAN)</b><br>
                  -Imagen<br>
                  -Fondo<br>
                  -Voz<br>
                  -Palabra
                </p>
              </div>
            </div>
        </div> <!-- row -->

      </div><!-- end container -->
    </div>
    <!-- end Services section -->

    <!-- Play video section -->
    <div class="section-lg bg-image bg-home-header parallax" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/bg-video-home.jpg)" id="inicio">
      <div class="bg-black-06">
        <div class="container text-center">
          <div class="row">
            <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-6 offset-lg-3">
              <h1 class="font-weight-thin text-white"><span class="font-weight-bold font-cursiva">Clase Demo</span></h1>

              <a class="button-circle button-circle-xl button-circle-outline-white-2 margin-top-20 popup-youtube"
                data-youtube-src="https://www.youtube.com/embed/UO4-MeYE-40" href="#"><i class="fa fa-play"></i>
              </a>

            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div>
    </div>
    <!-- end Play video section -->

    <!-- Prices section -->
    <div class="section bg-image bg-home-header"  style="background-image: url(<?= base_url() ?>theme/svg/home/precios.svg)">
      <div class="container">
        <div class="text-center container-precios margin-bottom-50">
          <h2 class="subtitulo-home font-weight-bold text-white"><span>¡Evolución en idiomas!</span><br><div class="font-cursiva">Nuestros planes</div></h2>
        </div>
        <div class="row">
          <!-- Prices box 1 -->
          <div class="col-12 col-md-4">
            <div class="prices-box">
              <img src="<?= base_url() ?>theme/svg/home/ingrediente.svg" alt="Linkids" class="mx-auto d-block margin-bottom-20">
              <h3 class="text-white font-weight-bold">Plan<br>Familiar</h3>
              <div class="price">
                <h1 class="text-white">240<span>mxn / mensual</span></h1>
              </div>
              <a class="button button-md button-blue-home" href="<?= base_url('login.html') ?>" title="Iniciar Sesión en Linkids"><i class="fas fa-hand-point-up icon-btn-home"></i>¡Lo quiero!</a>
            </div>
          </div>
          <!-- Prices box 2 -->
          <div class="col-12 col-md-4">
            <div class="prices-box">
              <img src="<?= base_url() ?>theme/svg/home/ingrediente.svg" alt="Linkids" class="mx-auto d-block margin-bottom-20">
              <h3 class="text-white font-weight-bold">Plan<br>Familiar</h3>
              <div class="price">
                <h1 class="text-white">1,990<span>mxn / anual</span></h1>
              </div>
              <div class="price-features">
                <ul class="text-white">
                  <li>Ahorras $890mxn</li>
                </ul>
              </div>
              <a class="button button-md button-green-home" href="<?= base_url('login.html') ?>" title="Iniciar Sesión en Linkids"><i class="fas fa-hand-point-up icon-btn-home"></i>¡Lo quiero!</a>
            </div>
          </div>
          <!-- Prices box 3 -->
          <div class="col-12 col-md-4">
            <div class="prices-box">
              <img src="<?= base_url() ?>theme/svg/home/ingrediente.svg" alt="Linkids" class="mx-auto d-block margin-bottom-20">
              <h3 class="text-white font-weight-bold">Plan<br>para Escuelas</h3>
              <!--
              <div class="price-features">
                <ul class="text-white">
                  <li>Ahorras $890mxn</li>
                </ul>
              </div>-->
              <a class="button button-md button-blue-home" href="#contacto" title="Iniciar Sesión en Linkids"><i class="fas fa-envelope icon-btn-home"></i>Contactar</a>
            </div>
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Prices section -->

    <!-- Contact section -->
    <div class="section bg-image bg-home-header" id="contacto"  style="background: #f5f5f5">
      <div class="container">

      	<div class="row">
            <div class="col-12 col-sm-7" style="margin-bottom:0px;">
              <h2 class="margin-bottom-20 subtitulo-home font-weight-bold text-blue"><div class="font-cursiva">Contáctanos</div></h2>
            </div>
        </div> <!-- row -->

        <div class="row">
          <div class="col-12 col-md-12 text-white">
              <p class="text-blue"><a href="mailto:contacto@linkids.com.mx">contacto@linkids.com.mx</a></p>
          </div>
          <div class="col-12 col-md-12">
            <div class="contact-form">
              <form method="post" action="paginas/frontend/contacto" onsubmit="return sendForm(this,'.contactResult')">
                <div class="form-row margin-bottom-30">
                  <div class="col-12 col-sm-6">
                    <input type="text" id="name" name="nombre" placeholder="Tu nombre">
                  </div>
                  <div class="col-12 col-sm-6">
                    <input type="email" id="email" name="email" placeholder="Tu correo">
                  </div>
                </div>
                <textarea name="extras[message]" id="message" placeholder="Mensaje para Linkids"></textarea>
                <button class="button button-md button-green-home margin-top-30 text-white" type="submit" title="Enviar mensaje a Linkids"><i class="fas fa-user-circle icon-btn-home"></i>Enviar</button>

                <!-- Submit result -->
                <div class="submit-result contactResult">
                  <span id="success">Thank you! Your Message has been sent.</span>
                  <span id="error">Something went wrong, Please try again!</span>
                </div>
              </form>
            </div>
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Contact section -->

    <footer id="form-home">
      <div class="footer bg-blue">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 col-md-6 text-center text-md-left">
              <p class="margin-top-10 text-white">&copy; 2019 Linkids - ¡Evolución educativa en idiomas!</p>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right">
              <ul class="list-horizontal-unstyled">
                <li style="font-size: 24px;"><a href="#" class="text-white"><i class="fab fa-facebook-f"></i></a></li>
                <li style="font-size: 24px;"><a href="#" class="text-white"><i class="fab fa-instagram"></i></a></li>
              </ul>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
      </div>
    </footer>
