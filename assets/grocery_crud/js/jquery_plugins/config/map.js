var maps = [];
function mapa(contenedor,lat,lon){
    this.contenedor = contenedor;    
    this.map = undefined;
    this.marker = undefined;
    this.draggable = true;
    this.lat = lat;
    this.lon = lon;
    this.initialize = function() {                
        this.cargar_mapa();        
    }                
    this.cargar_mapa = function()
    {         
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng(this.lat,this.lon),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };        
        this.map = new google.maps.Map(document.getElementById(this.contenedor),mapOptions);                            
        this.marker = new google.maps.Marker({
        position: new google.maps.LatLng(this.lat,this.lon),
        map: this.map,   
        draggable:this.draggable,
        title: 'tu posicion'});        
    }
}

$(".map_grocery_crud").each(function(){
    var m = $(this);
    var mm = new mapa('map_'+m.data('name'),m.data('lat'),m.data('lng'));
    mm.initialize(); 
    google.maps.event.addListener(mm.marker,'dragend',function(){
        $('#field-'+m.data('name')).val(mm.marker.getPosition());
    });
    maps['field-'+m.data('name')] = mm;
});